
## Install Tools needed for Devops
[Gitlab-toolkit-project](https://gitlab.grupocgd.com/ast/tools/toolkit.git "Toolkit Project")

Pre-Requirements:
- having docker installed
- having curl installed
- user need to be part of docker group

For that First you will need to create a private token on gitlab.

![token](assets/images/gitlab-token.png "Gitlab Private Token")
Then we need to install the toolkit with the command:

```
bash <(curl --header 'PRIVATE-TOKEN: <your private token>' 'https://gitlab.grupocgd.com/api/v4/projects/661/repository/files/setup.sh/raw?ref=master')
```

__setup script:__
```
#!/bin/bash
# Pré-requisitos locais (localhost):
# - docker
# - curl
# Definição de variáveis
TOOLKIT_CONFIG=$HOME/.toolkit
GITLAB_TOKEN='PRIVATE-TOKEN: ycYy1w-PWFuGJvcAM1L7'
# Create ToolKit configuration folder
mkdir -p $TOOLKIT_CONFIG
# Get toolkit template
curl -k --header "${GITLAB_TOKEN}" https://gitlab.grupocgd.com/api/v4/projects/661/repository/files/toolkit.j2/raw?ref=master \
-o $TOOLKIT_CONFIG/toolkit.j2
# Get toolkit configuration
curl -k --header "${GITLAB_TOKEN}" https://gitlab.grupocgd.com/api/v4/projects/661/repository/files/toolkit.yaml/raw?ref=master \
-o $TOOLKIT_CONFIG/toolkit.yaml
# Setup toolkit
cat $TOOLKIT_CONFIG/toolkit.yaml | docker run -i \
-u `id -u`:`id -g` -v $HOME:$HOME -v /tmp:/tmp -v `echo "/$(echo "$PWD" | cut -d "/" -f2)"`:`echo "/$(echo "$PWD" | cut -d "/" -f2)"` \
 -w $PWD \
-e HOME=$HOME  \
nexus.grupocgd.com:8444/cgd/jinja2 jinja2 --format=yaml  $TOOLKIT_CONFIG/toolkit.j2 -o $TOOLKIT_CONFIG/toolkit.env
# Persist toolkit into the environment
grep -qxF ". $TOOLKIT_CONFIG/toolkit.env" $HOME/.bashrc || echo ". $TOOLKIT_CONFIG/toolkit.env" >> $HOME/.bashrc
# Reload bashrc for parent shell
echo "Close your terminal to reload environment and end the setup."
# alias for list tools 
echo "Type list for display available tools list"
```
This script will read through the toolkil.yaml file and launch a jinja container capable of creating the toolkit.env by translating each tool into alias

__toolkit.yaml:__
```
---
tools:
  - alias: ansible
    image: nexus.grupocgd.com:8444/cgd/ansible
- alias: ansible-lint
    image: nexus.grupocgd.com:8444/cgd/ansible
- alias: ansible-vault
    image: nexus.grupocgd.com:8444/cgd/ansible
    run_interaction: '-ti'
- alias: awx
    image: nexus.grupocgd.com:8444/cgd/ansible
- alias: rancher
    image: nexus.grupocgd.com:8444/cgd/rke-cli
- alias: rke
    image: nexus.grupocgd.com:8444/cgd/rke-cli
- alias: helm3
    image: nexus.grupocgd.com:8444/cgd/rke-cli
- alias: kubectl
    image: nexus.grupocgd.com:8444/cgd/rke-cli
  
  - alias: mkdocs
    image: nexus.grupocgd.com:8444/cgd/mkdocs
    run_ports: '-p 8888:8888'
- alias: jinja2
    image: nexus.grupocgd.com:8444/cgd/jinja2
  
  - alias: docker-compose
    image: nexus.grupocgd.com:8443/docker/compose
    run_add_vols: '-v /var/run/docker.sock:/var/run/docker.sock'
    privileged: true
    run_user: '-u 0:0'
- alias: molecule
    image: nexus.grupocgd.com:8444/cgd/molecule
    run_add_vols: '-v /var/run/docker.sock:/var/run/docker.sock'
    privileged: true
    run_user: '-u 0:0'
    run_envs: '-e REGISTRY_USER -e REGISTRY_PASS'
- alias: cookiecutter
    image: nexus.grupocgd.com:8444/cgd/cookiecutter
defaults:
  run_interaction: '-i --rm'
  run_user: '-u `id -u`:`id -g`'
  run_vols: '-v $HOME:$HOME -v /tmp:/tmp -v `echo "/$(echo "$PWD" | cut -d "/" -f2)"`:`echo "/$(echo "$PWD" | cut -d "/" -f2)"`'
  run_workdir: '-w $PWD'
  run_envs: '-e HOME=$HOME'
  image: nexus.grupocgd.com:8444/cgd/centos7
  run_ports: ''
  run_add_vols: ''
```
__toolkit.j2:__
```
{% for tool in tools%}
alias {{tool.alias}}='docker run {% if tool.privileged is defined and tool.privileged is true %}\
--privileged {% endif %}{{ tool.run_interaction | default(defaults.run_interaction)}} \
{{ tool.run_user | default(defaults.run_user) }} {{ tool.run_vols | default(defaults.run_vols) }} \
{{ tool.run_add_vols | default(defaults.run_add_vols) }} {{ tool.run_workdir | default(defaults.run_workdir) }} \
{{ tool.run_envs | default(defaults.run_envs) }} {{ tool.run_ports | default(defaults.run_ports) }} \
{{ tool.image | default(defaults.image) }} {{ tool.command | default(tool.alias) }}'
{% endfor %}
{% set names = [] %}{%- for tool in tools -%}{%- do names.append(tool.alias) -%}
{%- endfor -%}
alias list='echo {{ names }}'
```
If some new tool is loaded you can run the first script in order to add it to the env terminal.<br>
Type list for available tools<br>
![list-tools](assets/images/list.png "Listing tools")