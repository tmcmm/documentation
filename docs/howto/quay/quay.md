# Instal Quay- Container Registry

## 1. Run an instance of Redis

De modo a correr uma instância de Redis vamos fazer pull da imagem existente no nexus e arranca-la no porto 6379 da seguinte maneira:

```
docker run -p 6379:6379 --name redis -d --restart=always  nexus.grupocgd.com:8444/cgd/redis
```

-d esta opcao permite ao container correr em background mode ou "dettached mode"

-p esta opcao permite fazer mapeamento de portos, neste caso irá abrir um porto 6379 no host que corresponde ao porto 6379 no container


[Link documentacional do Redis](https://redis.io/documentationm)


## 2. Run an instance of Postgres

É necessário também ter uma base de dados, neste caso vamos usar postgresql 
Vamos configurar um mount para que os dados possam ser persistidos nos restarts aos containers:

```
docker run -p 5432:5432 --name postgres_quayo -d --restart=always nexus.grupocgd.com:8444/cgd/postgres:11 -v <dir>:/var/lib/postgresql/data
```

-d esta opcao permite ao container correr em background mode ou "dettached mode"
--restart=always permite que o container arranque sempre que vá abaixo por razões externas
-p esta opcao permite fazer mapeamento de portos, neste caso irá abrir um porto 6379 no host que corresponde ao porto 6379 no container
-v cria um mount no lado do host que corresponde a um directorio dentro do container.

É necessário criar uma base de dados e um utilizador com privilegios de superuser da seguinte maneira:

docker exec -it <container_id> bash

```
psql
CREATE DATABASE quayo;
CREATE USER quayo WITH PASSWORD 'quayo';
GRANT ALL PRIVILEGES ON DATABASE "quayo" to quayo;
ALTER USER quayo WITH SUPERUSER;
\c quayo;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
```

## 3. Run an instance of Quay in config mode

Docker login on quayo:
docker login -u="redhat+quay" -p="O81WSHRSJR14UAZBK54GQHJS0P1V4CLWAJV1X2C4SD7KO59CQ9N3RE12612XU1HR" quay.io

Para configurar:
docker run --privileged=true -p 8443:8443 -d quay.io/redhat/quay:v3.2.1 config my-secret-password

!!! note "Nota"

    É Necessário corrrer o quay em modo de configuração para que possa ser criado o ficheiro de yaml
    que depois é passado no arranque do quay.
    OS dados de login para a pagina de configuração é user:quayconfig e password é definida pelo utilizador no arranque do container.

Iremos logar no quay em https://localhost:8443

![Quay config Home page](assets/quay_configuration.png)

Vamos agora configurar um novo registo e seguir os passos.

Vamos configurar a base de dados e usar os dados do nosso container postgres.
![Quay data base configuration](assets/Quay_DB_Config.PNG)

Irá ser criado o esquema automaticamente:
![Quay schema](assets/Quay_DB_Schema.PNG)

Após criação do schema é necessário definir um super user para o quay.
![Quay SuperUser](assets/Quay_DB_Administrator.PNG)

!!! note "Nota"
    
    Caso configuremos o modo de autenticação LDAP , este superuser terá de ser um user existente na AD

Configurar logo da caixa e servidor endpoint:

![Quay logo](assets/config_quay_server.PNG)

Poderá ser apontado para localhost, no entanto no nosso caso foi pedido a criação de um DNS proprio para o quay.

Configurar Redis:

![Quay Redis](assets/redis_config.png)

Configurar e-mail:

![Quay e-mail](assets/email_config.PNG)

Configurar Ldap:

Server:ldap://dsjxxi.grupocgd.com
BASE=DC=GrupoCGD,DC=com
DN= OU=Utilizadores,OU=Servicos-Centrais,OU=CGD
CN= Hercules APLIC_CQ3,OU=HRC - Services,OU=Utilizadores-Servicos,OU=Servicos-Transversais,OU=CGD,DC=GrupoCGD,DC=com
Password= Keypass (user YYHRC66)
UID=sAMAccountName
Mail Attribute: mail

![Quay ldap](assets/ldap_config.PNG)

No final carregamos em validar a configuração e exportamos a configuração como um ficheiro quay-config.tar.gz

![Quay validate](assets/validate.PNG)

![Quay validate](assets/download.PNG)

Depois de lidarmos com a configuração vamos fazer git pull do projecto do quay no gitlab:

<https://gitlab.grupocgd.com/docker/quay.git>

Arrancar o container de quay com:

```
docker run --name quay -d --privileged=true -p 8080:8080 -p 9092:9092 -v <dir>:/quay -v <dir>/quay-config:/conf/stack -v <dir>/quay-storage:/datastorage nexus.grupocgd.com:8444/cgd/quayo:3.2.0
```

Vamos aceder a http://localhost:8080

!!! note "Nota"

    A porta 8443 é exposta no container só se existirem certificados na directoria config que faz o bind para a /conf/stack.
    Se a EXTERNAL_TLS_TERMINATION no config.yaml esta a true como é o nosso caso uma vez que queremos que seja o HA-PROXY a expor o serviço de quay esta directoria (conf/stack) não deve ter certificados ssl.
    Ou seja caso queiramos que a comunicação use SSL terá de ser o nó a fazer essa comunicação com os certificados endpoint.
    O flow de comunicação é o seguinte:
    ingress traffic -> HA-PROXY port 443 -> instance port 8080 -> container port 8080.


Após ter tudo a funcionar e caso queiramos começar a enviar containers para o nosso repositório quay temos de fazer docker  login no repositório e estávamos a debater-nos com o seguinte erro nas versões mais recentes de docker:

Link do erro em docker: <https://qiita.com/tkprof/items/c8f736c577ea3bf7a3ef>

O que foi feito foi acrecentar mais umas opções de cifra na configuração do HA-PROXY:

![Quay ha-proxy](assets/configuracao_HA_PROXY.PNG)

Após esta configuração já nos foi permitido fazer o login no repositório com o dcoker cli nas versões mais recentes:

![Quay login](assets/dockerlogin.PNG)


Depois de ter tudo a funcionar podemos utilizar o docker-compose para levantar os 3's containers como serviço e assim desta forma garantir que estão todos disponíveis ao mesmo tempo, correndo:

```
docker-compose -f docker-compose-quay.yml up -d
```
