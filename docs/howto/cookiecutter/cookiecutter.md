# Templating with Cookiecutter

***[Cookiecutter version  1.7.2]***

**Cookiecutter**  replaces templated items with names it looks up in **cookiecutter.json** so you can produce projects of any kind, including projects that aren’t programs.

![Cookiecutter](assets/cookiecutter.png)

If you don't have cookiecutter installed check our Toolkit solution that runs on docker containers:<br>

[Toolkit Page Documentation](../toolkit/toolkit.md)

Otherwise you will need to clone cookiecutter gitlab repo and run the setup script<br>

```
git clone https://gitlab.grupocgd.com/docker/docker-image-cookiecutter.git
```
__Dockerfile:__
```
FROM nexus.grupocgd.com:8444/cgd/centos7

RUN yum makecache fast \
    && yum install --enablerepo centos-extra,centos-epel -y python2-pip git \
    && yum clean all \
    && pip install -i https://nexus.grupocgd.com/repository/pypi/simple \
    --trusted-host nexus.grupocgd.com --upgrade cookiecutter

ENV GIT_COMMITTER_NAME="CookieCutter"
ENV GIT_COMMITTER_EMAIL="cookiecutter@cgd.pt";

COPY VERSION /
```
__Makefile:__
```
SERVER := nexus.grupocgd.com:8444
NAME   := ${SERVER}/cgd/cookiecutter
MAJOR_VERSION := $$(cat VERSION | cut -f1 -d.)
MINOR_VERSION := $$(cat VERSION | cut -f2 -d.)
TAG    := $$(cat VERSION)
IMG    := ${NAME}:${MAJOR_VERSION}.${MINOR_VERSION}
STABLE := ${NAME}:${MAJOR_VERSION}
LATEST := ${NAME}:latest
 
build:
	@docker build --rm -t ${IMG} .
	@docker tag ${IMG} ${LATEST}
	@docker tag ${IMG} ${STABLE}
 
push:
	@docker push ${NAME}
 
login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS} ${SERVER}
```
__cookiecutter script:__
```
#!/bin/bash

RUNUID=`id -u`
RUNGID=`id -g`
SCRIPTDIR=`dirname "$0"`
VERSION=$(cat ${SCRIPTDIR}/VERSION)

if [ -z "${GIT_COMMITTER_NAME}" ];
then
    GIT_COMMITTER_NAME="CookieCutter";
fi
if [ -z "${GIT_COMMITTER_EMAIL}" ];
then
    GIT_COMMITTER_EMAIL="cookiecutter@cgd.pt";
fi

echo "Using cookicutter container version ${VERSION}"
docker run -u $RUNUID:$RUNGID -v $HOME:$HOME -ti --rm -v $PWD:/data -e \
"GIT_COMMITTER_EMAIL=${GIT_COMMITTER_EMAIL}" -e "GIT_COMMITTER_NAME=${GIT_COMMITTER_NAME}" \
-e HOME=$HOME -w /data nexus.grupocgd.com:8444/cgd/cookiecutter:${VERSION} cookiecutter $@
```
## Create Project Structure:
We can create this way any form of project structure in a standard way using templates.<br>
__Templates available:__

[CookieCutter Templates](https://gitlab.grupocgd.com/ast/templates "Gitlab Templates Projects")<br> 

__run command:__
```
cookiecutter -f https://gitlab.grupocgd.com/ansible-roles/ansible-role-cookiecutter-template.git
```

