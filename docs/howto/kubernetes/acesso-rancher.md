# Acesso a cluster gerido pelo rancher

## Login

O login no rancher é efetuado a partir do URL https://rancher.grupocgd.com . A autenticação deve ser efetuada recorrendo à integração com o Keycloak, opção "Log in with keycloak":

![rancher login](assets/images/rancher-login.png)

O utilizador e password serão idênticos aos registados na Active Directory da CGD.

## Download da configuração do kubectl

No portal do Rancher selecionar o cluster pretendido e selecionar a opção "Kubeconfig File" no canto superior direito: 

![rancher login](assets/images/rancher_kubectl.png)

## Criação de namespaces

A criação de namespaces em projetos deve ser efetuada via rancher. Devido à gestão de autenticação e autorização no cluster (rbac), se a criação dos namespaces for efetuada via kubectl, os mesmos poderão ficar inutilizádos. 

Os namespaces podem ser criados no interface web do rancher ou via rancher cli

