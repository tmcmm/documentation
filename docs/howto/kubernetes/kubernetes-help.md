# Kubernetes Documentation Page

> **Kubernetes** is a platform created for the management of container applications

## What is kubernetes?

Kubernetes (K8s) is an open-source system for automating deployment,
scaling, and management of containerized applications. It groups
containers that make up an application into logical units for easy
management and discovery. Kubernetes builds upon 15 years of experience
of running production workloads at Google, combined with best-of-breed
ideas and practices from the community.

**Planet Scale**: Designed on the same principles that allows Google to
run billions of containers a week, Kubernetes can scale without
increasing your ops team.

**Never Outgrow**: Whether testing locally or running a global
enterprise, Kubernetes flexibility grows with you to deliver your
applications consistently and easily no matter how complex your need is.

**Run Anywhere**: Kubernetes is open source giving you the freedom to
take advantage of on-premises, hybrid, or public cloud infrastructure,
letting you effortlessly move workloads to where it matters to you.

## How does kubernetes works?

**Kubernetes** as several components working at the same time. Some of
the components worth noticing are:

-   ***containers***

-   ***pods***

-   ***deployments***

-   ***services***

-   ***ingressess***

The image below, shows how all this components interact with each other

![k8s architecture](./assets/images/k8s-architecture.png)

We will now explain all the components in detail.

> **Note**
>
> There are other components not referenced here. HPA can be used to
> autoscale deployments. There are jobs, cronjobs, daemonsets, etc..

### Containers

**Container** is the most basic component in a kubernetes cluster. The
**container** as the application itself. It’s made to run in a
**docker** environment and is isolated from other containers running in
the same node.

**Containers** can range from a fully running application server we want
to continually run and monitor to a simple batch job we intend to run
only once.

**Containers** have their own operative system so they can run in any
node with **docker** installed.

> **Note**
>
> It’s important to note an application might use several containers

### Pods

**Pod** is an aggregation unit and the first component in a kubernetes
structure. **Applications** might have one or more **containers** and
those closely coupled **containers** are placed under the same **pod**.

**Containers** inside a **pod** can see each other *(like docker
containers in the same network)* and share the same resources. **Pods**
can have limited resources and run in isolation so they wont affect
other pods.

**Pods** can run for just a few moments (batch jobs, cron jobs, etc..)
or stay up (server) until it’s requested to go down.

**Pods** can be run directly without a deployment.

### Deployments

**Deployment** is another aggregation unit that controls where pods run
inside the cluster. **Deployments** can have one or more **pods**.
**Deployments** controls the amount of replicas of the same **pod**.

With a **deployment** you can choose the amount of replicas for a
certain **pod** to be ran on the cluster. We can choose the affinity of
the **pods** for certain nodes (**pods** prefer to be ran with high
affinity nodes).

**Deployments** will restart any **pod** that fails, they respect a
predefined redeploy order and can maintain a maximum availability.

### Services

**Services** expose the **containers** inside the **pods** of a specific
deployment. Those services can expose the services internally (ex:
**ClusterIP**) or externally (ex: **NodePort**). Applications on the
cluster can access other deployments by using the service name/ip and
the exposed port.

### Ingresses

**Ingress** is one way to expose a **deployment** outside of the
cluster. It uses a **service** exposed internally using **ClusterIP**
and exposes it externally on a specific endpoint.

The **ingress** used by default is the nginx-ingress wich is a L7 load
balancer that balances load to the **service** inside the cluster

## Kubernetes Cheat Sheet

__Kubectl Autocomplete:__
```
source <(kubectl completion bash) 
```
__To add it to your bash shell permanently:__
```
echo "source <(kubectl completion bash)" >> ~/.bashrc
```


__Configuration:__
```
kubectl config view 
```
__Export KUBECONFIG var pointing to another kubeconfig file in order to use multiple files:__
```
export KUBECONFIG=~/.kube/config:~/.kube/kubconfig2 
kubectl config view
```

__Get password for user e2e:__
```
kubectl config view -o jsonpath='{.users[?(@.name == "e2e")].user.password}'
```
```
kubectl config view -o jsonpath='{.users[].name}'    # get first user
kubectl config view -o jsonpath='{.users[*].name}'   # get list of users
kubectl config get-contexts                          # get list of contexts
kubectl config current-context                       # get current context 
kubectl config use-context my-cluster-name           # use my-cluster-name as current context
```

__Add new cluster to kubeconfig file that supports basic authentication:__
```
kubectl config set-credentials kubeuser/foo.kubernetes.com --username=kubeuser --password=kubepassword
```
__Get rancher ca -certificate:__
```
 kubectl config view --minify --raw --output 'jsonpath={..cluster.certificate-authority-data}'
(..)h1Y1NHSEljYS9zcGpxc0ZsZTNnM3NZeWRRTjltUHJDZ0ZzSXQKNVE1S0I1UzYxS0VnTHJnTDNPd2FT(--)
```
__Get cluster certificate in cer format:__
```
kubectl config view --minify --raw --output 'jsonpath={..cluster.certificate-authority-data}' | base64 -D | openssl x509 -text -out -

-----BEGIN CERTIFICATE-----
MIIFMjCCAxqgAwIBAgIQSrgQdZFOuaRNd0Q95JmU2TANBgkqhkiG9w0BAQsFADAZ
MRcwFQYDVQQDEw5HcnVwb0NHRFJvb3RDQTAeFw0wOTA0MDgxNDU0MjBaFw0zNjA1
MDQxNDI4MThaMBkxFzAVBgNVBAMTDkdydXBvQ0dEUm9vdENBMIICIjANBgkqhkiG
9w0BAQEFAAOCAg8AMIICCgKCAgEAqIsmvDwTzb3rlb08G1xgaa3644z29LKQqSkJ
euPzr0xZ4+tUkIGupIYjxL8mWL8nfNsbihltnOnA1qzhZXIs8+vdqPdsShIQOwrF
GXsZwlvlRAoIkyoZpQxPjbrz3UgcKE5Y1aqBAQCsVcKjsxuCakISoR/daXV2QJX/
V7hT4sF9K8qnC/exp0uWcnuorQjKtqlfr9HQMC9HnihpGocV1lo4OLXdqbG/Rux/
BllnYqDdj8YZQKD2hBsJUZP2xUTo9F5NUY/Mta9ubjHuphxPUYVAnnLpG4xAgvRX
IEH4f/GoJ/FEXN8ozFVeRmnJ/XYtL7vJ0ot5rsVVrF+7mAIT7OC04uMn6exZMI13
143fwu/rHKjNhxy/t2c5TumuDktfHeqSvfcwDBU7znaqVLhSz1k488z733b/O2yn
1asA9bV/bDGUwhTTZJhZQOXHlN9tyfjRuLZP1Jljq3wJqpw+YcbxBfpuYIRJPWCd
1owYgtgQBi0zqJ7mMeFsvpqSRNasBchUm2wBBDYC2QW9KXzKv5fjAGR3ea5IcvaU
DUio2Vy23QGh4ffqwGQqscGu6fbEUZHtdI2Ue5VREuvN3a7NtM0KYrVFFZOcmRHY
oUlbecUN52jt/0r08xH2HNaBZLsV6LxrpY6qIpRfOr61lOA1D6YmsSgdmHHCzFMh
Gn9FOGsCAwEAAaN2MHQwCwYDVR0PBAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
VR0OBBYEFCSpjA3GccA8ofrmPVEMzSmxvuG2MBAGCSsGAQQBgjcVAQQDAgEBMCMG
CSsGAQQBgjcVAgQWBBR5zLUxC40YPF0eNerKsLLfqQhOxTANBgkqhkiG9w0BAQsF
AAOCAgEAcREQUCgl5bm6dz1sy+4BW6XxTCB62m2DXH+EAL/PxV+KkZyKP6xZJVOe
YZu6CZnn2jtUTd2y3PBLRpmll5O5de4+4yceOWB92pcvLv/psv7BAvu0aaDfdlR6
HTuELcNx4tuy2QW5TamsiCweFPVDQabvSpL6FfP0lhbnTbOXyiS20Ci9TxC6QBok
enjXR5Ic/O8dLrAXk4nY1BcbCOMiOVzSj4imYwlqCe1Aa6gi8jCIbDTPKVRlH4t9
D8U5AsqnRPEGVbNk2Ib8rdior5t8ucSGHIca/spjqsFle3g3sYydQN9mPrCgFsIt
5Q5KB5S61KEgLrgL3OwaSAquJ7O2ooFkf/d4dE/1mAAswMNXN6YMCaZf7Sn+9khN
Ua/yPNUvz5m0bAH1HrWe+ATuERjZ4PtrCUOG+v84ne42r9crXrFFMgCFu0QUe1e7
GwdflULUJe7k6UWE0N9w1NiZfBWnJuupPm5nD9ucVJc8cTqNOnT52L/cC2Vndlwj
jrkkBU029tYFvBm2GIs9gxwsxkcDkwhwk5NBNTezubXzHQNSxSpw66NO76TFf+VK
cYhBXlLEfEuu7EXTw7JCK0GbTwgx5cPDrhyAjRNrrPWAaDk2crOu+wIiVNb1IDGn
K033JhWeSjzjdl1m9Ev4kCNzE3F6L+Nw1ib83cXgVanVtwBJVIY=
-----END CERTIFICATE-----
```
__Check Resources:__
```
kubectl get services                         
kubectl get pods --all-namespaces             
kubectl get pods -o wide                       
kubectl get deployment my-dep               
kubectl get pods                            
kubectl get pod my-pod -o yaml             
```
__Get cluster current cluster resources__
```
kubectl get nodes --no-headers | awk '\''{print $1}'\'' | xargs -I {} sh -c '\''echo {} ; kubectl describe node {} | grep Allocated -A 5 | grep -ve Event -ve Allocated -ve percent -ve -- ; echo '\'''
```
```
kubectl api-resources --namespaced=true      # Every resources with namespace
kubectl api-resources --namespaced=false     # Every resources withouth namespace
kubectl api-resources -o name                # Get resources with name
kubectl api-resources -o wide                # Get resources wide 
kubectl api-resources --verbs=list,get       # Get resources that suport verbs list and get
kubectl api-resources --api-group=extensions # Get resources of api-group equals extensions
```

__List services classified by name:__
``` 
kubectl get services --sort-by=.metadata.name
```

__List pods classified by pod restarts__
```
kubectl get pods --sort-by='.status.containerStatuses[0].restartCount'
```
__List PersistentVolumes classified by capacity__
```
kubectl get pv --sort-by=.spec.capacity.storage
```
__Get label version of every pod with label app=cassandra:__
```
kubectl get pods --selector=app=cassandra -o \
  jsonpath='{.items[*].metadata.labels.version}'
```
__Get every pod being executed on default namespace:__
```
kubectl get pods --field-selector=status.phase=Running
```
__Get ExternalIPs of every node:__
```
kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="ExternalIP")].address}'
```

### Secrets:

__decrypt a secret:__
```
kubectl get secret default-token-cfz78 -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
kubectl -n log patch serviceaccount default -p ' {"imagePullsecrets": [{"name":"registry-harbor"}]}'
```
```
kubectl get secret registry-harbor -n log -o jsonpath="{['data']['\.dockerconfigjson']}"
ewoJImF1dGhzIjogewoJCSJodHRwczovL2luZGV4LmRvY2tlci5pby92MS8iOiB7CgkJCSJhdXRoIjogImRHMWpiVzA2VDNaMmVXdGtkRGsxTmpJaCIKCQl9LAoJCSJsZGM2MDAxYXBzNDUuZ3J1cG9jZ2QuY29tOjgwNjAiOiB7CgkJCSJhdXRoIjogIllXUnRhVzQ2U0dGeVltOXlNVEl6TkRVPSIKCQl9LAoJCSJscGM2MDAxYXBzMjEuZ3J1cG9jZ2QuY29tOjgwODAiOiB7CgkJCSJhdXRoIjogIll6QTRNak16TVRwbmNtbDZlbXhaTnpnNSIKCQl9LAoJCSJuZXh1cy5ncnVwb2NnZC5jb206ODQ0MyI6IHsKCQkJImF1dGgiOi(....)
```

__Create secret based on a file:__
```
kubectl create secret generic -n log security-roles --from-file=roles.yml
```

__List every secret in use by a pod:__
```
kubectl get pods -o json | jq '.items[].spec.containers[].env[]?.valueFrom.secretKeyRef.name' | grep -v null | sort | uniq
```

### Pods
__Execute bash inside a pod:__
```
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash
```
```
kubectl logs my-pod                                 # Get pod logs
kubectl logs -l name=myLabel                        # Get pod logs with label=mylabel
kubectl logs my-pod --previous                      # Get pod logs from the previous instance
kubectl logs my-pod -c my-container                 # Get logs of specific container inside a pod
kubectl logs -l name=myLabel -c my-container        # Get container logs of a pod label
kubectl run nginx --image=nginx --restart=Never -n  # Execute pod in a specific namespace
mynamespace                                         
```

### Labels:
__Get Labels:__
```
kubectl get nodes --show-labels
```
__Create label and atribute to a node:__
```
kubectl label nodes <your-node-name> node_type=worker
```
__Remove Label:__
```
 kubectl label node <node> <label>-
```

### Networking:

__Service port-forward:__
```
Kubectl port-forward svc/argocd-server -n argocd 8083:80
```

### Permissions:

__All accounts get cluster admin temporary:__
```
Kubectl create clusterrolebinding all-serviceaccounts-admin-temp --clusterrole=cluster-admin --group=system:serviceaccounts
```

### Namespace won't get deleted:
```
Kubectl delete –force –grace-period 0 –ns opa
```
If namespace doesn't get delete you can check if the resource as any pending pods:

```
kubectl api-resources --verbs=list --namespaced -o name   | xargs -n 1 kubectl get --show-kind --ignore-not-found -n opa
```

Put finalizers string empy:
```
kubectl get namespace opa -o json |jq '.spec = {"finalizers":[]}' >temp.json
```
Using curl to send this finalizers string file to the namespace:
```
KUBEAPI=https://rancher.cqgrupocgd.com/k8s/clusters/c-92wbk/api/v1/namespaces
curl -k -s $KUBEAPI/opa/ -H 'Authorization: Bearer token-9wk2z:lwqlng8jt8fddd79x7mdhkd7x754wdx4rnjsldwxr7q2chtrkc7g6m'
```
Use curl to POST it in the namespace:
```
curl -k -H "Content-Type: application/json" -X PUT --data-binary @temp.json $KUBEAPI/opa/finalize -H 'Authorization: Bearer token-9wk2z:lwqlng8jt8fddd79x7mdhkd7x754wdx4rnjsldwxr7q2chtrkc7g6m'
```

### Get PVC usage using curl
You have to create an bearer token to user in the API curl call<br>

![Api-Key](./assets/images/api_key.png)

Get Cluster api endpoint server:
```
kubectl config view --minify | grep server | cut -f 2- -d ":" | tr -d " "
```
__Pass both to the script as:__<br>
```
#!/usr/bin/env bash
#KUBEAPI=https://rancher.domain/v3/
#BEARER='token-fjx4x:lm454tgfq2zgg7574xjkxp(..)'

#curl -k -H "Content-Type: application/json" https://rancher.domain/k8s/clusters/c-vmvl6/api/v1/nodes/ -H 'Authorization: Bearer token-xlzmg:zkscss6r6ctpd9n6p864df822tfd(...)'

function getNodes() {
  curl -s -k $KUBEAPI -H 'Authorization: Bearer '$BEARER'' | jq -r '.items[].metadata.name'
}

function getPVCs() {
  jq -s '[flatten | .[].pods[].volume[]? | select(has("pvcRef")) | '\
'{name: .pvcRef.name, capacityBytes, usedBytes, availableBytes, '\
'percentageUsed: (.usedBytes / .capacityBytes * 100)}] | sort_by(.name)'
}

function column() {
  awk '{ for (i = 1; i <= NF; i++) { d[NR, i] = $i; w[i] = length($i) > w[i] ? length($i) : w[i] } } '\
'END { for (i = 1; i <= NR; i++) { printf("%-*s", w[1], d[i, 1]); for (j = 2; j <= NF; j++ ) { printf("%*s", w[j] + 1, d[i, j]) } print "" } }'
}

function defaultFormat() {
  awk 'BEGIN { print "PVC 1K-blocks Used Available Use%" } '\
'{$2 = $2/1024; $3 = $3/1024; $4 = $4/1024; $5 = sprintf("%.0f%%",$5); print $0}'
}

function humanFormat() {
  awk 'BEGIN { print "PVC Size Used Avail Use%" } '\
'{$5 = sprintf("%.0f%%",$5); printf("%s ", $1); system(sprintf("numfmt --to=iec %s %s %s | sed '\''N;N;s/\\n/ /g'\'' | tr -d \\\\n", $2, $3, $4)); print " " $5 }'
}

function format() {
  jq '.[] | "\(.name) \(.capacityBytes) \(.usedBytes) \(.availableBytes) \(.percentageUsed)"' |
    sed 's/^"\|"$//g' |
    $format | column
}

if [ "$1" == "-h" ]; then
  format=humanFormat
else
  format=defaultFormat
fi

for node in $(getNodes); do
 curl -k -s $KUBEAPI/$node/proxy/stats/summary -H 'Authorization: Bearer '$BEARER''
done | getPVCs | format
```
__bash script you can use:__
![k8s pvc prd](./assets/images/pvc-claim.png)

## Recover Rook Pvc

__Although there is still no confirmation on the consistency of all the data here is some steps to recover:__

- Commands made for retrieving ceph-fs meta data (rook-tools):

!!!note  "Note"
Before executing the commands, it was necessary to stop all pod workloads and rollout the daemonset cephfsplugin to remove the hanging mount processes from ceph-fs.

```
cephfs-journal-tool --rank ceph-fs: all event recover_dentries summary
cephfs-journal-tool --rank ceph-fs: all journal reset
cephfs-table-tool all reset session
cephfs-table-tool all reset snap
cephfs-table-tool all reset inodes
ceph fs reset ceph-fs --yes-i-really-mean-it
```
After the last command the filesystem became HEALTHY and the rook-operator recovered the remaining services<br>

## Recover of the Node:

The server has not fully recovered from the restore. The Unix OS detected at startup that the volume /var/lib/docker was corrupt and some data was removed by the check process during server boot.<br>
__The docker repositories and network configuration, images and containers were damaged so the workloads were not working properly.__<br>
To recover it was necessary:<br>

- Stop docker and containerd services<br>
- Remove folder /var/lib/docker (backup at /var/lib/docker/docker-bck.tar.gz)<br>
- And go back up the services, now zeroed.<br>
Reconnect the node to the cluster with the command:<br>
```
docker run -d --privileged --restart = unless-stopped --net = host -v / etc / kubernetes: / etc / kubernetes -v / var / run: / var / run nexus.grupocgd.com:8443/ rancher / rancher-agent: v2.3.6 --server https://rancher.grupocgd.com --token qhkv924qdtst7k2jptwvxwcl2ln7d7jkk8pqscl8qc28p5dxkht4fd --ca-checksum 96d47e5f865451ace2fbb0e9f6bb9e8f6b6e9f6bb6e8f6bb6e8f6b6b6b6fdbfbbbb0e8bfdbbbbbbbbfdddddbb and ddddddddbb9b9dbdbcb9e0dbbbbmfddddddddddb
```
__No further action was required and the cluster returned to normal, leaving the node with no problems.__

!!!note "Note"
Ensure anti-virus exceptions in all clusters (anti-virus is disabled in cluster k8s1)
