+AUTHOR: Andre Rosendo
======================

+EMAIL: andre.calote.rosendo@cgd.pt
===================================

+TITLE: Kubernetes Tutorial
===========================

+OPTIONS: toc:nil date:nil H:3 num:2 todo:nil tags:nil
======================================================

+REVEAL\_INIT\_OPTIONS: width:"90%", height:"90%", margin:0, minScale:1, maxScale:1, embedded:true, progress:true
=================================================================================================================

+REVEAL\_HLEVEL: 2
==================

-   Kubernetes Basic Tutorial (rancher) :presentation: \*\* DONE Create
    a job/cronjob CLOSED: [2019-12-12 Thu 10:56] \*\*\* Select
    "Resources" \> "Workloads" on the left top corner
    [[./assets/images/rancher-select-workload.png]] \*\*\* Select
    "Deploy" on the right top corner
    [[./assets/images/rancher-select-deploy.png]] \*\*\* Select "More
    options" on the right top
    [[./assets/images/rancher-select-deploy-moreoptions.png]] \*\*\*
    Select the "Workload Type" Following options will run a job on the
    cluster:
    -   Run on a cron schedule
    -   Job [[./assets/images/rancher-workload-type.png]] \*\*\* Fill up
        the job details
    -   name
    -   docker image
    -   namespace
    -   port mappings
    -   etc.. \*\* DONE Create a deployment CLOSED: [2019-12-12 Thu
        11:42] \*\*\* Select "Resources" \> "Workloads" on the left top
        corner [[./assets/images/rancher-select-workload.png]] \*\*\*
        Select "Deploy" on the right top corner
        [[./assets/images/rancher-select-deploy.png]] \*\*\* Fill up the
        job details
    -   name
    -   docker image
    -   namespace
    -   port mappings
    -   etc..

    Creating a *Deployment* with port mappings will create the
    associated service for it \*\* DONE Create a service CLOSED:
    [2019-12-12 Thu 12:06] Services are mostly created automatically
    when we create deployments, but they can be created manually or
    edited for convenience \*\*\* Select "Resources" \> "Workloads" on
    the left top corner [[./assets/images/rancher-select-workload.png]]
    \*\*\* Select "Service Discovery" below the main rancher top bar
    [[./assets/images/rancher-select-service.png]] \*\*\* Select "Add
    Record" [[./assets/images/rancher-service-addrecord.png]] \*\*\*
    Fill up service details
    -   name
    -   namespace
    -   Resolves To
    -   etc..

/"Show advanced options"/ will give you further configurations \*\* DONE
Create an ingress CLOSED: [2019-12-12 Thu 14:06] \*\*\* Select
"Resources" \> "Workloads" on the left top corner
[[./assets/images/rancher-select-workload.png]] \*\*\* Select "Load
Balancing" below the main rancher top bar
[[./assets/images/rancher-select-ingress.png]] \*\*\* Select "Add
ingress" [[./assets/images/rancher-ingress-addingress.png]] \*\*\* Fill
up ingress details - name - namespace - rules

    Rules contains the host-name to use and the services to connect to

\*\* DONE Create an HorizontalPodAutoscaler CLOSED: [2019-12-12 Thu
14:20] \*\*\* Select "Resources" \> "HPA"
[[./assets/images/rancher-select-hpa.png]] \*\*\* Select "Add HPA"
[[./assets/images/rancher-hpa-addhpa.png]] \*\*\* Fill up HPA details -
name - namespace - workload - min replicas - max replicas - metrics

-   Kubernetes Monitoring \*\* Prometheus Out of the box rancher ships
    with prometheus on their catalogs. We can activate the prometheus
    monitoring but choosing "Tools" \> "Monitoring" and then "Enable"

Prometheus is a metrics collection engine that can be used to collect
metrics from several applications and containers. Prometheus can be used
by itself but lacks a proper graphical interface so it's usually used to
export metrics to another tool for visualization (Usually Grafana).

In Prometheus we can also create alerts with base on the metrics it
receives from the applications

Pods and Deployments added to a cluster with monitoring activated will
have of its metrics available in the prometheus application. To access
the prometheus interface we can click on "Resources" \> "Istio" and
click on the prometheus icon. \*\* Grafana When we enable monitoring on
the rancher cluster it will install prometheus on the cluster followed
by grafana.

Grafana is a tool for visualization an aggregation of metrics in eye
pleasing structures. Grafana ships with several dashboards pre-made for
the cluster monitorization. When checking the deployment for an
application running on the cluster we can chose the metrics tab and some
of the metrics will be exposed there. By clicking on the grafana icon we
can open the grafana dashboard specific to that part of the application.

To access the grafana interface we can click on "Resources" \> "Istio"
and click on the grafana icon. We can also select the cluster main page,
click on the triple dots and select "Go to grafana" \*\* App Monitoring
When an application is added to the cluster we have access to the pod
metrics but not the application itself. Most of the time the pod metrics
should be enough since it encompasses memory usage, cpu usage and some
few others.

If we want to be more specific in our metrics and monitor specific
components inside the application we need to expose those application
metrics to prometheus so they can be collected and eventually exposed on
grafana. In most cases it can be a simple library we add in our project.
The applications will expose prometheus metrics on and endpoint and we
can configure the cluster prometheus to read the metrics from those
endpoints. \*\* Istio Istio makes it easy to create a network of
deployed services with load balancing, service-to-service
authentication, monitoring, and more, with few or no code changes n
service code. You add Istio support to services by deploying a special
sidecar proxy throughout your environment that intercepts all network
communication between microservices, then configure and manage Istio
using its control plane functionality.

Istio as several different functionalities but were mostly interested in
the monitoring ones. Other functionalities will become more interesting
as we start to create our service mesh.

We can access all the istio monitoring utilities by going to "Resources"
\rightarrow{} "Istio" and choose the icon on the top right corner. The
options available are: - kiali - jaeger - grafana - prometheus \*\*\*
Kiali Kiali is an observability console for Istio with service mesh
configuration capabilities. It helps you to understand the structure of
your service mesh by inferring the topology, and also provides the
health of your mesh. Kiali provides detailed metrics, and a basic
Grafana integration is available for advanced queries. Distributed
tracing is provided by integrating Jaeger.

    With kiali we can see how our services communicate with each other and with
    the outside. Kiali also shows metrics about those connections by showing us
    their http status.

    Kiali doesn't require further customization from the applications inside the
    deployments.

\*\*\* Jaeger Jaeger is another istio tool that allows us to get
distributed tracing for our service. It allows to get a small sample of
connections going by a certain service and show us the trace for the
communication. This traces are divided in spans and each span as a time
associated with it. We can see which part of the service took the most
amount of time. Spans in jaeger also have their logs associated with
them so if we see increased time in one of the spans we might be able to
see the logs associated with it.

    To use jaeger applications need to adhere to the *opentracing* method so
    jaeger can receive the tracing metrics for the service calls.

-   Glossary \*\* Container Wrapper around an application that makes it
    able to run anywhere. If it supports running containers the
    application will no matter the language used or the type of
    application. \*\* Docker Tool to create and run containers. A docker
    container will run in any machine the docker software is installed.
    \*\* Kubernetes Tool developed by google to help manage the
    increasingly number of containers. It creates a cluster running
    docker and allows us to run docker containers inside the cluster

Kubernetes can do several things to help us manage containers including:
- Automatic deployment - Automatic restart - Load balancing - Auto
Scaling of Applications - Monitoring - etc... \*\*\* Pods Collection of
containers all running on the same host \*\*\* Deployment Collection of
pods spread (or not) around the cluster \*\*\* Service Deployment
exposure inside the cluster \*\*\* Ingress Service exposure outside the
cluster \*\* Rancher Graphical interface to orchestrate kubernetes
clusters. \*\* Prometheus Tool to collect metrics. \*\* Grafana Tool to
expose metrics to the user in a visual appealing manner. Requires a
collector (prometheus). \*\* Istio Tool to manage the mesh inside the
kubernetes cluster. \*\*\* Kiali Draws the cluster mesh and shows us
service communications. \*\*\* Jaeger Distributed tracing for the
services \*\* Helm Application manager for the cluster. Allows us to
install, upgrade, rollback all components at once for an application in
the cluster. \*\*\* Helm chart Yaml files in a specific structure to
describe the application and how to install it in the cluster \*\*
Elasticsearch Central log collector for easy consultation.
