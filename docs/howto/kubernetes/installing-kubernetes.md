# Installing Kubernetes

### CGD canais Documentation on Kubernetes

### Requirements:
Make sure the following tools are installed on the installation server:

-   kubectl
-   rke
-   helm

You can install all these tools with the Toolkit Project that launches all these tools in containers (only requirement is to have docker installed).

__For Further documentation on installing tools with Toolkit:__[Toolkit Page Documentation](../toolkit/toolkit.md)

## Install Docker on localhost and Kubernetes hosts
In order to install docker you can install it manually on your machine from the official repo and configure it or you can use the ansible role that installs and configure.

- You will have to clone the cicd tools repo:
```
git clone https://gitlab.grupocgd.com/awx-projects/cicdtools.git
```
- If it is on your local machine run:
```
ansible-galaxy install -r roles/requirements.yml
```
- Then alter the site.yml to run on localhost and run:
```
ansible-playbook site.yml --connection=local
```
If you need to install it on the kubernetes nodes:

- Alter the inventory file with your kubernetes nodes<br>
You need to create a new-group and pass the hostnames below<br>

![inventory](assets/images/inventory.png "Ansible Inventory")

- Then you will need to log on [AWX](https://awx.grupocgd.com/ "Awx Homepage") and update the project source aswell as the inventory source.

![project-awx](assets/images/project.png "Project AWX ")
![inventory-awx](assets/images/inventory-awx.png "Inventory AWX ")

- After that you can launch the template that will install the docker on the destination machines and provide you with a user docker that can interact with the docker.sock to manage containers, but first you need to set the limit to the new group that you created on the inventory file.
![template-limit](assets/images/limit.png "Template limit ")
![awx-job](assets/images/awx-finished.png "AWX Job")

These are the files that are used on the installation of docker:
__site.yml:__
```
- hosts: all

  tasks:
  - block:
    - import_role:
        name: ansible-role-so-commons

    - import_role:
        name: sudoers
      vars:
        rules_file_source_path: "{{ playbook_dir }}/templates/docker.sudoers.j2"
        rules_file_name: docker

    - import_role:
        name: docker-ce
      vars:
        docker_version: 19.03.13
  tags:
  - docker  
```
 > The requirements file as the ansible roles that will be downloaded as a requirement to run the playbook, as you can see above we have 3 tasks of import_role<br>
__Requirements.yml:__
```
- src: "https://oauth2:J6q2ANwuzGR3YYWuUF9B@gitlab.grupocgd.com\
/ansible-roles/ansible-role-so-commons.git"
  scm: git
  version: master
  name: ansible-role-so-commons

- src: "https://oauth2:J6q2ANwuzGR3YYWuUF9B@gitlab.grupocgd.com\
/ast/ansible/roles/docker-ce.git"
  scm: git
  version: master
  name: docker-ce

- src: "https://oauth2:J6q2ANwuzGR3YYWuUF9B@gitlab.grupocgd.com\
/ansible-roles/ansible-role-sudoers.git"
  scm: git
  version: master
  name: sudoers
```
## High Availability (HA) Install

This installation will follow the rancher documentation for an high
availability install.

### Starting the cluster
To install the cluster, rancher recommends the use of their `rke` executable <br>

  - First we need to log on the bastion server lpc6001cit04 that has the docker ssh public keys exchanged during the installation of docker role.
  - As member of GGSSIA-DEG-UEG3-Canais you can log on user rke with:
``` 
sudo -iu rke
cd /deploy/rke/clusters/rancher_lab/etc
``` 
- Then you will need to create a file as this example below:<br>
```
# This is the rancher master controler
cluster_name: "cluster-lab-upgrade"
kubernetes_version: v1.16.15-rancher1-1
ignore_docker_version: false
ssh_key_path: id_rsa_cluster

local_cluster_auth_endpoint:
    enabled: true
    fqdn: "api.cloudlab2.k8scgd.com"
    ca_certs: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlINGpDQ0JjcWdBd0lCQWdJVGV3QUFBQVplTTFkOXQrVmNZd0FCQUFBQUJqQU5CZ2txaGtpRzl3MEJBUXNGDQpBREFaTVJjd0ZRWURWUVFERXc1SGNuVndiME5IUkZKdmIzUkRRVEFlRncweE5qQTFNRFV4TXpNNU1qQmFGdzB5DQpOakExTURVeE16UTVNakJhTUVVeEV6QVJCZ29Ka2lhSmsvSXNaQUVaRmdOamIyMHhHREFXQmdvSmtpYUprL0lzDQpaQUVaRmdoSGNuVndiME5IUkRFVU1CSUdBMVVFQXhNTFEwZEVSVzUwVTNWaVEwRXdnZ0VpTUEwR0NTcUdTSWIzDQpEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUM2cGRsRkFaaFlEM24wQnZxeFNXdWU1RmRrTFVKOU1XSGw3aVhCDQpqaUdmVUhlTExkaVgyelVKc3BXOExqMWRyRmtjMW1GTDIrZXMrdDhjTHpYcGg4ZDBmR3dGSHZBZWhnOVgrRm14DQo1WGFFWUpRRGZ0cWlwUDA3VG9lcThGWlJLQmdjUUZHL0N1angzalBJS0ZnMks3WTh2VUlKYzdkYnR4dkFPZUpoDQp4ZHprekw1WWd2VGZ4Um1zTHVkUk83ZVh5NFlmclk1QTRYRTJKYnFSK0hlNFdyWDhQVGc4ZzJOWU5oNTlHQ0hhDQpkcHRCemRuTmJvbW5OTjFYWHBtRWZTbnAvUHJHMFozbGg3ZVNmT000ZkdVUW9mNEtQNXp4b240alc3Y2V1VE9HDQpZdS81b3NIOXhmbFh1U3BhMmxJUmdMK0pTandoWVF5TEhlT1Erdy9taDZ2YlRpa2ZBZ01CQUFHamdnUDFNSUlEDQo4VEFTQmdrckJnRUVBWUkzRlFFRUJRSURBZ0FETUNNR0NTc0dBUVFCZ2pjVkFnUVdCQlExSExUWVY0MHNPd2FiDQpwZkZWL3o1WjJkZnlCVEFkQmdOVkhRNEVGZ1FVWXVhVThqS2tEL1VKMEJNb29IbDlpdlY1amx3d2dnRTlCZ05WDQpIU0FFZ2dFME1JSUJNRENCa0FZSUtRRUJBUUVCQVFFd2dZTXdRQVlJS3dZQkJRVUhBZ0VXTkdoMGRIQTZMeTkzDQpkM2N1WjNKMWNHOWpaMlF1Y0hRdmNHdHBMMUJ2YkdsamVTOVZVMHhsWjJGc1VHOXNhV041TG1GemNBQXdQd1lJDQpLd1lCQlFVSEFnRVdNMlowY0RvdkwyWjBjQzVuY25Wd2IyTm5aQzV3ZEM5d2Eya3ZVRzlzYVdONUwxVlRUR1ZuDQpZV3hRYjJ4cFkza3VkSGgwQURDQm1nWUlVZ0lDQWdJQ0FnSXdnWTB3UlFZSUt3WUJCUVVIQWdFV09XaDBkSEE2DQpMeTkzZDNjdVozSjFjRzlqWjJRdWNIUXZjR3RwTDFCdmJHbGplUzlWVTB4cGJXbDBaV1JWYzJWUWIyeHBZM2t1DQpZWE53QURCRUJnZ3JCZ0VGQlFjQ0FSWTRablJ3T2k4dlpuUndMbWR5ZFhCdlkyZGtMbkIwTDNCcmFTOVFiMnhwDQpZM2t2VlZOTWFXMXBkR1ZrVlhObFVHOXNhV041TG5SNGRBQXdHUVlKS3dZQkJBR0NOeFFDQkF3ZUNnQlRBSFVBDQpZZ0JEQUVFd0N3WURWUjBQQkFRREFnR0dNQThHQTFVZEV3RUIvd1FGTUFNQkFmOHdId1lEVlIwakJCZ3dGb0FVDQpKS21NRGNaeHdEeWgrdVk5VVF6TktiRys0Yll3Z2Y0R0ExVWRId1NCOWpDQjh6Q0I4S0NCN2FDQjZvWW9hSFIwDQpjRG92TDNkM2R5NWpaMlF1Y0hRdmNHdHBMMGR5ZFhCdlEwZEVVbTl2ZEVOQkxtTnliSWFCdld4a1lYQTZMeTh2DQpRMDQ5UjNKMWNHOURSMFJTYjI5MFEwRXNRMDQ5UjBOWVRrTk1TVkpEUVZNek1ERXNRMDQ5UTBSUUxFTk9QVkIxDQpZbXhwWXlVeU1FdGxlU1V5TUZObGNuWnBZMlZ6TEVOT1BWTmxjblpwWTJWekxFTk9QVU52Ym1acFozVnlZWFJwDQpiMjRzUkVNOVozSjFjRzlqWjJRc1JFTTlZMjl0UDJObGNuUnBabWxqWVhSbFVtVjJiMk5oZEdsdmJreHBjM1EvDQpZbUZ6WlQ5dlltcGxZM1JEYkdGemN6MWpVa3hFYVhOMGNtbGlkWFJwYjI1UWIybHVkRENCK2dZSUt3WUJCUVVIDQpBUUVFZ2Uwd2dlb3dOd1lJS3dZQkJRVUhNQUtHSzJoMGRIQTZMeTkzZDNjdVkyZGtMbkIwTDNCcmFTOUhjblZ3DQpiME5IUkZKdmIzUkRRU2d4S1M1amNuUXdnYTRHQ0NzR0FRVUZCekFDaG9HaGJHUmhjRG92THk5RFRqMUhjblZ3DQpiME5IUkZKdmIzUkRRU3hEVGoxQlNVRXNRMDQ5VUhWaWJHbGpKVEl3UzJWNUpUSXdVMlZ5ZG1salpYTXNRMDQ5DQpVMlZ5ZG1salpYTXNRMDQ5UTI5dVptbG5kWEpoZEdsdmJpeEVRejFuY25Wd2IyTm5aQ3hFUXoxamIyMC9ZMEZEDQpaWEowYVdacFkyRjBaVDlpWVhObFAyOWlhbVZqZEVOc1lYTnpQV05sY25ScFptbGpZWFJwYjI1QmRYUm9iM0pwDQpkSGt3RFFZSktvWklodmNOQVFFTEJRQURnZ0lCQUFlalc5MHRpazk2TWdCTGpVdFBHZ1hPK1g5Z3lMMFZiVzBWDQpOc3A2M3J6ZTEzTDlRdTAxNmV3UjY4U2ZieVFxc2pwWjRNMXRtbHVxRzBuelNRN3RFTHpGVElCTkJSTWYxWDlwDQphSVFzaVl2OHFqaVIxc0kvRloyNVBHT3JTdDcrOWZ1cExhenlrQXVBdkp5V1RPdGdiOGNnKzdjRGZ5VzFzKy9DDQpxSldGQjdmMkdVbEZ1R3NkaVFOZnh1ZVdJNWl1VGdNcUpLOTlFMEZSZzVMdlN4YU55VUp0cmFXbm9LM1dJRnRIDQpZdDIwM2hnd2ErL2JDZzFFcjhVWEtBZ1B5dEtqK2lENHlMMlJPVUZiTzN3SzhJTmNLUjZ1aGVrVlZoMmlNN01QDQpEZ2x2Nlk1RGMycVRtN2ZHK3ZkOVc4S1l2RHpVdktkeUN2TVJ6SlpQSXpCemNIalhaTmlIbll6b3o5dmJPM1J0DQpEVTg0V2xZaW5XeFNISXduQWpCeVB2V3JWL1NPeFJ2c1dDU2VWbDhsaFdHYUNzL1lKdnhDbVBIemkxRWY0TlNJDQpKWFZWVmVncVRqY2xLb2VXU0xGTTJVbktUZ0Z5UzhIQ2dSQkJ0ZEYzQUxTLzA2ZHFjMGxmcEpUcmFobXorb1pnDQp1Wm16ekNTWlRuSXRSTjlZV3E5SGg4UU9kVzdkei9GWEt6MUJES0hQV3dXSm52NVQwTVNRb2hRTHE2TUxzRlB2DQoxNnJLTkZzbXljeGFzeGhDVG1wVzloSE8xVjF4SzJoTnFmazNENUR0cktSZDhjandMUC9sLzNQdW5keStYZUFMDQpPYzNIb2lBU1NKUThkUkRMSDUycW41UmNwdGNoeElNLzJTMGQrSGV2ZWpaa3pVQkYwTmI4VjN4SC9IUnVvYy9XDQpxT0dKVGdrMg0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ0K"

nodes:
  ### Cluster lab upgrade

  - address: 10.11.208.33
    hostname_override: ldc6001cit05a
    user: docker
    role:
      - controlplane
      - etcd

  - address: 10.11.208.34
    hostname_override: ldc6001cit05b
    user: docker
    role:
      - worker

  - address: 10.11.208.35
    hostname_override: ldc6001cit05c
    user: docker
    role:
      - worker

  - address: 10.11.208.36
    hostname_override: ldc6001cit05d
    user: docker
    role:
      - worker

### Private Registry
private_registries:
    - url: registry.dev.grupocgd.com
      is_default: true
    - url: nexus.grupocgd.com:8444
      user: docker-rancher
      password: docker

### SSH Key
#ssh_agent_auth: true
#ssh_key_path: id_rsa_cluster

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h
```
- Then you have to make sure you have the private key __id_rsa_cluster__ on the directory you are on. This key will authenticate the user docker on the destination hosts.<br>

- We can install the cluster by running the following command 
```
    rke up --config filename.yml
```
There are some problems with using a custom `data-root` in a cluster
installation.

With those configurations everything should go right and we should
receive a message saying:

    INFO[0051] Finished building Kubernetes cluster successfully

### Upgrading Kubernetes version using RKE
You can check kubernetes available versions with:
```
[rke@lpc6001cit04 ~]$ rke config --list-version --all
```
  v1.17.11-rancher1-1<br>
  v1.18.8-rancher1-1<br>
  v1.16.15-rancher1-1<br>
  v1.15.12-rancher2-5<br>

And then you can pass it to the cluster yaml file as a variable to upgrade or downgrade kubernetes cluster<br>
  __kubernetes_version:__ v1.16.15-rancher1-1

!!! note "Important Note"
    If you can't find the desirable version is because you don't have the right rke version.<br> 
    So in order to upgrade this version you will need to **download the rke version then upload it on Nexus binaries** , clone the rke image repo from gitlab and alter the rke version

__Download rke version needed:__<br>
[Github-rke-releases-project](https://github.com/rancher/rke/releases/ "RKE Releases")<br>
__Upload it to nexus binaries repository:__<br>
[Nexus Binaries Repo](https://nexus.grupocgd.com/repository/binaries/rancher/rke/ "Nexus Binaries")<br>
__Clone the gitlab repo from rke image:__
```
git clone https://gitlab.grupocgd.com/ast/docker/rke-cli.git
```
__Alter the version you just downloaded in the Dockerfile:__
```
FROM nexus.grupocgd.com:8444/cgd/centos7

ENV RKE_VERSION 1.1.7
ENV HELM2_VERSION 2.16.5
ENV HELM3_VERSION 3.3.4
ENV HELM_2TO3_VERSION 0.5.1
ENV KUBECTL_VERSION 1.18.0-0
ENV RANCHER_CLI_VERSION 2.4.0
ENV K3D_VERSION 3.1.5

ADD ./files/kubernetes.repo /etc/yum.repos.d/kubernetes.repo
ADD ./files/rpm-package-key.gpg /etc/pki/rpm-gpg/rpm-package-key.gpg
ADD ./files/yum-key.gpg /etc/pki/rpm-gpg/yum-key.gpg

RUN curl https://nexus.grupocgd.com/repository/binaries/rancher/rke/${RKE_VERSION}/rke-${RKE_VERSION}-linux-amd64.bin --output /bin/rke \
    && chmod +x /bin/rke \
    && yum makecache fast \
    && yum install -y --enablerepo=kubernetes wget git kubectl-${KUBECTL_VERSION} \
    && yum clean all \
    && curl https://nexus.grupocgd.com/repository/binaries/helm/helm/${HELM3_VERSION}/helm-${HELM3_VERSION}-linux-amd64.tar.gz --output helm-${HELM3_VERSION}-linux-amd64.tar.gz \
    && tar xzf helm-${HELM3_VERSION}-linux-amd64.tar.gz \
    && cp linux-amd64/helm /bin/helm3 \
    && chmod +x /bin/helm3 \
    && rm -rf linux-amd64 helm-${HELM3_VERSION}-linux-amd64.tar.gz \
    && curl https://nexus.grupocgd.com/repository/binaries/helm/helm/${HELM2_VERSION}/helm-${HELM2_VERSION}-linux-amd64.tar.gz --output helm-${HELM2_VERSION}-linux-amd64.tar.gz \
    && tar xzf helm-${HELM2_VERSION}-linux-amd64.tar.gz \
    && cp linux-amd64/helm /bin/helm \
    && chmod +x /bin/helm \
    && rm -rf linux-amd64 helm-${HELM2_VERSION}-linux-amd64.tar.gz \
    && curl https://nexus.grupocgd.com/repository/binaries/rancher/rancher-cli/${RANCHER_CLI_VERSION}/rancher-cli-${RANCHER_CLI_VERSION}-linux-amd64.tar.gz --output rancher-cli-${RANCHER_CLI_VERSION}-linux-amd64.tar.gz \
    && tar xzf rancher-cli-${RANCHER_CLI_VERSION}-linux-amd64.tar.gz \
    && cp rancher-v${RANCHER_CLI_VERSION}/rancher /bin/rancher \
    && chmod +x /bin/rancher \
    && rm -rf rancher-v${RANCHER_CLI_VERSION} rancher-cli-${RANCHER_CLI_VERSION}-linux-amd64.tar.gz \
    && curl https://nexus.grupocgd.com/repository/binaries/rancher/k3d/${K3D_VERSION}/k3d-${K3D_VERSION}-linux-amd64.tar.gz --output k3d-${K3D_VERSION}-linux-amd64.tar.gz \
    && tar xzf k3d-${K3D_VERSION}-linux-amd64.tar.gz \
    && cp k3d-linux-amd64 /bin/k3d \
    && chmod +x /bin/k3d \
    && rm -rf k3d-linux-amd64 k3d-${K3D_VERSION}-linux-amd64.tar.gz
```
Then build the new image that will be used in your toolkit env:
In your recent cloned repo you need to alter the version of the file VERSION to the downloaded one and then run:
```
make build && make push
```
This will build the new rke image version and push it to the registry
__makefile:__
```
SERVER := registry.dev.grupocgd.com
NAME   := ${SERVER}/cgd/rke-cli
MAJOR_VERSION := $$(cat VERSION | cut -f1 -d.)
MINOR_VERSION := $$(cat VERSION | cut -f2 -d.)
MICRO_VERSION := $$(cat VERSION | cut -f3 -d.)

TAG    := $$(cat VERSION)
IMG    := ${NAME}:${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}
STABLE := ${NAME}:${MAJOR_VERSION}
LATEST := ${NAME}:latest
 
build:
	@docker build --no-cache --rm -t ${IMG} .
	@docker tag ${IMG} ${LATEST}
	@docker tag ${IMG} ${STABLE}
 
push:
	@docker push ${NAME}
 
login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS} ${SERVER}
```
__Force the pull of the latest image:__
```
docker pull registry.dev.grupocgd.com/cgd/rke-cli:latest
```
__Then for the last step run the command listing the available kubernetes version again:__
```
rke config --list-version --all
```
# References

# 
[RKE installation](https://rancher.com/docs/rke/latest/en/installation/)<br>
[High Availability (HA)Install](https://rancher.com/docs/rancher/v2.x/en/installation/ha/)<br>
[Air gap install (system behindproxy)](https://rancher.com/docs/rancher/v2.x/en/installation/air-gap/)<br>


# Uninstall Kubernetes and dependencies
__Run the below script:__
```
#!/bin/bash
# Removes everything running on docker
docker stop $(docker ps -a -q); docker system prune --all --volumes --force; 

# Mounts anything related to kubelet
#sudo umount $(sudo df -h | grep "/var/lib/kubelet/" | awk '{ print $6}')
for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do sudo umount $mount; done

# Deletes all rancher related files
sudo rm -rvf /etc/ceph \
       /etc/cni \
       /etc/kubernetes \
       /opt/cni \
       /opt/rke \
       /run/secrets/kubernetes.io \
       /run/calico \
       /run/flannel \
       /var/lib/calico \
       /var/lib/etcd \
       /var/lib/cni \
       /var/lib/kubelet \
       /var/lib/rancher/rke/log \
       /var/log/containers \
       /var/log/pods \
       /var/run/calico

# Deletes all routes create
# netstat -rn | tail -n +3 | grep "flannel" | awk '{system("sudo route del -net "$1"/24 gw "$2)}'
# netstat -rn | tail -n +3 | grep "cali" | awk '{system("sudo route del -host "$1" gw "$2)}' 

sudo yum remove -y docker-ce docker-ce-cli containerd

sudo rm -rf /var/lib/docker \
            /var/lib/containerd \
            /var/lib/docker-engine \
            /apps/docker

sudo find / -name docker -exec rm -rfv {} +;
sudo reboot
```
__Create a loop if there are more than one machine that you pretend to uninstall:__
```
for ((i=56; i<61; i=$i+1)); do ssh ldc6001aps$i < script.sh & done
```

### Configure RHSSO on Rancher
In order you configure Rhsso on Rancher you need to log on [Keycloak](https://auth.grupocgd.com/auth/ "Keycloak Homepage")

![rhsso](assets/images/rhsso.png "rhsso")
![rhsso1](assets/images/rhsso1.png "rhsso1")
![rhsso2](assets/images/rhsso2.png "rhsso2")
![rancher-sso](assets/images/rancher-rhsso.png "rancher-sso")
![rancher-sso](assets/images/rancher-rhsso1.png "rancher-sso1")
![rancher-sso](assets/images/rancher-rhsso2.png "rancher-sso2")


### Future vision with Public Cloud
![rancher-future](assets/images/future.png "rancher-future")