# Docker Basics

CGD canais v1.0 :toc: :sectnums:

> Docker uses containers, and those containers are made from images.
>
> You can download docker images from local/external repos by running
> the docker run command.
>
>     sudo docker run hello-world
>
> Running the hello-world container is a good way to check if the docker
> installation went well.

## Basic Setup

The first things that should be done is the installation of `docker`.
You can install docker by following the docker installation guide ?.

Most docker commands require root permissions to be executed. One thing
that can be done is to add the user to the docker group.

    sudo usermod -aG docker [username]

-   **username**: *User to be given docker permission.*

> **Caution**
>
> Docker containers can be used to obtain root privileges on the docker
> host. Be very careful with adding users to the `docker` group.

## Docker Images

Docker will try to find a repo containing that image so it cant create a
new container and run it. Every time you use the `docker run` command
you’ll create a new container for that image.

To check the images you have downloaded you can do:

    # shows the active images
    sudo docker image ls

    # shows all the images
    sudo docker image ls -a

You can remove images by using the `id` or `name`:

    sudo docker images rm hello-world

You can only remove an image if there isn’t a single container using
them.

## Docker Containers

A container is the binary that docker runs. You create containers from
docker images so you can execute them.

You can check which containers docker with the following command:

    # shows the active containers
    sudo docker container ls

    # shows all the containers
    sudo docker container ls -a

The commands are the same as the `image` commands and you can only
remove a container if it isn’t active. Containers can be removes either
by the id or by the name.

    sudo docker container rm hello-world

## Container Options

When you run a container you can give it some parameter that will change
their overall behavior.

    sudo docker run --name hello-world -p 8080:8080 hello-world -u="0:980"

-   **-p**: *Allows the user to expose some ports for the container.*
    *You can add several ports by repeating the parameter **-p**.* *It
    binds the port of the host to the port of the container.*

-   **--name**: *Name for the container.* *It’s easier to manage the
    containers if we give it a proper name*

-   **-u**: *User and group to use with docker.* *In the example **0**
    means **root** and **980** means the **docker** group.*

### Accessing Docker Container Files

In some cases while using docker containers it might be useful to be
able to access the container data. To be able to interchange data
between the container and the host it’s necessary to use a volumes and
mount the containers to use specific volumes.

There is somewhat detailed documentation from docker in how we should
use volumes and what we can do with them ? so im just gonna give a
practical case with an example.

### Running HTTPD Docker Container

When running an httpd docker container ? it’s expected to be able to
change the httpd configurations files.

To run the httpd container you would run the following command:

    docker run -d --name httpd -p 80:80 -p 443:443 httpd

It will attempt to install the httpd docker from an image stored locally
and then after it fails it will download the image from the [***Docker
Hub***](http://hub.docker.com)

Now we can check if the httpd container is running properly by testing
out the following url:

    http://localhost

From this step we want to start configuration the httpd container and
very easily realise we cant access the httpd files inside the container.

To be able to access the httpd files inside the container we need to
create a volume and mount that volume when the container is created.

To create a volume you just have to decided on a name that makes sense.
For the **httpd** volume i’m gonna go with the name **httpd**.

    docker volume create httpd

We can now start a new container using the newly created volume.

    docker run --name httpd -p 80:80 -p 443:443 --mount source=httpd,target=/usr/local/apache2/ httpd

You can use the parameter `-v` or the parameter `--mount`. I’ve chosen
to use the parameter `--mount` cause it’s the recommend one by the
documentation ?.

`--mount source=httpd,target=/usr/local/apache2/`

-   **source**: *The name of the volume to use for the container*

-   **target**: *The folder on the container to bind to the volume*

You can now inspect the volume and check which folder that volume is
mounted on the host.

    docker volume inspect httpd

You should expect some output like this:

    [
        {
            "CreatedAt": "2019-08-07T10:38:55+01:00",
            "Driver": "local",
            "Labels": {},
            "Mountpoint": "/var/lib/docker/volumes/httpd/_data",
            "Name": "httpd",
            "Options": {},
            "Scope": "local"
        }
    ]

The ***Mountpoint*** indicates the folder where the volume was mounted.
We can now access the files that were mounted into that folder just have
in consideration you need root access to be able to see those files.

Just logging into a root console would do the trick:

    sudo -i

### Container Networks

All containers by default are added to the default bridge. When using
the parameter **-p** we publish a port on the default bridge.

When the containers are started they are given a new ip inside the
docker interface. They can be accessed through that ip. Containers can
communicate with each other using the ip directly but since the ip can
change whenever the container is started, would be a better option to
connect using the containers name.

The solution is to create a network? and add both containers to the same
network. Doing that will allow each container to expose all the ports to
the network and connect directly using the containers name as the host.

#### Creating a new network

The type of network we will be creating is a bridge network?. It’s the
type of network that allows docker containers to communicate between
each other.

Creating a network follows the same scheme as other component of docker.
You can just type the following cli command:

    docker network create internal

-   **internal**: *The name I chose for the network*

#### Removing network

All containers must be disconnected from the network to remove it,

    docker network rm internal

#### Adding container to the network

Containers can be added to a network at the moment of creation by using
the parameter **--network**

    docker run --network internal httpd

To add an already running container to a new network you can connect it
directly to the network.

    docker network connect internal httpd

#### Removing container from the network

    docker network disconnect internal httpd

## Docker and nexus

As we start to use docker containers is a good practice to use a proxy
to the docker hub. It’s also important to have a place were we can store
our private docker images.

The in house solution to host repositories is nexus and since it
supports docker we’ll stick with it.

### Installing Nexus

We’re gonna be installing nexus as a container. To be able to run nexus
as a container? and use it there are several things we need to do:

-   **Expose the nexus ports**: *As a start we will only expose the port
    8081 as the nexus default port. We will do it by using the parameter
    **-p 8081:8081***

-   **Get access to nexus files**: *To have access to nexus files we
    will have to create a new volume and mount it on the container.
    Create it by running **docker volume create nexus** and adding the
    following parameter on the run command **--mount
    source=nexus,target=/nexus-data***

-   **Allow connection between HTTPD and nexus**: *We will do it by
    creating a new docker network and adding both containers to that
    network*

-   **Give the container a name**: *Going to call it **nexus***

<!-- -->

    docker volume create nexus

    docker network create internal

    docker run -p 8081:8081 --name nexus --network internal --mount source=nexus,target=/nexus-data sonatype/nexus

Nexus should now be running and exposed to ``

To configure nexus docker repository follow the documentation from
nexus?. In this tutorial it’s used and insecure repository on docker but
we re gonna configure it using ssl and terminating it at the httpd
layer.

#### Nexus HTTPD ssl termination

Docker wants to use ssl to connect to the repository and we will allow
it cause its a good practice. But instead of nexus serving ssl directly
we are gonna connect to httpd using ssl and then from httpd connect to
nexus directly using http.

Assuming we have an **HTTPD** container running and it’s on the same
network as the **nexus** container we can create a reverse proxy?
directly to nexus.

> **Note**
>
> To connect from a container to another use the containers name as host

After we configure httpd to receive and ssl connection we need to
configure the reposiroties on docker and add the public certificate to
docker trust store.

#### Configuring nexus repository on docker

First we need to add the new registries to the docker daemon. Have in
mind we will use a group repository to pull content and we are gonna use
a private repository to push content(nexus doesn’t allow push on a group
repo).

Create or edit the following file:

    {
            "registry-mirrors": ["https://nexus.docker:443","https://nexus.private:443"]
    }

> **Note**
>
> **nexus.docker** and **nexus.private** are hosts on the **/etc/hosts**
> pointing at localhost

    {
      "registry-mirrors": [
        "https://nexus.grupocgd.com:8443",
        "https://nexus.grupocgd.com:8444"
      ],
      "bip": "10.11.67.1/24"
    }

After we added the registries we can add the certificates following the
docker documentation?

#### Deleting images from nexus

Deleting docker images from nexus might pose a problem. The simplest way
to do it seems to be sending a DELETE request to the repository. Since
the repository requires login were sending the information plain on the
curl request. The image being deleted is chosen from the manifest given.

    curl --head -X  DELETE  http://[user]:[pass]@nexus.local/repository/docker-private/v2/nexus-prd/manifests/sha256:7e850fbcb9b5bc70cff094383100281f1bdfb5d6d86541b63e181ecd7de1b692

## Monitoring

Being able to monitor containers and whats inside is very important
topic. I’ll be focusing on using **Prometheus** with **Grafana** as a
production grade monitoring system.

### Docker default monitoring

Fortunately docker as some quick tools that can be used for monitoring
docker containers. The command docker stats will show a rather
simplistic table with all the containers running on the system and their
usage.

    docker stats

### Prometheus

While we can user docker stats for simplistic information about the
containers running, it’s not very scalable and its host dependent.
Prometheus is an application that collects metrics and exposes them.

#### Installation

To install prometheus we can run the following command:

    docker run -p 9090:9090 -u="0:980" --name prometheus --mount source=prometheus,target=/prometheus nexus.docker/prom/prometheus --config.file=/prometheus/prometheus.yml

As a small breakdown of the command we have:

-   **-p**: *Port published to by the container;*

-   **-u="0:980"**: *User responsible for the container (rood:docker);*

-   **--name**: *Name of the container*

-   **--mount source=prometheus,target=/prometheus**: *Volume
    (previously created) where to mount prometheus;*

-   **nexus.docker/prom/prometheus**: *Name of the image using nexus
    proxy as a repo*

-   **--config.file=/prometheus/prometheus.yml**: \_\_Path for the
    configuration file (previously created).

The default configuration file used on the first prometheus start:

    global:
      scrape_interval:     15s # By default, scrape targets every 15 seconds.

      # Attach these labels to any time series or alerts when communicating with
      # external systems (federation, remote storage, Alertmanager).
      external_labels:
        monitor: 'codelab-monitor'

    # A scrape configuration containing exactly one endpoint to scrape:
    # Here it's Prometheus itself.
    scrape_configs:
      # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
      - job_name: 'prometheus'

        # Override the global default and scrape targets from this job every 5 seconds.
        scrape_interval: 5s

        static_configs:
          - targets: ['localhost:9090']

After we start prometheus container with this command line we will be
only collecting metric on prometheus itself since we haven’t add
container metrics to it.

> **Note**
>
> Prometheus needs to be able to collect metrics from other containers
> so have in mind we have to add him to a container network.

#### Getting Container Metrics (**cAdvisor**)

Now that we have prometheus installed we need to extract some
container/host metrics. **cAdvisor** is a tool made by docker that
allows us to see container metrics via browser and expose them to
prometheus.

We can install **cAdvisor** very easily by running a container.
**cAdvisor** requires access to some host folders to be able to retrieve
the host/container metrics

    docker run   --volume=/:/rootfs:ro   --volume=/var/run:/var/run:rw   --volume=/sys:/sys:ro   --volume=/var/lib/docker/:/var/lib/docker:ro   --publish=8080:8080   --detach=true   --name=cadvisor -u="0:980"  nexus.docker/google/cadvisor

The command is rather straight forward but its important to notice the
folders we mount for the container. Mounting all those system folders
will allow **cAdvisor** to monitor the host system.

> **Note**
>
> For **Prometheus** to see **cAdvisor** metrics we need to add them
> both to the same network

#### Getting Host Metrics (**node-exporter**)

**Ñode-exporter** is a prometheus exporter for host metrics. Since we
are running **node-exporter** as a container we need to mount serveral
files in the host machine to be able to recover metrics. We’re trying to
get the host metrics and not the containers. It’s good practices to run
**node-exporter** on the host itself instead of a docker container.

The command to run **node-exporter** as a container goes as follows

    docker run -p 9100:9100 --name node-exporter -u="0:980" -v "/proc:/host/proc" -v "/sys:/host/sys" -v "/:/rootfs" --net="backend" nexus.docker/prom/node-exporter --path.procfs /host/proc --path.sysfs /host/proc --collector.filesystem.ignored-mount-points "^/(sys|proc|dev|host|etc)($|/)"

We need to add an entry on the prometheus configuration file so it
receives **node-exporter** metrics.

> **Note**
>
> Don’t forget to add **node-exporter** to the **Prometheus** docker
> network

### Grafana

We installed **Prometheus** and all the metrics exporters. Now we just
need a visual interface to represent those metrics. **Grafana** is a
platform for beautiful analytics and monitoring. It can collect metrics
from prometheus and expose them on a proper format for easy monitoring.

To install grafana use the following command:

    docker run -d -p 3000:3000 -u="0:980" --name grafana --mount source=grafana,target=/var/lib/grafana nexus.docker/grafana/grafana

> **Warning**
>
> The **Grafana** mount volume doesn’t contains the configuration file.
> For further configuration we must mount a configuration file
> explicitly or copy the configuration file into the **Grafana**
> container.

#### LDAP authentication

To be able to use ***LDAP*** with **Grafana** we need to enable it on
the configuration file.

First we need to get the configuration file from inside the container. I
suggest copying it into the mounted volume.

    docker cp grafana:/etc/grafana/grafana.ini grafana.ini

Now we can edit the file and turn **LDAP** on.

    ...
    [auth.ldap]
    enabled = true
    config_file = /etc/grafana/ldap.toml
    allow_sign_up = true
    ...

Now we send the edited file back into the container:

    docker cp grafana.ini grafana:/etc/grafana/grafana.ini

Now that we enable **LDAP** the **LDAP** configurations are stored on
the file `ldap.toml` as we stated on the `grafana.ini` file. Copy the
file `ldap.toml` from the container.

    docker cp grafana:/etc/grafana/ldap.toml ldap.toml

Sample for the **LDAP** configuration:

    # To troubleshoot and get more log info enable ldap debug logging in grafana.ini
    # [log]
    # filters = ldap:debug

    [[servers]]
    # Ldap server host (specify multiple hosts space separated)
    host = "dsjxxi.grupocgd.com"
    # Default port is 389 or 636 if use_ssl = true
    port = 389
    # Set to true if ldap server supports TLS
    use_ssl = false
    # Set to true if connect ldap server with STARTTLS pattern (create connection in insecure, then upgrade to secure connection with TLS)
    start_tls = false
    # set to true if you want to skip ssl cert validation
    ssl_skip_verify = false
    # set to the path to your root CA certificate or leave unset to use system defaults
    # root_ca_cert = "/path/to/certificate.crt"
    # Authentication against LDAP servers requiring client certificates
    # client_cert = "/path/to/client.crt"
    # client_key = "/path/to/client.key"

    # Search user bind dn
    bind_dn = "cn=C095208,ou=SSI,ou=Utilizadores,ou=Servicos-Centrais,ou=CGD,dc=GrupoCGD,dc=com"
    # Search user bind password
    # If the password contains # or ; you have to wrap it with triple quotes. Ex """#password;"""
    bind_password = '[PASSWORD]'

    # User search filter, for example "(cn=%s)" or "(sAMAccountName=%s)" or "(uid=%s)"
    search_filter = "(cn=%s)"

    # An array of base dns to search through
    search_base_dns = ["dc=GrupoCGD,dc=com"]

    ## For Posix or LDAP setups that does not support member_of attribute you can define the below settings
    ## Please check grafana LDAP docs for examples
    # group_search_filter = "(&(objectClass=posixGroup)(memberUid=%s))"
    # group_search_base_dns = ["ou=groups,dc=grafana,dc=org"]
    # group_search_filter_user_attribute = "uid"

    # Specify names of the ldap attributes your ldap uses
    [servers.attributes]
    name = "givenName"
    surname = "sn"
    username = "cn"
    member_of = "memberOf"
    email =  "email"

    # Map ldap groups to grafana org roles
    [[servers.group_mappings]]
    group_dn = "CN=GDLSSIL-DSA-USA51-WAI-IPS,OU=SSI,OU=Utilizadores,OU=Servicos-Centrais,OU=CGD,DC=GrupoCGD,DC=com"
    org_role = "Admin"
    # To make user an instance admin  (Grafana Admin) uncomment line below
    grafana_admin = true
    # The Grafana organization database id, optional, if left out the default org (id 1) will be used
    org_id = 2

    [[servers.group_mappings]]
    group_dn = "cn=users,dc=grafana,dc=org"
    org_role = "Editor"

    [[servers.group_mappings]]
    # If you want to match all (or no ldap groups) then you can use wildcard
    group_dn = "*"
    org_role = "Viewer"

-   **bind\_dn**: *User to login into the active directory to retrieve
    the list of users. (Recommend using a functional user so we don’t
    have to change the password every X months)*

-   **[[servers.group\_mappings]]**: *We can add LDAP groups permission
    following the example*

Now we just copy the file into the container:

    docker cp ldap.toml grafana:/etc/grafana/ldap.toml

All other configurations and dashboard building can be made from inside
the application.

## SpringBoot Containers

Now that we have our container infrastructure ready to be used we need
to build them into docker images.

### Dockerfile

To build images docker uses a file called `Dockerfile` as a
configuration file with a set of commands to be run to generate the
image. `Dockerfile` variates a lot from image to image with different
levels of complexity but **springboot** containers are rather simple to
produce.

    # Uses the base image from alpine for a smaller container
    # nexus.docker is the registry where to get that image from
    FROM nexus.docker/openjdk:8-alpine

    # Volume where tomcat writes a bunch of stuff

    VOLUME /tmp
    # Copies app.properties from the current folder to the docker image
    COPY app.properties app.properties

    # Path where we have our libs and classes
    ARG DEPENDENCY=target/dependency

    # Copies all the necessary files into the image
    COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
    COPY ${DEPENDENCY}/META-INF /app/META-INF
    COPY ${DEPENDENCY}/BOOT-INF/classes /app

    # Sets the run command as the spring boot application class
    ENTRYPOINT ["java","-cp","app:app/lib/*","com.cgd.wai.SBApplication"]

In the previous docker file we are assuming the needed files are
contained in the folder **target/dependency**. We know that by default
we don’t have that folder. We can either unpack the **springboot** jar
manually or we can tell maven to do it for us. In the spring boot
documentation ? the do it manually so here I’ll show how to do it with
maven.

To configure maven to unpack the jar add the following plugin
configuration to the `pom.xml`:

                      <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-dependency-plugin</artifactId>
                            <executions>
                                <execution>
                                    <id>unpack</id>
                                    <phase>package</phase>
                                    <goals>
                                        <goal>unpack</goal>
                                    </goals>
                                    <configuration>
                                        <artifactItems>
                                            <artifactItem>
                                                <groupId>${project.groupId}</groupId>
                                                <artifactId>${project.artifactId}</artifactId>
                                                <version>${project.version}</version>
                                                <outputDirectory>${project.build.directory}/dependency</outputDirectory>
                                            </artifactItem>
                                        </artifactItems>
                                    </configuration>
                                </execution>
                            </executions>
                        </plugin>

With all done we can just build an image by calling the docker daemon:

    docker build -t nexus.private/sbalnova .

# References

# 

[1] Docker Installation -
<https://docs.docker.com/install/linux/docker-ce/centos/>

[2] Docker Volumes - <https://docs.docker.com/storage/volumes/>

[3] Docker Networks - <https://docs.docker.com/network/>

[4] Docker Bridge Networks - <https://docs.docker.com/network/bridge/>

[5] Httpd Docker Container - <https://hub.docker.com/_/httpd>

[6] Nexus Docker Container - <https://hub.docker.com/r/sonatype/nexus3/>

[7] Nexus as a docker repository -
<https://blog.sonatype.com/using-nexus-3-as-your-repository-part-3-docker-images>

[8] Nexus behind a reverse proxy -
<https://help.sonatype.com/repomanager3/installation/run-behind-a-reverse-proxy>

[9] Docker Certificates -
<https://docs.docker.com/engine/security/certificates/>

[10] Spring Boot Containers -
<https://spring.io/blog/2018/11/08/spring-boot-in-a-container>
