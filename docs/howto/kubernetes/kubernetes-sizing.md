# Kubernetes Sizing

CGD canais v1.0 :toc: :sectnums:

> Small documentation regarding the sizing for a production grade
> kubernetes cluster.

## Kubernetes structure and recommended size

A production grade overall kubernetes structure would be as following:

![Kubernetes structure](./assets/images/api-server.png)

-   **Master**: Typically contains the `API Server`,
    `Controller Manager`, `Scheduler` and `etcd`. High availability and
    large clusters might require `etcd` to be run on independent nodes.
    All the components can be run as containers and be supervised by
    kubernetes (static pods)

-   **Worker**: Contains the `containers` that run under the kubernetes
    network

### Recommended Size per component

![Recommended size per
component](./assets/images/kubernetes-components-ha.png)

Since all the main components are located inside the master instance (as
a starting phase we should include `ectd` on the master node) we require
a minimum of 3 master master instances to run a proper kubernetes
cluster.

Workers are added depending on the working load. For a typical system
the more instances the more redundancy can be added. Although more
instances are better two are enough to test most redundancy mechanisms.

Just 1 access instance would be enough since its not critical and might
not even be used from the start.

### etcd

### Dual host deployments

![Recommended dual host
configurations](./assets/images/dual-host-deployment.png)

### Triple host deployments

# References

# 

[How to Build an Enterprise Kubernetes Strategy, White Paper, Updated
July
2019](./assets/attachments/How to Build an Enterprise Kubernetes Strategy, White Paper, Updated July 2019.pdf)

<https://kubernetes.io/blog/2018/08/03/out-of-the-clouds-onto-the-ground-how-to-make-kubernetes-production-grade-anywhere/>
