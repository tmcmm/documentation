# GitLab Git Ops pipelining

## GitOps
GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.<br>

Modern applications are developed with speed and scale in mind. Organizations with a mature DevOps culture can deploy code to production hundreds of times per day. DevOps teams can accomplish this through development best practices such as version control, code review, and CI/CD pipelines that automate testing and deployments.<br>

GitOps is an operational framework that can automate this process of provisioning infrastructure. Similar to how we use application source code, infrastructure teams that practice GitOps use configuration files stored as code (infrastructure as code). GitOps configuration files generate the same infrastructure environment every time it’s deployed, just as application source code generates the same application binaries every time it's built.<br>

## Gitops Pipelining

In order to create a pipeling on gitlab you have to create a yaml file starting with a dot for example __.gitlab-ci.yml__<br>
The best pratice it is to create a project that has all the functions you will need to invoke on your main yaml file. That way everything is cleaner and can be escalated easily<br>

For example a function that builds a maven project:<br>
```
variables:
  MAVEN_CLI_OPTS: '-s maven_config/settings.xml --batch-mode -Dmaven.test.failure.ignore=true -Dmaven.wagon.http.ssl.insecure=true'
  
Build APP:
  image:
    name: 'nexus.grupocgd.com:8444/cgd/jenkins-slave-maven'
    entrypoint:
      - /bin/bash
  stage: Build App
  before_script:
    - >-
      export
      HTTP_PROXY="http://${dockerUser}:${dockerPassword}@proxy.grupocgd.com:8080"
    - >-
      export
      HTTP_PROXY="http://${dockerUser}:${dockerPassword}@proxy.grupocgd.com:8080"
    - export NO_PROXY="repo.maven.apache.org"
  script:
    - echo "Iniciando build da aplicacao..."
    - mvn $MAVEN_CLI_OPTS clean install 
  artifacts:
    expire_in: 1 day
    when: always
    paths:
      - $PATH_TO_JAR
  only:
    - develop
    - pre-release
```
function that deploys to rancher:<br>
```
variables:
  RANCHER_URL: 'https://rancher.grupocgd.com'
  RANCHER_NAMESPACE_TU: ''
  RANCHER_NAMESPACE_TI: ''
  RANCHER_NAMESPACE_CQ: ''
  RANCHER_NAMESPACE_PR: ''
  RANCHER_WORKLOAD: ''
  APP_VERSION: ''   
  PROMETHEUS_PATH: '/actuator/prometheus'
  PROMETHEUS_PORT: 8080
  PROMETHEUS_SCRAPE: 'true'

Deploy to Rancher - TI:
  image:
    name: 'nexus.grupocgd.com:8444/cgd/jenkins-docker-slave'
  stage: Deploy Rancher TI
  script:
    - date -d '+1 hour' '+%F %T' > dateNow
    - DATE_NOW=$(cat dateNow)
    - >-
      curl
      "$RANCHER_URL/v3/project/c-pb4f4:p-6nvxc/workloads/deployment:$RANCHER_NAMESPACE_TI:$RANCHER_WORKLOAD"
      -X PUT -H "Authorization: Bearer $rancherNameToken:$rancherToken"  -H "Accept-Encoding: gzip, deflate, br"  -H "Connection: keep-alive"  -H
      "Pragma: no-cache"  -H "Cache-Control: no-cache"  -H "content-type:application/json"  -H "accept: application/json" --data-binary
      "{\"id\":\"deployment:$RANCHER_NAMESPACE_TI:$RANCHER_WORKLOAD\",\"annotations\":{\"cattle.io/deployDate\":\"$DATE_NOW\",\"cattle.io/appVersion\":\"$APP_VERSION\",\"cattle.io/pipelineGitlab\":\"$CI_PIPELINE_URL\",\"prometheus.io/path\":\"$PROMETHEUS_PATH\",\"prometheus.io/port\":\"$PROMETHEUS_PORT\",\"prometheus.io/scrape\":\"$PROMETHEUS_SCRAPE\"}}}"
      --compressed
  only:
    - pre-release
```
function that has other functions as dependency:<br>
```
include:
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .function-buildDocker-TU.yml
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .function-buildDocker-TI.yml    
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .function-deployRancher-TU.yml
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .function-deployRancher-TI.yml
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .function-buildMaven.yml    
    
stages:
  - Build App
  - Docker TU
  - Docker TI
  - Deploy Rancher TU
  - Deploy Rancher TI

variables:
  DOCKER_HOST: 'tcp://localhost:2375'
  DOCKER_DRIVER: overlay
  DOCKER_TLS_CERTDIR: ''
  RANCHER_URL: 'https://rancher.grupocgd.com'
  RANCHER_NAMESPACE_TU: ''
  RANCHER_NAMESPACE_PR: ''
  RANCHER_WORKLOAD: ''
  APP_VERSION: ''  
  MAVEN_CLI_OPTS: '-s maven_config/settings.xml --batch-mode -Dmaven.test.failure.ignore=true'
  
services:
  - 'nexus.grupocgd.com:8443/gitlab/docker_dind'
```
This way we can use these functions on our main yaml file like:<br>
```
variables:
  APP_NAME: agilejob-relatorioscreditosempresas
  APP_VERSION: 1.0.0
  PATH_TO_JAR: ${APP_NAME}-App/target/${APP_NAME}-App.jar
  DOCKER_REGISTRY: registry.grupocgd.com
  DOCKER_IMAGENAME: $APP_NAME
  DOCKER_TAG_TU: tu
  DOCKER_TAG_TI: ti
  DOCKER_TAG_CQ: cq
  DOCKER_TAG_PR: pr
  RANCHER_NAMESPACE_TU: agile-tu
  RANCHER_NAMESPACE_TI: agile-ti
  RANCHER_NAMESPACE_CQ: agile-cq
  RANCHER_NAMESPACE_PR: agile-pr
  RANCHER_WORKLOAD: $APP_NAME

include:
  - project: 'AGILE/gitlab-ci-functions'
    ref: master
    file: .pipeline-buildAndDockerAndDeploy.yml
```

For example if you have an ansible project that it need to be launched on ansible tower and has a dependency of a maven build on other project you can:<br>

```
stages:
  - build
  - deploy

build:
  stage: build
  script:
  - echo "Building App!"
  - "curl -X POST -F token=$ci_job_token -F ref=master https://gitlab.grupocgd.com/api/v4/projects/575/trigger/pipeline"

deploy_dev:
  stage: deploy
  image: nexus.grupocgd.com:8444/cgd/ansible
  script: "awx --conf.host https://awx.grupocgd.com --conf.token $awx_token \
  job_template launch ansible-job-sdghp-deploy-dev --monitor"
  environment:
    name: desenvolvimento
    url: https://api-sdghp.dev.grupocgd.com
  only:
  - desenvolvimento

deploy_qld:
  stage: deploy
  image: nexus.grupocgd.com:8444/cgd/ansible
  script: "awx --conf.host https://awx.grupocgd.com --conf.token $awx_token \
  job_template launch ansible-job-sdghp-deploy-cq --monitor"
  environment:
    name: qualidade
    url:  https://api-sdghp.qld.grupocgd.com
  only:
  - qualidade

deploy_prd:
  stage: deploy
  image: nexus.grupocgd.com:8444/cgd/ansible
  script: "awx --conf.host https://awx.grupocgd.com --conf.token $awx_token \
  job_template launch ansible-job-sdghp-deploy-pr --monitor"
  environment:
    name: producao
    url: https://api-sdghp.grupocgd.com
  when: manual
  only:
  - producao
```
You will find the project id for the curl command below your project main page
![project-id](assets/webservices.png "webservices")
