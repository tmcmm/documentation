# GitLab Pages HowTo

## Introdução

Esta feature do GitLab permite a criação de sites estáticos apartir de um reposítório GitLab. Em conjunto com a ferramenta mkdocs permite apartir de documentos markdown gerar sites pré-formatados em html para expôr ou disponibilizar documentação.

Descrição do serviço GitLab Pages: <https://about.gitlab.com/stages-devops-lifecycle/pages/>

Descrição do serviço MKDocs: <https://www.mkdocs.org/>

## Criar um projeto e site de exemplo com mkdocs

### Pré-requisitos

Será necessário acesso e correr as seguintes imagens docker: 

- nexus.grupocgd.com:8444/cgd/cookiecutter:latest
- nexus.grupocgd.com:8444/cgd/mkdocs:latest

É mais simples efetuar clone dos projectos git referentes às imagens pois já contêm um script wrapper para executar os comandos necessários:

- https://gitlab.grupocgd.com/docker/docker-image-cookiecutter.git
- https://gitlab.grupocgd.com/docker/docker-image-mkdocs.git

### Criar um novo projecto git recorrendo a um template cookiecutter


    docker-image-cookiecutter/cookicutter.sh -f https://gitlab.grupocgd.com/ast/templates/cookiecutter-mkdocs.git

Os campos do templates são pedidos e devem ser preenchidos de acordo com os requisitos do projecto

    Using cookicutter container version 1.1
    Username for 'https://gitlab.grupocgd.com': c080225
    Password for 'https://c080225@gitlab.grupocgd.com': 
    project_name [example-docs]:          
    site_name [Example Site]: 
    repo_url [https://gitlab.grupocgd.com/docs/example-docs]: 


### Utilizar o mkdocs para desenvolver e efetuar preview do site

1. Dentro da directoria do projecto example-docs executar:

        ../docker-image-mkdocs/exec.sh serve -a 0.0.0.0:8000

Output:

        Using mkdocs container version 1.4
        INFO    -  Building documentation... 
        INFO    -  Cleaning site directory 
        INFO    -  Documentation built in 0.31 seconds 
        [I 200322 18:51:30 server:296] Serving on http://0.0.0.0:8000
        INFO    -  Serving on http://0.0.0.0:8000
        [I 200322 18:51:30 handlers:62] Start watching changes
        INFO    -  Start watching changes
        [I 200322 18:51:30 handlers:64] Start detecting changes
        INFO    -  Start detecting changes

2. Aceder ao site de preview em <http://localhost:8888>, se o docker utilizado estiver instalado no teu PC local.

### Push do projecto para o gitlab para geração do site

1. Para efeitos deste tutorial criei um projeto pessoal no GitLab em https://gitlab.grupocgd.com/Rui_Alves/example-docs.git (Não esquecer de clicar na opção "Initialize repository with a README")

2. Apontar projecto local para o gitlab. Dentro da directoria criada pelo template cookiecutter: 

        git init
        git add --all
        git commit -m "Versão inicial"
        git checkout -b "initial_version"
        git remote add origin https://gitlab.grupocgd.com/Rui_Alves/example-docs.git
        git push --set-upstream origin initial_version


!!!Note "Nota"

    De seguida é necessário criar um merge request no GitLab com origem no branch "initial_version" para o "master".
    Após este passo o pipeline definido em .gitlab-ci.yml é executado automáticamente e o site é gerado ficando disponivel, neste caso em: <https://rui_alves.pages.grupocgd.com/example-docs/>

    

    