__Check git version:__
```
git --version 
```
__git initial configuration:__
```
git config --global user.name "username"
git config --global user.email "usermail@gmail.com"
git config --list
git config --global credential.helper wincred(para lembrar as credenciais do git)
```
__initiate repository:__
```
 git init
```
__Stop tracking on a local repository:__
```
rm -rf .git
```
__Before making the first commit:__
```
git status
```
__Files that we want git to ignore:__
```
touch .gitignore (and we put the projects in there using a text editor, even using wildcards - ex: *. txt)
```
__Add files to the staging area:__
```
git add (file name)
git add -A (adds everyone from the directory)
```
__Remove files from the staging area:__
```
git reset (file name)
git reset (removes all files)
```
__First commit:__
```
git commit -m "Comment: my first commit"
```
__See the logs of commits made on git:__
```
git log
```
__Clone a remote repository to this local directory:__
```
git clone https://githubexample.com.
```
__Clone a specific branch to a local board:__
```
git clone -b <_name_branch> git url <paste_name>
```
__View the remote repository information:__
```
git remote -v
git branch -a (see remote and local repository information)
```
__Add a remote repository:__
```
git remote add origin git@lpc6001cit04.grupocgd.com: UCX4-Channels / uim.git
git remote set-url origin git@lpc6001cit04.grupocgd.com: UCX4-Channels / uim.git
```
__Take a look at the changes made to the files:__
```
git diff (shows the changes made to the code / files)
git status (shows files that have been changed)
git add -A (add these files for staging)
git commit -m "Files with changes"
```

__Push the code:__
```
git pull origin master (Pulls the code in case other collaborators have made changes to the code in the meantime since the last pull)
git push origin master (Pushes the code we changed to the remote repository)
```
__Push to a specific branch:__
```
git push <local_name_repository_name> master: <_name_branch>
```
__Push a specific branch to master:__
```
git push origin <branch>: master
```
__Create a tag associated with the code:__
```
git tag -a v1.4 -m 'version 1.4'
```
__See the tags associated with the code:__
```
git tag -l <pattern>
```
__Create a branch to work on a separate branch without ruining the collaborators' project:__
```
git branch example (Create a branch named example)
git branch (lists all branches of the project and shows the current branch we are working on)
git checkout example (changes the desktop to the newly created branch called example)
```
__After making the commits we will push this new branch:__
```
git push -u origin example (pushes the new local branch to the remote branch, in this case it is the example branch)
git branch -a (lists information from local and remote repositories)
```
__To join the branches:__
```
git checkout master (we moved our workspace to this directory)
git pull origin master (we pull if there is any change since the last pull)
git branch --mergerd (shows branches that have already been merged)
git merge example (being on the master branch we merge the example branch created previously to the local master branch)
git push origin master (we push from the local master branch to the remote repository)
```
__Delete the branch:__
```
Locally: git branch -d example (locally delete the previously created branch, since we merge with the master we no longer need this example branch)
git branch -a (list the repository information and it is visible that the branch still exists remotely, however, the location has been deleted)
git push origin --delete example (deletes the branch in the remote repository)
git branch -a (we verify that it has now been deleted locally and remotely)
```

__git change the name of the remote branch:__
```
git branch -m old_branch new_branch # Rename branch locally
git push origin: old_branch # Delete the old branch
git push --set-upstream origin new_branch # Push the new branch, set local branch to track the new remote
```

__For Windows Integration:__<br>
Download git: [Git Homepage](https://git-scm.com/downloads "Git Homepage")
Git comes with an integration client called git bash (where you can execute all the commands described above):
![git-bash](assets/git-bash.png "git bash")

Attached is a troubleshooting if you cannot authenticate when pushing or pulling the code repository.

### In case of authentication failure:
If an authentication failure occurs during the process described above, it must be because the machine is closed and does not allow the creation of a credential in the “Windows Credential Manager”: <br>

![generic-credential](assets/generic-credential.png "generic credential")

There are two options:<br>
- Create a “Generic Credential”<br>
- Configure access via SSH (see topic later in this document)<br>

To overcome the problem, a “Generic Credential” must be created with the following data:<br>
__Internet or network address:__ [Gitlab Homepage](https://gitlab.grupocgd.com "Gitlab Homepage")<br>
__Username:__ User access<br>
__Password:__ Password to access gitlab<br>
 
## Troubleshooting
 
Errors that may arise when using git.:<br>

__fatal: refusing to merge unrelated histories__
This error indicates that Git considers local files to be different from remote files (another branch).<br>

```
git pull origin master
warning: no common commits
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://gitlab.grupocgd.com/DCP/
* branch master -> FETCH_HEAD
* [new branch] master -> origin / master
```
output:<br>
__fatal: refusing to merge unrelated histories__

To solve the problem, you will need to run the command:<br>

```
git pull origin master --allow-unrelated-histories
```
It will merge the local files with those in the central repository.<br>
### Configure push without credentials
To avoid the need to enter credentials whenever a new push is made, access via SSH can be configured (with the addition of the public key).<br>

Steps to implement:<br>
- Generate the key pair (if it does not exist) on the client (command line git bash) with the command:<br>

```
$ ssh-keygen -t rsa -C "tiago.miguel.mendes@cgd.pt"
```

where argument C is a comment, in this example we use email as comment<br>

- After generating the key, we will copy it to the gitlab server for the profile [menu: Profile-Settings -> SSH Keys -> Add SSH Key]<br>
To copy the key we will list it in the command-line window and then copy-paste it to the Git window in the browser:<br>

```
$ cat ~ / .ssh / id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRyEhTEZVfdv0zaH9A4K2NH83NSAakURLXIxEtTHk7eD0wvsn9wn1uSzLCG3XgQFMzee4F OZNeVu6vXKJ1zbwz3hERjk44Q Q5TdGXd2YVeJLSc87XPGnKebRjn0RLyDpeO9bHjyHRAUcdaoC0Co1FEV WTXqrs4Hi6sdEbJDklw9UeYKDx2laaJQeDnVnarCdt1Ak6OvgVhxL1RXo9N0ubEbzDxZl4cSfLbnoAvKI(...)
```
Putting the key in GitLab (browser):<br>
![gitlabssh-credential](assets/ssh-key.png "gitlabssh credential")


- After adding the public key to the gitlab profile, we will configure our remote repository. If we have not yet configured the connection between local and remote repository, we use the command:<br>

```
$ git remote add origin git@gitlab.grupocgd.com: <insert_project_name>
```
If we have already configured the remote repository we will just change the pointer so that the communication is done by ssh:<br>
```
$ git remote set-url origin git@gitlab.grupocgd.com: <insert_project_name>
```

- To confirm that the SSH Key is well configured, test an SSH connection using the command:<br>
```
$ ssh -T git@gitlab.grupocgd.com
```
[A Git welcome message should appear]
