
## Install a shared runner on Rancher using Helm3
Log on your:<br>
[Gitlab Homepage - DEV](https://gitlab.dev.grupocgd.com "Git Homepage")<br>
[Gitlab Homepage - PRD](https://gitlab.grupocgd.com "Git Homepage")<br>

With admin access go to:<br>
![gitlab-runner](assets/registering.png "git runner")
![gitlab-runner](assets/registered.png "git runner")
Retrieve the token and the url and save for later use.<br>
__Get server sub entity certificate:__
```
 echo | openssl s_client -servername gitlab -connect gitlab.dev.grupocgd.com:443 2>/dev/null | openssl x509 -text
```
```
-----BEGIN CERTIFICATE-----
MIIHlTCCBn2gAwIBAgITGAAJppe+J+lqzEwYWgADAAmmlzANBgkqhkiG9w0BAQsF
ADBFMRMwEQYKCZImiZPyLGQBGRYDY29tMRgwFgYKCZImiZPyLGQBGRYIR3J1cG9D
R0QxFDASBgNVBAMTC0NHREVudFN1YkNBMB4XDTIwMDQwMjE0NTMyOVoXDTIyMDQw
MjE0NTMyOVowgZcxCzAJBgNVBAYTAlBUMSYwJAYDVQQKEx1DYWl4YSBHZXJhbCBk
ZSBEZXBvc2l0b3MgUy5BLjEaMBgGA1UECxMRU1NJLVByb2plY3RvcyBXRUIxGTAX
BgNVBAMTEGRldi5ncnVwb2NnZC5jb20xKTAnBgkqhkiG9w0BCQEWGnRpYWdvLm1p
Z3VlbC5tZW5kZXNAY2dkLnB0MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKC
AgEA3LsH4g2iczuVifqxJJOE4vNoqsqs7+6s9sH8cGrCQSfgafDeU9kj3nRZfluo
vcEsb/8GkRpImGwLBLbemI4w7e5ADUsZfSkxojA4ogIjsTCn6Pw3y7N2+WkOtDNa
znN/flYUq2B7ILXTQEqqvtQWedKOAZUC1/omokCyYyzofxua7SKFed0BBMLrKERC
2VcbLb97gSJOlBPndjVO/hr6A9NM73KGE2KK1V4UhthSinHLQ+xDStO7kONT0Xth
Jo0mo/ss5Km1pz17SS+mZX4ZmpKHmd94mwNKVeEflueYaaCQ3+R6/u0Gghea4yE6
w1rAzwiuuzzqNu8JnQCqv0k2VH+Ja+Lsnt5DpAXURyTRVk+2TE3JKnudl6IkjRnf
6AacPji0u1plaPPEMS3giTLqMGVchbSpZ0+6qK78RrCDUHxexRj8pMV17MqEr1MF
lCtMp3yCIVy5k+127frEQsdKVe8aR1DSCx7bXCNPMB+QDnyRwnh8Q+WkDZaYuaf/
Jfvn6TKDAOaiXdkoX+IFx2FUnolcxcrmFGgINk97bCL1vqk8qcrQsH352icxBTdB
bZ2fyRCKrf32HbMYg04fHi92glUaccwY0PRRw2csF5TGpsFx6EiEHASEd2t9/uL9
DdtHErcEY/N+hK+/Kbdbpeut2S2a81QmBQmRq8coG4iKGYMCAwEAAaOCAykwggMl
MDQGA1UdEQQtMCuCEGRldi5ncnVwb2NnZC5jb22CA2RldoISKi5kZXYuZ3J1cG9j
Z2QuY29tMB0GA1UdDgQWBBTeD2pVkqgKzjXN8pNgoo/tzi9n4zAfBgNVHSMEGDAW
gBRi5pTyMqQP9QnQEyigeX2K9XmOXDCCAREGA1UdHwSCAQgwggEEMIIBAKCB/aCB
+oaBvWxkYXA6Ly8vQ049Q0dERW50U3ViQ0EoMiksQ049R0NYTkNMSUlDQVMzMDEs
Q049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENO
PUNvbmZpZ3VyYXRpb24sREM9R3J1cG9DR0QsREM9Y29tP2NlcnRpZmljYXRlUmV2
b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2lu
dIY4aHR0cDovL2NhY2VydC5ncnVwb2NnZC5jb20vQ2VydEVucm9sbC9DR0RFbnRT
dWJDQSgyKS5jcmwwggEEBggrBgEFBQcBAQSB9zCB9DCBqwYIKwYBBQUHMAKGgZ5s
ZGFwOi8vL0NOPUNHREVudFN1YkNBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBT
ZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPUdydXBvQ0dE
LERDPWNvbT9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNh
dGlvbkF1dGhvcml0eTBEBggrBgEFBQcwAoY4aHR0cDovL2NhY2VydC5ncnVwb2Nn
ZC5jb20vQ2VydEVucm9sbC9DR0RFbnRTdWJDQSgzKS5jcnQwCwYDVR0PBAQDAgWg
MDsGCSsGAQQBgjcVBwQuMCwGJCsGAQQBgjcVCIW+iE7b0Gi5mzOFod8jhqqoYCuF
sfxogqXaaQIBZAIBBzAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwJwYJ
KwYBBAGCNxUKBBowGDAKBggrBgEFBQcDAjAKBggrBgEFBQcDATANBgkqhkiG9w0B
AQsFAAOCAQEAFAHQP/NADflao/grSjHLld6mK37HJbOWrV8PUnJXTGzDk1JzNRTH
MEtt0oUwQW6qDKsaSy+Psw6Z9DMpJpY5GuJk2chP3K5tt66q2lJyUv1Gofwfx+Qt
bu60XOROSYxOEFKdXYiIcgFIf86ItXdhQDCKjblUSxW80sM3pWZkG05LfyPfv89M
CxmKiMeRCxb2wzvYjP6cOQP7R7kmGKNrJav8zBvpygLx+U6AV2NAVy8dL2b4SfN2
3ovxQrXR+j+ZCHJUe5sEAfW+M2TjZETdYCRBpI8InjqqS0d6pg9iDwaTD4S77YVG
UJQK6ktnWLKAHvWiTcHfOSXjNOTEb2idtw==
-----END CERTIFICATE-----
```
__Pass it to the file that is going to store the secret and run:__
```
kubectl -n gitlab create secret generic cert-ca-grupocgd --from-file=gitlab.dev.grupocgd.com.crt
```
__Create the docker registry secret key to access the nexus proxy:__
```
kubectl -n gitlab create secret docker-registry nexusgroup --docker-server=nexus.grupocgd.com:8443 --docker-username=<user> --docker-password=<pass> --docker-email=<email>
```
__Alter the values of yaml file with the retrieved token:__
```
certsSecretName: cert-ca-grupocgd
checkInterval: 30
concurrent: 10
gitlabUrl: https://gitlab.dev.grupocgd.com/
image: registry.dev.grupocgd.com/cgd/gitlab-runner:alpine-v12.10.2
rbac:
  clusterWideAccess: false
  create: true
runnerRegistrationToken: XW7z_rD7WAxFTUTzYE7x
runners:
  builds:
    cpuRequests: 100m
    memoryRequests: 128Mi
  helpers:
    cpuRequests: 100m
    image: nexus.grupocgd.com:8443/gitlab/gitlab-runner-helper:x86_64-${CI_RUNNER_REVISION}
    memoryRequests: 128Mi
  image: registry.dev.grupocgd.com/cgd/centos7
  imagePullSecrets:
  - nexusgroup
  privileged: true
  services:
    cpuRequests: 100m
    memoryRequests: 128Mi
```
__Add gitlab repo, update and install the runner:__
```
helm3 repo add gitlab https://nexus.grupocgd.com/repository/gitlab-charts/
helm3 repo update
helm3 upgrade --namespace gitlab gitlab-runner -f values.yml gitlab/gitlab-runner --version 0.16.1
```
Check on gitlab and rancher if the runner is well registered and has its image running on the cluster.<br>
![gitlab-runner](assets/rancher-gitlab-runner.png "git runner on Rancher")