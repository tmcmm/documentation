# Ansible HowTo

## Ansible Project

### Create a gitlab ansible playbook project

1. Go to https://gitlab.grupocgd.com and create a new project

    ![gitlab new project](assets/gitlab_new_project.png)

2. The project must have a name in format "ansible-project-\<name\>" and created in gitlab group "awx-projects". By default you should configure this repository as "private".

    ![gitlab new project](assets/gitlab_ansible_playbook_project.png)

### Generate a project skaffold with a cookiecutter template

1.  Clone the new ansible project git project to your workspace and the docker-image-cookiecutter project

        []$ git clone https://gitlab.grupocgd.com/awx-projects/ansible-project-example.git
        [}$ git clone https://gitlab.grupocgd.com/docker/docker-image-cookiecutter.git

2.  Use cookicutter script to create a project skaffold for your ansible project

        []$ docker-image-cookiecutter/cookicutter.sh -f https://gitlab.grupocgd.com/ast/templates/cookiecutter-ansible-project.git
        Using cookicutter container version 1.1
        Using cookicutter container version 1.1
        Username for 'https://gitlab.grupocgd.com': c080225
        Password for 'https://c080225@gitlab.grupocgd.com': 
        project_name [ansible-project-example]: ansible-project-example


## Ansible Roles

### Create a gitlab ansible role project

1. Go to https://gitlab.grupocgd.com and create a new project

    ![gitlab new project](assets/gitlab_new_project.png)

2. The project must have a name in format "ansible-role-\<name\>" and created in gitlab group "ansible-roles". By default you should configure this repository as "private".

    ![gitlab new project](assets/gitlab_ansible_role_project.png)

### Generate a project skaffold with a cookiecutter template

1. Clone the new ansible role git project to your workspace and the docker-image-cookiecutter project

        []$ git clone https://gitlab.grupocgd.com/ansible-roles/ansible-role-example.git
        [}$ git clone https://gitlab.grupocgd.com/docker/docker-image-cookiecutter.git

2. Use cookicutter script to create a project skaffold for your ansible role

        []$ docker-image-cookiecutter/cookicutter.sh -f https://gitlab.grupocgd.com/ast/templates/cookiecutter-ansible-role.git
        Using cookicutter container version 1.1
        Username for 'https://gitlab.grupocgd.com': c080225
        Password for 'https://c080225@gitlab.grupocgd.com':
        role_name [test_role]: ansible-role-example
        your_name [some guy]: Rui Alves
        role_description [some role description]: This is a ansible role to install and configure example software

