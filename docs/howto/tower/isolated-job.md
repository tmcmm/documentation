## Isolated Jobs Launched on Rancher
***[Ansible Runner version 1.4.4]***

- First: You have to create a bearer token to access the cluster in the rancher api

![Api-Key](assets/api_key.png)

- Second: You have to create a new credential type Openshift or Kubernetes API Bearer Token in the [AWX Homepage](https://awx.grupocgd.com "AWX Homepage")<br> 

![Credential-Type Bearer](assets/cluster-lab-bearer.png)

Get ca-cert-authority data from the cluster:<br>
```
kubectl config view --minify --raw --output 'jsonpath={..cluster.certificate-authority-data}' | base64 -D | openssl x509 -text -out -
```
Get the Cluster api endpoint:<br>
```
kubectl config view --minify | grep server | cut -f 2- -d ":" | tr -d " "
```
__api endpoint of cluster-lab-upgrade:__<br>
https://rancher.cqgrupocgd.com/k8s/clusters/c-58xl5<br>
__ca-cert:__
```
-----BEGIN CERTIFICATE-----
MIIFMjCCAxqgAwIBAgIQSrgQdZFOuaRNd0Q95JmU2TANBgkqhkiG9w0BAQsFADAZ
MRcwFQYDVQQDEw5HcnVwb0NHRFJvb3RDQTAeFw0wOTA0MDgxNDU0MjBaFw0zNjA1
MDQxNDI4MThaMBkxFzAVBgNVBAMTDkdydXBvQ0dEUm9vdENBMIICIjANBgkqhkiG
9w0BAQEFAAOCAg8AMIICCgKCAgEAqIsmvDwTzb3rlb08G1xgaa3644z29LKQqSkJ
euPzr0xZ4+tUkIGupIYjxL8mWL8nfNsbihltnOnA1qzhZXIs8+vdqPdsShIQOwrF
GXsZwlvlRAoIkyoZpQxPjbrz3UgcKE5Y1aqBAQCsVcKjsxuCakISoR/daXV2QJX/
V7hT4sF9K8qnC/exp0uWcnuorQjKtqlfr9HQMC9HnihpGocV1lo4OLXdqbG/Rux/
BllnYqDdj8YZQKD2hBsJUZP2xUTo9F5NUY/Mta9ubjHuphxPUYVAnnLpG4xAgvRX
IEH4f/GoJ/FEXN8ozFVeRmnJ/XYtL7vJ0ot5rsVVrF+7mAIT7OC04uMn6exZMI13
143fwu/rHKjNhxy/t2c5TumuDktfHeqSvfcwDBU7znaqVLhSz1k488z733b/O2yn
1asA9bV/bDGUwhTTZJhZQOXHlN9tyfjRuLZP1Jljq3wJqpw+YcbxBfpuYIRJPWCd
1owYgtgQBi0zqJ7mMeFsvpqSRNasBchUm2wBBDYC2QW9KXzKv5fjAGR3ea5IcvaU
DUio2Vy23QGh4ffqwGQqscGu6fbEUZHtdI2Ue5VREuvN3a7NtM0KYrVFFZOcmRHY
oUlbecUN52jt/0r08xH2HNaBZLsV6LxrpY6qIpRfOr61lOA1D6YmsSgdmHHCzFMh
Gn9FOGsCAwEAAaN2MHQwCwYDVR0PBAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYD
VR0OBBYEFCSpjA3GccA8ofrmPVEMzSmxvuG2MBAGCSsGAQQBgjcVAQQDAgEBMCMG
CSsGAQQBgjcVAgQWBBR5zLUxC40YPF0eNerKsLLfqQhOxTANBgkqhkiG9w0BAQsF
AAOCAgEAcREQUCgl5bm6dz1sy+4BW6XxTCB62m2DXH+EAL/PxV+KkZyKP6xZJVOe
YZu6CZnn2jtUTd2y3PBLRpmll5O5de4+4yceOWB92pcvLv/psv7BAvu0aaDfdlR6
HTuELcNx4tuy2QW5TamsiCweFPVDQabvSpL6FfP0lhbnTbOXyiS20Ci9TxC6QBok
enjXR5Ic/O8dLrAXk4nY1BcbCOMiOVzSj4imYwlqCe1Aa6gi8jCIbDTPKVRlH4t9
D8U5AsqnRPEGVbNk2Ib8rdior5t8ucSGHIca/spjqsFle3g3sYydQN9mPrCgFsIt
5Q5KB5S61KEgLrgL3OwaSAquJ7O2ooFkf/d4dE/1mAAswMNXN6YMCaZf7Sn+9khN
Ua/yPNUvz5m0bAH1HrWe+ATuERjZ4PtrCUOG+v84ne42r9crXrFFMgCFu0QUe1e7
GwdflULUJe7k6UWE0N9w1NiZfBWnJuupPm5nD9ucVJc8cTqNOnT52L/cC2Vndlwj
jrkkBU029tYFvBm2GIs9gxwsxkcDkwhwk5NBNTezubXzHQNSxSpw66NO76TFf+VK
cYhBXlLEfEuu7EXTw7JCK0GbTwgx5cPDrhyAjRNrrPWAaDk2crOu+wIiVNb1IDGn
K033JhWeSjzjdl1m9Ev4kCNzE3F6L+Nw1ib83cXgVanVtwBJVIY=
-----END CERTIFICATE-----
```
- Third: Create the Instance Group on AWX with the image of ansible-runner:
![Instance Group](assets/instance_group.png)
```
apiVersion: v1
kind: Pod
metadata:
  namespace: tower
spec:
  containers:
    - image: 'registry.dev.grupocgd.com/cgd/quay.io/ansible-tower/ansible-runner:1.4.4'
      tty: true
      stdin: true
      imagePullPolicy: Always
      args:
        - sleep
        - infinity
```
- Fourth: Create the inventory with the new Instance_Group:<br>
![inventory_rancher](assets/awx_inventory.png)

__Create tower namespace with the default cpu limits:__
```
kubectl create namespace tower
```
__default cpu limits:__
```
apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-limit-range
spec:
  limits:
  - default:
      cpu: 1
    defaultRequest:
      cpu: 0.5
    type: Container
```
run command:
```
kubectl apply -f filename.yaml -n tower
```
For the last part of this how to you will need to launch a template pointing to the last inventory file created<br>
![template](assets/job_successfull.png)
![rancher-job](assets/rancher_job.png)