## Migrate data from AWX to Ansible Tower 
***[from AWX version 1.0.1.17 to Tower 3.6.2]***

**Note:** Ansible version will also change from **Ansible 2.7.6** to version **Ansible 2.9.1**

This procedure has been tested on a lab environment with the software versions mentioned above.
Should be a once-only operation since AWX will be abandoned.

### What you will need: ###
* An installation of AWX (running on docker)
* An installation of Tower (running on Centos/RHEL)
* Credentials to access both the origin and destination postgres databases (AWX and Tower)
* A third server where you can install AWX (docker) and a postgres DB (all data copied from origin DB).
  This will be used to convert all data before it can be imported into Tower
* A linux box (Desktop/Server/Container) where you can install client-tools to get data from AWX and insert data into Tower


## 1. Install AWX 2.0 on the third server ##
You may use the server ldc6001aps45 - check the */deploy/awx_dev_2.0.0/awx-2.0.0/awx-2.0.0* directory, where you can find the binaries for AWX 2.0.0
Stop the current 1.0 AWX instance (preferably, but not mandatory) and copy all files and folders from the "data" postgres directory
(you can zip/tar it if it is stopped).


unpack all files that were copied from the origin AWX DB and replace the "data" postgres directory on the third server.
[You may use the following directory as destination, on the server ldc6001aps45: /deploy/awx_dev_2.0.0/data]

Make sure that the unpacked data/ directory matches the directory used by the Postgres server 
(awx 2.0 local Postgres installation will create a subdiretory named "pgdata", which does not exist on AWX 1.0).

Preferably use **your own instance of Postgres**, since it will be used throughout all versions, as your upgrade.
Download and install AWX 2.0 and install it (using a the postgres DB that you have prepared).

Start up the AWX 2.0 instance - it should detect that data is from AWX 1.0 and perform an upgrade. It will take several minutes to finish.

**Examples on ldc6001aps45 (you will need root user to unpack data into the DB directory):**
``` 
[root@ldc6001aps45 ~]$ cd /deploy/awx_dev_2.0.0/data
[root@ldc6001aps45 awx-2.0.0]$ tar xvfz /deploy/awx_dev_2.0.0/data_fevereiro_2020-02-20.tar.gz
[root@ldc6001aps45 awx-2.0.0]$ ls data/
[root@ldc6001aps45 awx-2.0.0]$ chown -R :awx data
[root@ldc6001aps45 ~]# chmod -R 777 data

[awx@ldc6001aps45 awx-2.0.0]$ docker run -d --name awx_dev_2.0_pg -v /deploy/awx_dev_2.0.0/data:/var/lib/postgresql/data -p 5433 nexus.grupocgd.com:8443/postgres:9.6
[awx@ldc6001aps45 awx-2.0.0]$ cd /deploy/awx_dev_2.0.0/awx-2.0.0/awx-2.0.0
[awx@ldc6001aps45 awx-2.0.0]$ ansible-playbook -i installer/inventory installer/install.yml
```

Then access AWX using the address *http://ldc6001aps45:8052*


## 2. Upgrade AWX 2.0 to 9.0.1 on the third server ##
Download AWX 9.0.1 and run the installer on the same machine where you've installed AWX 2.0
It will upgrade the AWX version and also upgrade the Database

The third server is now ready to get data from it, and migrate that data into Tower 3.6


## 3. Download & Install awx-migrate tool so that data can be migrated

On the client machine, clone the awx-migrate tool. It will allow us to get data from AWX 9.0

![tower](assets/awx-migrate.png)

```
 Cloning of the awx-migrate project:
[]$ git clone https://github.com/autops/awx-migrate

```

## 4. Download & Install tower-cli tool to communicate via REST API with tower and awx

On the client machine, install the tower-cli tool:
(you can find it also on *ldc6001aps45:/deploy/awx-migrate-dev*)

https://tower-cli.readthedocs.io/en/latest/

You may also use *pip* (python) to install it as a python package.


![tower](assets/tower-cli.png)

```
 Cloning of the tower-cli project:
[]$ git clone https://github.com/ansible/tower-cli

```

## 5. Configure access to AWX REST API from the command-line :


We will create the file $HOME/.tower_cli.cfg with the entries:

![tower](assets/tower_cli_cfg.png)


## 6. Configure the file awx-migrate-wrapper that comes with the awx-migrate project

First we need to go on the destination host where *Ansible Tower* is installed and copy the secret key:

```
sudo cat /etc/tower/SECRET_KEY 
<Alphanumeric string>
```
Copy this string - you'll need to paste it into the following file on the client machine.

Edit the script *awx-migrate-wrapper* and fill in the variables with the connection data to the origin and destination DB.
The string that you've just copied should be use to define the variable AWX_DST_SECRET_KEY.
The variable AWX_SRC_SECRET_KEY should be filled in with the secret key fro awx (check the inventory file you've used to install AWX 2.0)
The other variables that start with "AWX_SRC" should be filled-in with info that allows connection to the Origin database.
The remaining variables that start with "AWX_DST" should be filled-in with info that allows connection to the Destination database.

Here's an example:

![tower](assets/awx-migrate-wrapper.png)

Edit the script *awx-migrate* and add the following lines to the top of the file, just after the first line (python interpreter).
Whis will allow the encoding for special characters:

![tower](assets/special_chars.png)

Otherwise when you try to run the script the following error might happen when importing data with special chars such as 'Plataformas de Gestão e Monitorização'

![tower](assets/special_chars_errors.png)


## 7. Prepare the destination Tower installation:
Be sure that you have your instance running and that the License has been configured:
- Check the license on the tower installation


## 8. Delete cache file and run the script to import the data:
First of all, check if the file "awx-data.pickle" exists on the directory where you have the awx-migrate scripts.
If it does, delete it.
- Delete the "awx-data.pickle" file

Run the script to produce the output of the data by segment on json format:

```
[awx@ldc6001aps45 awx-migrate-dev]$ ./awx-migrate-wrapper
```

![tower](assets/awx-migrate-output.png)

A few .json files will be written on your current directory, that contains all data extracted from AWX Database.


## 9. Import the data into the Ansible Tower:

In order to import the data into ansible tower we will need the tower-cli tool to send the data in the json format using REST API we need to alter once more the tower config file present in home dir .tower_cli.cfg

![tower](assets/tower_cli_cfg2.png)

We also need to alter the tower-cli script to permit special chars in the encoding/decoding process so run:

```
[awx@ldc6001aps45 awx-migrate-dev]$ which tower-cli
/bin/tower-cli
[awx@ldc6001aps45 ~]$ 
```

Use any editor to edit the file and enter the following:

![tower](assets/tower-cli-script.png)

Then send the data with the following command:

![tower](assets/tower-cli-send-user.png)

The documentation points that the data needs to be sent in the following order:
  - user
  - organization
  - team
  - credential_type
  - credential
  - notification_template
  -  inventory_script
  -  project
  -  inventory
  -  job_template
  -  workflow

We tried to send all the data from the awx version 2.0.0 and it works for every component except for the job_templates with the following error:

![tower](assets/error-awx-2.0.png)


So after upgrading the awx to the version awx-9.0.0 (it upgrades automatically using the same data dir in our case we used postgres 9.6), We were able to send the data using the tower-cli tool but this time with the following error:

![tower](assets/playbook-not-found.png)


So this error happens because it can't find the playbook to import to tower. So in order for it to find the playbook we need to update any project that has the SCM (Source Control Manager) link. For that We will use the tower-cli tool with the following:

```
for ((i=0; i<100; i=$i+1)); do tower-cli project update $i; echo $i; done
```

You might experience the following error on the jobs

![tower](assets/gitlab-error.png)

You need to copy ca-cgd.crt to the destination hosts in order to communicate with gitlab.grupocgd.com and run

```
cp /tmp/ca-cgd.crt /etc/pki/ca-trust/source/anchors/ca-cgd.crt && update-ca-trust extract
```

After this is you can update the projects and when sending data it will recognize the playbooks existing on the job_templates:

![tower](assets/tower-send-awx.png)


## 10. Make users bind to ldap

When you import the users they are created as local with default password 'password' so you need to alter the user table in the database to interpret the user as LDAP. 

Before going into the database you will need to sed the password binded to LDAP (because this is set to a default password) with the tower-cli tool:

```
tower-cli setting modify AUTH_LDAP_BIND_PASSWORD 'NnonJtxpZ6VQGSBBXzpb'
Resource changed.
======================= =========== 
          id               value    
======================= =========== 
AUTH_LDAP_BIND_PASSWORD $encrypted$
======================= =========== 

```


For that you need to run the following query inside the database:


```
select 'update main_profile set ldap_dn="cn='||auth_user.username||',ou=ssi,ou=utilizadores,ou=servicos-centrais,ou=cgd,dc=grupocgd,dc=com"''where user_id='||auth_user.id||';' from auth_user inner join main_profile on main_profile.id=auth_user.id where main_profile.ldap_dn='' and username not in ('admin','jenkins','uimapi');
```

Then pass the output to a sql file and run as follow

```
psql -h host -U user -d database --port x -f sql_queries.sql 
```

where sql_queris.sql is the file you passed the output


## 11. If you need to start over (in case of errors)
If you need to start the import process again, you will need to clean up the Database on the destination (Tower) server.
To do that, you have to drop all tables and sequences of the "awx" schema.

Then you need to recretate the DB structure again. This is only achievable by running the Tower installation again (setup.sh).
You will need sudo (root) privileges for the user that is perfoming the installation.

