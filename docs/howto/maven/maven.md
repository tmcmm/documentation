## Maven Install 
In order to install maven you will need to download the binaries and configure maven environment variables to use:<br>
[Install Maven on CentOs](https://tecadmin.net/install-apache-maven-on-centos/ "Maven Centos")<br>
[Install Maven on Windows](https://www.mkyong.com/maven/how-to-install-maven-in-windows/ "Maven Windows")<br>

### In windows you will need to add to the system variables
Run command on command prompt:<br>
```
control sysdm.cpl
```
create the variable maven home and pass it to the path system variable:<br>
![maven](assets/maven.png "Maven vars")
![maven](assets/maven1.png "Maven vars")
![maven](assets/maven2.png "Maven vars")

Check version of maven:<br>
```
mvn - version 
```
### In Linux you will need to 
Download the binaries from Maven .tar and unzip with tar -xvf:

Then export Maven to local variables with the command:<br>
```
Export PATH = /pathToMaven/Maven/apache-maven-3.5.0/bin
```
and confirm that it is in the local variables with the command: env | grep PATH<br>


## Maven Projects
Maven is divided into two parts (the pom.xml which is where the application's dependencies are and the settings.xml file which are the maven settings that are in the path of the binaries of the maven, under the conf directory) <br>

__First Step:__
- Create the .m2 directory (/homedir/.m2)

- Enctrypt the master-password 
```
mvn --encrypt-master-password master1234
```
And save it in the settings-security.xml file inside the .m2 directory<br>


From this moment we can encrypt passwords as follows:<br>
```
mvn --encrypt-password "password"
```

Change proxy settings:<br>

In case we need to get the dependencies from internet and we are inside a corporate network we need to configure a proxy<br>
Go to apache-maven-3.5.0-bin\apache-maven-3.5.0\conf file and add the following (remember to add the password as encrypted):<br>
```
<proxy>
      <id> optional </id>
      <active> true </active>
      <protocol> http </protocol>
      <username> user </username>
      <password> "Password" </password>
      <host> proxy.domain.com </host>
      <port> 8080 </port>
    </proxy>
```
If we need to get the dependencies from a remote repository:<br>
We change the settings.xml file that is inside in your .m2 folder <br>
```
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">

        <localRepository />
        <interactiveMode />
        <usePluginRegistry />
        <offline />
        <pluginGroups />
        <servers>
                <server>
                        <id>Server1-Releases</id>
                        <username>user</username>
                        <password></password>
                </server>
                <server>
                        <id>Server2-Snapshots</id>
                        <username>user</username>
                        <password></password>
                </server>
        </servers>
        <mirrors>
                <mirror>
                        <id>maven-central</id>
                        <url>https://nexus.grupocgd.com:8081/repository/maven-central/</url>
                        <mirrorOf>!SDGHP-Releases,!SDGHP-Snapshots,external:*</mirrorOf>
                </mirror>
        </mirrors>
```
Pom file will have the dependencies as well as the servers that you will for instance deploy the binary that is built.<br>
```
project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.cgd.fatorial</groupId>
  <artifactId>fatorial</artifactId>
  <packaging>jar</packaging>
  <version>1.0.2</version>
  <name>fatorial</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <distributionManagement>
    <repository>
      <id>nexus-releases</id>
      <name>Repositorio de snapshot do Nexus</name>
      <url>https://nexus.grupocgd.com/repository/releases/</url>
    </repository>
  </distributionManagement>
  <build>
  <extensions>
    <extension>
        <groupId>com.github.shyiko.servers-maven-extension</groupId>
        <artifactId>servers-maven-extension</artifactId>
        <version>1.3.0</version>
    </extension>
    </extensions>	  
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>com.cgd.fatorial.App</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
```
!!!note "Note"
    Both settings.xml and pom.xml can contain the url to which you want to send the build however it is in settings.xml that authentication is supposed to be done

## Maven example project (Factorial)
Start a simple program:<br>

```
mvn archetype:generate -DgroupId=grupo -Dpackage=fatorial -DartifactId=artefact -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

Example: 
```
mvn archetype:generate -DgroupId=com.example.bank
   -DartifactId=OracleBanking
   -DarchetypeArtifactId=maven-archetype-quickstart
   -DinteractiveMode=false
```
![maven example](assets/maven3.png "Maven example")
![maven example](assets/maven4.png "Maven example")


Source Code Factorial:<br>
```
package fatorial;

import javax.swing.JOptionPane;

public class App {

	public static void main(String[] Args) {

		int numero;
		try {
			numero = Integer.parseInt(JOptionPane.showInputDialog(null,
					"Qual e o numero para calcular o fatorial?"));

			JOptionPane
					.showMessageDialog(null, "O factorial do número é "
							+ factorial(numero), "Resultado",
							JOptionPane.PLAIN_MESSAGE);

		}

		catch (NumberFormatException ntf) {
			JOptionPane.showMessageDialog(null, "Input must be Integer",
					"Error", JOptionPane.ERROR_MESSAGE);

		}
	}

	private static long factorial(int numero) {
		long factorial;
		if (numero < 0) {
			JOptionPane.showMessageDialog(null, "Input must be positive",
					"Numero inválido", JOptionPane.ERROR_MESSAGE);
			throw new IllegalArgumentException("Input must be positive");
		}
		if (numero < 2) {
			return factorial = 1;

		}
		factorial = numero * factorial(numero - 1);
		return factorial;
	}
}
```
Change the pom.xml to place the dependencies:<br>
```
<groupId>com.grupocgd.fatorial</groupId>
  <artifactId>fatorial</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>fatorial</name>
  <dependencies>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>1.6.2.redhat-5</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>
</dependencies>
```
Analyzing the pom file:<br>
![maven example](assets/maven5.png "Maven example")
![maven example](assets/maven6.png "Maven example")

Make build ignoring the nexus certificate:<br>
```
mvn clean package -Dmaven.wagon.http.ssl.insecure=true
```
![maven example](assets/maven7.png "Maven example")

Try the jar built with java command:
```
java -jar <App.jar>
```
![maven example](assets/maven8.png "Maven example")

In order to auto-increment the build version:<br>
```
mvn deploy build-helper: parse-version versions: set -DnewVersion = 1. $ {parsedVersion.minorVersion}. $ {parsedVersion.nextIn
crementalVersion} versions: commit -Dmaven.wagon.http.ssl.insecure = true
```

### Settings.xml example
```
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">

        <localRepository />
        <interactiveMode />
        <usePluginRegistry />
        <offline />
        <pluginGroups />
        <servers>
                <server>
                        <id>SDGHP-Releases</id>
                        <username>${USER_DEPLOY}</username>
                        <password>${PASS_DEPLOY}</password>
                </server>
                <server>
                        <id>SDGHP-Snapshots</id>
                        <username>${USER_DEPLOY}</username>
                        <password>${PASS_DEPLOY}</password>
                </server>
        </servers>
        <mirrors>
                <mirror>
                        <id>maven-central</id>
                        <url>https://nexus.grupocgd.com/repository/maven-central</url>
                        <mirrorOf>!SDGHP-Releases,!SDGHP-Snapshots,external:*</mirrorOf>
                </mirror>
	</mirrors>
	<profiles>
                <profile>
                        <id>nexus</id>
                        <!--Enable snapshots for the built in central repo to direct -->
                        <!--all requests to nexus via the mirror -->
                        <repositories>
                                <repository>
                                        <id>SDGHP-Releases</id>
                                        <name>SDGHP Releases</name>
                                        <url>https://nexus.grupocgd.com/repository/SDGHP/</url>
                                        <releases>
                                                <enabled>true</enabled>
                                        </releases>
                                        <snapshots>
                                                <enabled>false</enabled>
                                        </snapshots>
                                </repository>
                                <repository>
                                        <id>SDGHP-Snapshots</id>
                                        <name>SDGHP Snapshots</name>
                                        <url>https://nexus.grupocgd.com/repository/SDGHP-Snapshots/</url>
                                        <releases>
                                                <enabled>false</enabled>
                                        </releases>
                                        <snapshots>
                                                <enabled>true</enabled>
                                        </snapshots>
                                </repository>
					    </repositories>
                </profile>
        </profiles>
        <activeProfiles>
        <!--make the profile active all the time -->
        <activeProfile>nexus</activeProfile>
        </activeProfiles>
</settings>
```
### Pom.xml example
```
<?xml version="1.0" encoding="UTF-8"?>
<project
    xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
    
    <groupId>cgd</groupId>
    <artifactId>gh.online.gestao.historico</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<packaging>${packaging.type}</packaging>
	
	
	<!-- Inherit defaults from Spring Boot (empty relativePath => load parent from repository) -->
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.3.0.RELEASE</version>
		<relativePath/>
	</parent>
	
	
	<properties>
		<java.version>1.8</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.build.outputEncoding>UTF-8</project.build.outputEncoding>
		<skipTests>true</skipTests>
		<morphis.framework.version>1.0.3-SNAPSHOT</morphis.framework.version>
		<gh.common.version>1.0.0-SNAPSHOT</gh.common.version>
	</properties>
	
	<distributionManagement>
		<snapshotRepository>
			<id>SDGHP-Snapshots</id>
			<name>Internal Snapshots</name>
			<url>https://nexus.grupocgd.com/repository/SDGHP-Snapshots/</url>
		</snapshotRepository>
		<repository>
			<id>SDGHP-Releases</id>
			<name>Internal Releases</name>
			<url>https://nexus.grupocgd.com/repository/SDGHP/</url>
		</repository>
	</distributionManagement>
	
	
	<dependencies>
		<dependency>
			<groupId>morphis.framework</groupId>
			<artifactId>morphis.framework.base</artifactId>
			<version>${morphis.framework.version}</version>
		</dependency>
		<dependency>
			<groupId>cgd</groupId>
    		<artifactId>gh.common.gestao.historico</artifactId>
			<version>${gh.common.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>			
	</dependencies>	
	
	
	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<compilerArgument>-Xlint:all</compilerArgument>
					<failOnError>false</failOnError>
					<annotationProcessors>
						<annotationProcessor>morphis.framework.compiler.dataprocessor.DataProcessor</annotationProcessor>
						<annotationProcessor>morphis.framework.compiler.viewmodelprocessor.ViewModelProcessor</annotationProcessor>
					</annotationProcessors>
					<annotationProcessorPaths>
						<annotationProcessorPath>
							<groupId>morphis.framework</groupId>
							<artifactId>morphis.framework.compiler</artifactId>
							<version>${morphis.framework.version}</version>
						</annotationProcessorPath>
					</annotationProcessorPaths>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
<!-- 				WARNING: version 3.1.2 (defined in spring-boot-starter-parent) causes an "Unknown error" in eclipse 2018-12...  -->
				<version>3.1.1</version>	
				<configuration>
					<archive>
						<addMavenDescriptor>false</addMavenDescriptor>
					</archive>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>SpringBootWar</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<packaging.type>jar</packaging.type>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-maven-plugin</artifactId>
						<configuration>
							<mainClass>cgd.WebApp</mainClass>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>	
</project>
```
