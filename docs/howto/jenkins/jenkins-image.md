## Create a jenkins docker image for scalability - JCASC (Jenkins as code) 

1. What is Jenkins-Configuration as code.?

To start a JCASC instance you need to install the plugin configuration-as-code so go to the official 
https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md

![jenkins build version](assets/JCASC_started.png)

Jenkins as code relies on environment variable CASC_JENKINS_CONFIG and will load any jenkins.yaml present in the directory.

![jenkins build version](assets/Environment.png)

JCASC plugin will then read the environment variable and it will load up the configurations.

![jenkins build version](assets/jcasc_test.png)


## Use cases explained 

# 1. Use Case : Update Jenkins Version and Rollback if needed

__Clone the Jenkins JCASC Project:__

```
git clone git@gitlab.grupocgd.com:docker/jenkins-as-code-jcasc.git
```

Access [Jenkins PRD Homepage](https://jenkins.grupocgd.com "Jenkins Homepage") or [Jenkins DEV Homepage](https://jenkins.dev.grupocgd.com "Jenkins Homepage") and look for the version at the bottom of the page:

![jenkins build version](assets/2_214.png)

Inside dockerfile we need to increment the jenkins version on the top of the file.

![jenkins build version](assets/build_version_2.272.png)

__After incrementing we need to build the new image and push it to our registry repository and for that we will use the makefile:__
```
make build
make login
make push 
```
You can notice the entry __--latest-specified__ on the Dockerfile on the Installing plugins step.<br>
This entry will update all the latest dependencies of any plugin that is requested to have the latest version.<br>

![jenkins build version](assets/jenkins-latest-specified.png)

You can check the other available arguments at:<br>
[Jenkins plugin cli Github](https://github.com/jenkinsci/plugin-installation-manager-tool "Jenkins Plugin Cli")

The next step would be to log on the machines that host Jenkins:<br>
    - Jenkins-DEV: ldc6001aps69 (sudo to jenkins user)<br>
    - Jenkins-PRD: lpc6001aps21 (sudo to jenkins user)<br>

After that you will alter the docker-compose file to get the newer image. Keep in mind the __jenkins.yaml__ file that is loading the configurations.<br>
__Docker-compose file:__
```
version: '3.4'
services:

  jenkins-jcasc-latest:
    restart: always
    privileged: true
    image: registry.dev.grupocgd.com/cgd/jenkins-base_as_code:2.272
    container_name: jenkins_jcasc
    network_mode: bridge
    ports:
    - "8084:8080"
    - "50000:50000"
    volumes:
    - /deploy/jenkins/data/jenkins_home:/var/jenkins_home
    - /var/run/docker.sock:/var/run/docker.sock
```
Go to the directory containing the docker-compose file and run:<br>
```
docker-compose -f /deploy/jenkins/docker-compose-jenkins.yml up -d
```
or to stop:<br>
```
docker-compose -f /deploy/jenkins/docker-compose-jenkins.yml down -v
```

If you didn't use the tag __latest-specified__ you might encounter errors in the plugins because some of the plugins might not run in the newer jenkins version<br>
Look out for the logs in the jenkins_jcasc image:<br>

```
WARNING: ConfigurationAsCode.init failed perhaps due to plugin dependency issues

SEVERE: Failed Loading plugin Pipeline v2.6 (workflow-aggregator)
java.io.IOException: Failed to load: Pipeline (2.6)
 - Failed to load: Pipeline: Shared Groovy Libraries (2.17)
```
If a error like this happens you will need to update each of the plugin in the plugins.txt file manually or otherwise use the recommended tag __latest-specified__.<br>

Lookout for any errors on the container logs because you can encounter __incompatible syntax__ errors due to the newer plugins versions and the __jenkins.yaml__ configuration file.<br>

!!!note "Note"
        Before making any changes to the __jenkins.yaml__ file make a backup of the existing one in case we need to rollback.<br>


Lookout for errors like:<br>
```
WARNING: JENKINS-2111 path sanitization ineffective when using legacy Workspace Root Directory ‘${ITEM_ROOTDIR}/workspace’; switch to ‘${JENKINS_HOME}/workspace/${ITEM_FULL_NAME}’ as in JENKINS-8446 / JENKINS-21942
```

If we can't resolve the new configuration syntax error we need to rollback the version asap. <br>
In order to do that you need to alter the docker-compose file back to the previous image, recover the backed up version of jenkins.yaml and delete the plugins folder in order to recreate the plugins.<br>
After that run:

```
docker-compose -f /deploy/jenkins/docker-compose-jenkins.yml down -v
```
```
docker-compose -f /deploy/jenkins/docker-compose-jenkins.yml up -d
```
After that check the new instance of jenkins to confirm that it has ben upgraded<br>
![jenkins build version](assets/new-instance.png)

# 2. Use Case : RollBack Installed Plugin via UI

When trying to install a plugin and perform a restart to jenkins instance it fails with the error:<br>

![jenkins build version](assets/error_plugin.png)

That means that the upgraded plugins has a different syntax in the configuration of __jenkins.yaml__<br>
You have to stop the jenkins, remote the plugins folder and startup it up again.<br>

If you don't remove the plugins folder it will stay installed.


# 3. Use Case : Configuration change 

!!!note "Note"
        After changing the configuration on jenkins UI and then restart it, it won't save any configuration!!

Since you are starting a jenkins JCASC instance you have to write the configuration as yaml or when jenkins start it will read the jenkins.yaml and delete your manual configuration. So keep in mind to update __jenkins.yaml__ on every configuration of jenkins instance (__not jobs configuration__). For example changing the DockerTemplate of a jenkins slave.

# 4. Use Case : Export yaml after altering configuration  

After changing the configuration on jenkins UI such as maven-plugin-configuration as displayed on this image:


![jenkins build version](assets/maven_plugin.png)


You will have to export the jenkins.yaml configuration, or schedule a job for it to save, or put it on gitlab so jenkins can reload it from there.<br>
For it go to __https://server.grupocgd.com/configuration-as-code/__ and click on View Configuration or Download it. You also have documentation you can read<br>

![jenkins build version](assets/configuration_as_code.png)

# 5. Use Case : Install plugins on startup of instance

To know the server installed plugins (ex: https://jenkins.grupocgd.com) run the command:

```
curl -sSL "http://$JENKINS_HOST/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/' > plugins.txt
```
Where $JENKINS_HOST= user:pass@server. After knowing the installed plugins you can start your own JCASC instance with the same list of plugins.

For that you need to import the project running:
```
git clone git@gitlab.grupocgd.com:docker/jenkins-as-code-jcasc.git
```
And change the plugins.txt with your new plugins file.<br> 
After that change the __VERSION__ file to the newer version (if it is the same it will just pass latest) and run:

```
make build
make login
make push 
```


## Console Scripts 
### Stop all queued jobs executions
```
 import java.util.ArrayList
  import hudson.model.*;
  import jenkins.model.Jenkins

  // Remove everything which is currently queued
  def q = Jenkins.instance.queue
  for (queued in Jenkins.instance.queue.items) {
    q.cancel(queued.task)
  }

  // stop all the currently running jobs
  for (job in Jenkins.instance.items) {
    stopJobs(job)
  }

  def stopJobs(job) {
    if (job in com.cloudbees.hudson.plugins.folder.Folder) {
      for (child in job.items) {
        stopJobs(child)
      }    
    } else if (job in org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject) {
      for (child in job.items) {
        stopJobs(child)
      }
    } else if (job in org.jenkinsci.plugins.workflow.job.WorkflowJob) {

      if (job.isBuilding()) {
        for (build in job.builds) {
        build.doKill()
        }
      }
    }
  }
```
### Stop all slaves that are stuck on offline mode
```
for (aSlave in hudson.model.Hudson.instance.slaves) { if (aSlave.getComputer().isOffline()) { aSlave.getComputer().setTemporarilyOffline(true,null); aSlave.getComputer().doDoDelete(); } }
```
or
```
for (aSlave in hudson.model.Hudson.instance.slaves) {
  println('====================');
  println('Name: ' + aSlave.name);
  println('getLabelString: ' + aSlave.getLabelString());
  println('getNumExectutors: ' + aSlave.getNumExecutors());
  println('getRemoteFS: ' + aSlave.getRemoteFS());
  println('getMode: ' + aSlave.getMode());
  println('getRootPath: ' + aSlave.getRootPath());
  println('getDescriptor: ' + aSlave.getDescriptor());
  println('getComputer: ' + aSlave.getComputer());
  println('\tcomputer.isAcceptingTasks: ' + aSlave.getComputer().isAcceptingTasks());
  println('\tcomputer.isLaunchSupported: ' + aSlave.getComputer().isLaunchSupported());
  println('\tcomputer.getConnectTime: ' + aSlave.getComputer().getConnectTime());
  println('\tcomputer.getDemandStartMilliseconds: ' + aSlave.getComputer().getDemandStartMilliseconds());
  println('\tcomputer.isOffline: ' + aSlave.getComputer().isOffline());
  println('\tcomputer.countBusy: ' + aSlave.getComputer().countBusy());
  if (aSlave.name == 'jenkins-slave-002blqc2rp163') {
    println('Shutting down node!!!!');
    aSlave.getComputer().setTemporarilyOffline(true,null);
    aSlave.getComputer().doDoDelete();
  }
  println('\tcomputer.getLog: ' + aSlave.getComputer().getLog());
  println('\tcomputer.getBuilds: ' + aSlave.getComputer().getBuilds());
}
```
### Kill a job that is in Zombie state
Get the full name of the job and his build number:
```
Jenkins.instance.getItemByFullName("AGILE/SDAOP_AOP/EAR_Was_Aop")
                .getBuildByNumber(14)
                .finish(
                        hudson.model.Result.ABORTED,
                        new java.io.IOException("Aborting build")
                );
 
```

### Run a Job at 3AM of each Month
![jenkins build scheduled](assets/job-jenkins.png)

```
H 3 15 * *
```


!!!note "Note"
        Credentials are not exported as yaml , they are stored in credentials.xml and needs to be moved along side with jobs etc if you want to startup a new jenkins instance.

## Questions to the Support and Answers:

####  After exporting the configuration yaml using the plugin (https://jenkins_server/configuration-as-code) I noticed that some plugin configurations were not indented correctly in the yaml file. Although  successfully managed to get a Jenkins up with the same version as is in production environment  We had to do a lot of rearranging on the configurations of the yaml file.

#####  Yes, it is like that. Currently many plugins are not fully compatible with jcasc (artifactory is one of them...) and the export is not generating a valid configuration file. If you find any of them, the best option is to open a request in the community side. There is a json schema which is under development https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/docs/features/jsonSchema.md 

#### We need a scalable Jenkins to test the installation of plugins and to upgrade Jenkins if necessary. We tried to upgrade to a newer version of jenkins using a new docker image pointing to the same jcasc configuration file as in the previous version, but it failed to start. So regarding new versions is there a procedure to guarantee that it doesn’t crash on start?

##### There is no way to guarantee that it will not crash when using the existing JCasC configuration after an upgrade. You will have to test (and it is a good practice to so) at every upgrade and make the necessary fixes. There are too many (badly behaved) plugins or unhandled situations.

#### Whenever we change any configuration on jenkins  UI and want to save it as yaml how do we procede to export it and be sure it works on the next startup, since exporting as yaml is not 100% secure? Do I need to test it every single time on new startups?

##### Currently, you need to test any change to the configuration file. Engineering is working on features to improve and streamline this process (generated easily a test environment on a configuration bundle pull request (PR). But I am afraid that these features are for the enterprise (= paying) version. Configuration Bundle management is a feature of the CloudBees Jenkins Operation Center.
