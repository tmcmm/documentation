
You can create a project on gitlab that will act as the lib for the jenkins pipelining.<br>
This project contains every function that will be needed from the Jenkins Projects.<br>
This way every project can be easily escalated and its work environment much cleaner
![Jenkins libs](assets/jenkins.png "Jenkins libs")<br>
All these functions are stored under directory vars in the gitlab project.<br>

You can create a function that builds the maven project for example:
```
def call(String appName, String foundationVersion) {
	
echo "Versao do Foundation selecionado para execucao: ${foundationVersion}"

waitUntil {
		try {	

		withCredentials([usernamePassword(credentialsId: 'agile_user', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')])
		{
			withMaven(globalMavenSettingsFilePath: 'maven_config/settings.xml')
			{
				echo 'Building'
				sh "mvn versions:set-property -Dproperty=foundation.version -DnewVersion=${foundationVersion} -Dmaven.wagon.http.ssl.insecure=true"
				sh "mvn clean install -DskipSettings -DskipEnforcer -Dmaven.test.skip=true -DskipITs -DskipTests=true -Dcontext_root=${appName} -Dmaven.wagon.http.ssl.insecure=true"
			}								  
		}  

		return true
		} catch (e) {
			echo 'ATENCAO - Erro capturado no Stage "Build": ' + e.toString()
			timeout(time: 5, unit: 'MINUTES') {
				input id: 'Approve', message: 'ERRO detectado!!! Fazer YES para RETRY ou ABORT para parar com erro.', ok: 'Yes', submitter: getSubmitterGroup()
			}
			return false
		}
	}
}
```
For example function Deploy_EAR_TI.groovy:<br>
```
def call(String versionResultEAR, String versionResultZIP, String appName, String nameEar, String nameWar, String groupIdNexus, String newVersion, String tamanhoEarCompilado) {
    
    timeout(time: 15, unit: 'DAYS') {
        input id: 'Approve', message: 'Approve deploy to TI now?', ok: 'Yes', submitter: getSubmitterGroup()
	}

	node('jenkins-slave') {
		waitUntil {
			try {  

			    def remoteDeploy = [:]
				remoteDeploy.allowAnyHosts = true
				def remoteStatics = [:]
				remoteStatics.allowAnyHosts = true
				
				/* SERVIDORES */
				remoteDeploy.host = "uic6001was01"
				remoteStatics.host = "UIC6001WEB01"
				
				downloadArtefatos( "$nameEar", "$versionResultEAR", "$nameWar", "$appName", "$versionResultZIP", "$groupIdNexus", "$newVersion", "$tamanhoEarCompilado")
				
				withCredentials([usernamePassword(credentialsId: 'b59b726b-6489-47e0-9bdd-f7db0771cec3', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
					remoteDeploy.user = USERNAME
					remoteDeploy.password = PASSWORD
					remoteDeploy.name = USERNAME
				}

				sshRemove remote: remoteDeploy, path: "/usr/opt/apps/${appName}/dimensions/ear/${appName}-application.ear"
				sshPut remote: remoteDeploy, from: "${appName}-application.ear", into: "/usr/opt/apps/${appName}/dimensions/ear/"
				sshCommand remote: remoteDeploy, command: "/usr/opt/apps/${appName}/scripts/stop-server.sh"
				sshCommand remote: remoteDeploy, command: "/usr/opt/apps/${appName}/scripts/deployEAR.sh"
				sshCommand remote: remoteDeploy, command: "/usr/opt/apps/${appName}/scripts/start-server.sh"
				
				withCredentials([usernamePassword(credentialsId: '390fbeeb-d1c7-4053-aab5-092accb6b2dd', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
					remoteStatics.user = USERNAME
					remoteStatics.password = PASSWORD
					remoteStatics.name = USERNAME
				} 

				sshRemove remote: remoteStatics, path: "/usr/opt/IBM/HTTPServer/htdocs/${appName}"
				sshPut remote: remoteStatics, from: "${nameWar}.zip", into: "/usr/opt/IBM/HTTPServer/htdocs/"
				sshCommand remote: remoteStatics, command: "unzip /usr/opt/IBM/HTTPServer/htdocs/${nameWar}.zip -d /usr/opt/IBM/HTTPServer/htdocs/"
				sshRemove remote: remoteStatics, path: "/usr/opt/IBM/HTTPServer/htdocs/${nameWar}.zip"
				sshCommand remote: remoteStatics, command: "/usr/opt/IBM/HTTPServer/bin/apachectl restart", sudo: true 
			
				return true
			} catch (e) {
				echo 'ATENCAO - Erro capturado no Stage "Deploy_TI": ' + e.toString()
				timeout(time: 15, unit: 'DAYS') {
					input id: 'Approve', message: 'ERRO detectado!!! Fazer YES para RETRY ou ABORT para parar com erro.', ok: 'Yes', submitter: getSubmitterGroup()
				}
				return false
			}
		}
	}

}
```
This way you can call any function inside your groovy script as follows:<br>
```
import org.jenkinsci.plugins.pipeline.modeldefinition.Utils 
@Library(['global', 'agile-lib-ear', 'agile-deploy-pipelines']) _

def gitUrl = "https://gitlab.grupocgd.com/AGILE/SDAFC/agile-fce"
setParameters("$gitUrl")

def groupId = "pt.cgd.agile.fce"
def groupIdNexus = groupId.replace(".", "/")
def artifactId = "agile-fce"
def appName = "fce"
def serverName = "servFCEA"
def nameEar = "fce-ear-application"
def nameWar = "fce-module-presentation"
def destinoEmail = "guillermo.edo.rodrigues@cgd.pt, bruno.marques.luis@cgd.pt, michael.barreira.ribeiro@cgd.pt, alberto.nuncio.viegas@cgd.pt, gabriel.silva.rocha@cgd.pt, vinicius.meyer@cgd.pt"
def idGit = 324


try {
    foundationVersion = "${FOUNDATION_VERSION}"
    branchName = "${BRANCH_NAME}"
    hotfix = branchName.matches("(.*)hotfix(.*)")
} catch (e) {
    return false
}

node ('jenkins-slave-maven:8') {
	stage('Checkout') { 
		echo 'Checkout..'
		echo "Git URL que será feito o checkout: $gitUrl"
		echo "Branch a ser feito o checkout: $branchName"
		echo "HOTFIX: $hotfix"
		checkoutGit("$branchName", "$gitUrl")
	}
	stage ('Setting new version') {
		newVersion =  setNewVersion("${groupIdNexus}","${nameEar}", "true", "${hotfix}")
	}
	stage('Build') {
		buildMaven("${appName}", "$foundationVersion")
	}
	stage('Nexus') {
		versionResult = nexus("$groupId", "$groupIdNexus", "$nameEar", "$nameWar", "$newVersion", "true", "$foundationVersion", "$appName")
	}
}

stage('Deploy_TI') {
    if ("${SKIP_TI}" == "true") {
        echo "SKIP_TI detetado ... a sair com sucesso sem fazer Deploy_TI."
        Utils.markStageSkippedForConditional('Deploy_TI') 
    } else {
    	Deploy_EAR_TI("${versionResult[0]}", "${versionResult[1]}", "$appName", "$nameEar", "$nameWar", "$groupIdNexus", "$newVersion", "${versionResult[2]}")
    }
}

stage('Deploy_CQ') {
    if ("${SKIP_CQ}" == "true") {
        echo "SKIP_CQ detetado ... a sair com sucesso sem fazer Deploy_CQ."
        Utils.markStageSkippedForConditional('Deploy_CQ') 
    } else {
    	Deploy_EAR_CQ("${versionResult[0]}", "${versionResult[1]}", "$appName", "$nameEar", "$nameWar", "$groupIdNexus", "$newVersion", "${versionResult[2]}")
    }
}

stage('Deploy_PR') {
    if ("${SKIP_PR}" == "true") {
        echo "SKIP_PR detetado ... a sair com sucesso sem fazer Deploy_PR."
        Utils.markStageSkippedForConditional('Deploy_PR') 
    } else {
    	Deploy_EAR_PR("${versionResult[0]}", "${versionResult[1]}", "$appName", "$nameEar", "$nameWar", "$groupIdNexus", "$newVersion", "${versionResult[2]}")
		mergeRequest("$idGit","$gitUrl","$destinoEmail")
    }
}
```

