## Create a gitlab docker image project

1. Go to https://gitlab.grupocgd.com and create a new project

    ![gitlab new project](assets/gitlab_new_project.png)

2. The project must have a name in format "docker-image-\<name\>" and created in gitlab group "docker". By default you should configure this repository as "private".

    ![gitlab new project](assets/gitlab_docker_image_project.png)

## Generate a project skaffold with a cookiecutter template

1. Clone the new docker image git project to your workspace and the docker-image-cookiecutter project

        []$ git clone https://gitlab.grupocgd.com/docker/docker-image-example.git
        [}$ git clone https://gitlab.grupocgd.com/docker/docker-image-cookiecutter.git

2. Use cookicutter script to create a project skaffold for your docker image

        []$ docker-image-cookiecutter/cookicutter.sh -f https://gitlab.grupocgd.com/ast/templates/cookiecutter-docker-image.git
        Using cookicutter container version 1.1
        Username for 'https://gitlab.grupocgd.com': c080225
        Password for 'https://c080225@gitlab.grupocgd.com':
        role_name [test_role]: docker-image-example
        your_name [some guy]: Rui Alves
        role_description [some role description]: This is a docker image that runs example software
