# Docker Documentation Page

> **Docker** is a platform created for the management of containers 

## What is Docker?

It is a software technology that provides containers. Uses an abstraction layer and virtualization automation at the operating system level.<br> 
Isolates resources and allows independent containers to run on a single Linux instance.<br>
It is a virtualization alternative in which the host kernel is shared with the virtualized machine.<br>

**Image:** Binaries, libraries and source code that makes our application.<br>

**Container:** It is an instance of the image that runs as a process. We can have several containers of the same image.<br>

**Docker container run:**: Search for that image in the local cache, if not find the image in the remote repository of the docker hub.<br>
Assign a virtual IP on a private network inside the docker.<br>

**Docker network security:**<br>
  - Create our front-end and back-end apps on the same docker network.<br>
  - Internal communications are not exposed.<br>
  - All external ports of exposure are closed.<br>
  - You must expose ports with the -p option.<br>

**Docker network: DNS:**
The docker daemon has a built-in DNS server that the containers use by default.Create custom networks whenever possible<br>.

### First Steps

Add user to docker group:<br>
```
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
```
Add permissions to docker dir:<br>
```
$ sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
$ sudo chmod g+rwx "/home/$USER/.docker" -R
```
```
$ sudo systemctl enable docker
$ sudo chkconfig docker on
```
Add proxy to docker:<br>
Create file /etc/systemd/system/docker.service.d/http-proxy.conf and:<br>
```
Environment="HTTP_PROXY=http://squid.grupocgd.com:3128" "HTTPS_PROXY=http://squid.grupocgd.com:3128" "NO_PROXY=localhost,127.0.0.1,.grupocgd.com,.cgd.pt,.cqgrupocgd.com"
```
Alter the docker default dir (__/etc/systemd/system/docker.service.d/override.conf__):<br>
```
ExecStart=/
ExecStart=/usr/bin/dockerd -D --data-root /deploy/docker
```
```
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```
Confirm with:<br>
```
systemctl show --property=Environment docker
```

Keep containers alive during daemon downtime:<br>
change on file __/etc/docker/daemon.json:__<br>
```
live-restore:true
```
[Documentation on Live Restore](https://docs.docker.com/config/containers/live-restore/ "Daemon Runtime")<br>


## Docker commands

__Show information about the docker installed on the machine:__
```
docker info
```

__Show the containers that are running on the machine:__
```
docker ps -a (-q if we want to list only the ids)
```
__Place a shell inside a container:__
```
docker container run -it <id>
```
__Check the logs inside a container:__
```
docker logs <id>
```
__Check the docker volumes:__
```
ls docker volume
```
__Stop a container or all:__
```
Docker stop <id> OR docker stop $ (docker ps -a -q)
```
__Docker show the environment variables:__
```
systemctl show --property = Environment docker
```
__Pass variable in run:__
```
docker container run -d -p 3306: 3306 --name db -e MYSQL_RANDOM_ROOT_PASSWORD = yes MYSQL_RANDOM_ROOT_PASSWORD
```

__Docker inspect command:__
```
docker container inspect --format '{{.NetworkSettings.IPAddress}}' webhost
```

__Running a container and on exit deletes the container:__
```
docker container run --rm -it ubuntu: 14.04 bash
```
__Create a network and add a container to that network (by default the network created is of the bridge type):__
```
docker network create dude
docker container run -d --net dude --net-alias search elasticsearch: 2
```
### Docker cleanup-guide:

__Delete volumes:__
```
$ docker volume rm $ (docker volume ls -qf dangling = true)
$ docker volume ls -qf dangling = true | xargs -r docker volume rm
```
__Delete networks:__
```
$ docker network ls
$ docker network ls | grep "bridge"
$ docker network rm $ (docker network ls | grep "bridge" | awk '/ / {print $ 1}')
```

__Remove docker images:__
```
$ docker images
$ docker rmi $ (docker images --filter "dangling = true" -q --no-trunc)
$ docker images | grep "none"
$ docker rmi $ (docker images | grep "none" | awk '/ / {print $ 3}')
```
__Remove docker containers:__
```
$ docker ps
$ docker ps -a
$ docker rm $ (docker ps -qa --no-trunc --filter "status = exited")
```
__Resize disk space for docker vm:__
```
$ docker-machine create --driver virtualbox --virtualbox-disk-size "40000" default
```

__Starting a Nginx Web Server__
```
docker container run --publish 80:80 nginx
docker container run --publish 80:80 --detach nginx
ls -a docker container
```
```
docker container run --publish 80:80 --detach --name webhost nginx
docker stack ls # List stacks or apps
docker stack deploy -c <composefile> <appname> # Run the specified Compose file
docker service ls # List running services associated with an app
docker service ps <service> # List tasks associated with an app
docker inspect <task or container> # Inspect task or container
docker container ls -q # List container IDs
docker stack rm <appname> # Tear down an application
docker swarm leave --force # Take down a single node swarm from the manager
```
```
docker tag <image> username / repository: tag # Tag <image> for upload to registry
docker push username / repository: tag # Upload tagged image to registry
docker run username / repository: tag # Run image from a registry
```

__Docker list all ip's of the containers:__
```
ps -q docker | xargs -n 1 docker inspect --format '{{.NetworkSettings.IPAddress}} {{.Name}}' | sed 's / \ // /'
```

## Docker Troubleshooting WAI CASE

> __Summary:__ The docker by default in the installation assigns a private vlan that conflicts with internal networks.
 Private network prevents outside access. If a device on a private network wants to communicate with other networks, there must be a "gateway" to ensure that the external network is seen as a public address so that the router allows communication.

When installing Docker, you will need to assign the following vlan to the docker's bridge interface.<br>
Network 10.11.67.0/24 has been reserved <br>
This network will not be configured on any communications equipment, nor will it be assigned to any other solution.<br>


Conflicting machine:<br>
__172.19.143.46 - lpc6001wai20__

```
docker network inspect docker_gwbridge
"Subnet": "172.19.0.0/16",
"Gateway": "172.19.0.1"
```
List the vlans available on IPAM:<br> 
Put in Browser:<br>
```
https://10.63.254.10/wapi/v2.3.1/network?_Paging=1&_return_as_object=1&_max_results=100000
```

- Solution 1:<br>
Change the /etc/docker/daemon.json file and place the entry:<br>
```
"beep": "172.19.143.0/24"
/bin/systemctl restart docker
```
- Solution2:<br>

```
gwbridge_containers = $ (docker network inspect docker_gwbridge --format '{{range $ k, $ v: = .Containers}} {{$ k}} {{printf "\ n"}} {{end}}' | xargs - I {} docker container ls -f is-task = false -f id = {} - format {{. Names}})~
```
```
docker stop $ {gwbridge_containers}
docker network rm docker_gwbridge
```
creation of a new network:<br>
```
docker network create --subnet = 172.19.143.0 / 24 -o com.docker.network.bridge.enable_icc = false -o com.docker.network.bridge.name = docker_gwbridge docker_gwbridge
```