# Managing CI/CD Tooling

### Machines that hosts ci/cd tools

### Links 
[RHSSO-CQ](https://auth.cqgrupocgd.com/auth/ "RHSSO Quality Environment")<br>
[RHSSO-PRD](https://auth.grupocgd.com/auth/ "RHSSO Production Environment")<br>
[Jenkins-DEV](https://jenkins.dev.grupocgd.com "Jenkins Dev Environment")<br>
[Jenkins-PRD](https://jenkins.grupocgd.com "Jenkins Production Environment")<br>
[Nexus-DEV](https://nexus.dev.grupocgd.com "Nexus DEV Environment")<br>
[Nexus-PRD](https://nexus.grupocgd.com "Nexus Production Environment")<br>
[Sonarqube-PRD](https://sonarqube.grupocgd.com/sonarqube "Sonarqube Production Environment")<br>
[Gitlab-PRD](https://gitlab.grupocgd.com "Gitlab Production Environment")<br>
[Gitlab-DEV](https://gitlab.dev.grupocgd.com "Gitlab Development Environment")<br>
[Harbor-PRD](https://registry.grupocgd.com "Harbor Production Environment")<br>
[Harbor-DEV](https://registry.dev.grupocgd.com "Harbor Development Environment")<br>
[Tower-DEV](https://awx.dev.grupocgd.com "Harbor Production Environment")<br>
[Tower-PRD](https://awx.grupocgd.com "Harbor Production Environment")<br>

#### DEV -> Harbor,Gitlab,Jenkins,Nexus
- LDC6001APS69 __10.11.207.13__ - Linux [4 CPUs *2 cores = 8 cores + 16 GB RAM - RHELinux Server release 7.8 (Maipo)]<br>
#### PRD1 -> Nexus, Harbor, Jenkins Slaves, Squid, Sonarqube 
- LPC6001APS21 __10.11.34.45__ - Linux [4 CPUs *1 cores = 4 cores + 24 GB RAM - RHELinux Server release 7.6 (Maipo)]<br>
#### PRD2 -> Jenkins, Gitlab 
- LPC6001APS22 __10.11.34.46__ - Linux [4 CPUs *1 cores = 4 cores + 24 GB RAM - RHELinux Server release 7.6 (Maipo)]<br>
### Ansible Tower
#### DEV
- LDC6001CIT01 __10.11.208.20__  - Linux [2 CPUs *1 cores = 2 cores + 6 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
- LDC6001CIT02 __10.11.208.24__  - Linux [2 CPUs *1 cores = 2 cores + 6 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>

#### Production
- LPC6001CIT06 __10.11.33.40__  - Linux [2 CPUs *2 cores = 4 cores + 8 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
- LPC6001CIT07 __10.11.33.41__  - Linux [2 CPUs *2 cores = 4 cores + 8 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
- LPC6001CIT08 __10.11.33.42__  - Linux [2 CPUs *2 cores = 4 cores + 8 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
#### Isolated Node
- LPC6001CIT09 __10.29.19.32__  - Linux [2 CPUs *1 cores = 2 cores + 4 GB RAM - RHELinux release 8.2 (Ootpa)]<br>

### Rhsso
#### Quality
- LQC6001IAS05 __10.29.222.11__  - Linux [2 CPUs *1 cores = 2 cores + 16 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
- LQC6001IAS06 __10.29.222.12__  - Linux [2 CPUs *1 cores = 2 cores + 16 GB RAM - RHELinux Server release 7.7 (Maipo)]<br>
#### Production
- LPC6001IAS05 __10.29.50.15__  - Linux [2 CPUs *1 cores = 2 cores + 2 GB RAM - RHELinux Server release 7.6 (Maipo)]<br>
- LPC6001IAS06 __10.29.50.16__  - Linux [2 CPUs *1 cores = 2 cores + 2 GB RAM - RHELinux Server release 7.6 (Maipo)]<br>

### Troubleshooting
Everything you need to know of services running on these servers can be found in /deploy/INFO with user deploy (sudo -iu deploy) you can run:<br>
```
cat /deploy/INFO
```

```
###############################################################################################################################
        Informação sobre processos que devem estar a correr neste servidor Sonar
################################################################################################################################
    ##  Nexus ##

       utilizador : nexus ( sudo -iu nexus )

       docker ps -a                                                                                - lista container Nexus
       docker logs -f --tail=300 <container id>                                                    - logs da aplicacao Nexus
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-nexus-compose-v3.yml down    - stop container Nexus
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-nexus-compose-v3.yml up -d   - start container Nexus
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-nexus-compose-v3.yml restart - restart container Nexus
       docker exec -it <container id> bash                                                         - aceder o container Nexus
#################################################################################################################################
    ##  Docker GitLab Runner ##

       utilizador : deploy ( sudo -iu deploy )

       docker ps -a                                                                                            - lista container gitlab-runner
       docker logs -f --tail=300 <container id>                                                                - logs da aplicacao gitlab-runner
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-cgd-gitlab-runner-compose.yml down       - stop container gitlab-runner
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-cgd-gitlab-runner-compose.yml up -d      - start container gitlab-runner
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-cgd-gitlab-runner-compose.yml restart    - restart container gitlab-runner
       docker exec -it <container id> bash                                                                     - aceder o container gitlab-runner
#################################################################################################################################

    ##  Sonarqube ##

       utilizador : sonar ( sudo -iu sonar )

       docker ps -a                                                                                - lista container Sonar
       docker logs -f --tail=300 <container id>                                                    - logs da aplicacao Sonar
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-sonar-compose.yml down    - stop container Sonar
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-sonar-compose.yml up -d   - start container Sonar
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-sonar-compose.yml restart - restart container Sonar
       docker exec -it <container id> bash                                                         - aceder o container Sonar

#################################################################################################################################
    ##  Squid ##

       utilizador : deploy ( sudo -iu deploy )

       docker ps -a                                                                                - lista container Squid
       docker logs -f --tail=300 <container id>                                                    - logs da aplicacao Squid
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-squid-compose.yml down    - stop container Squid
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-squid-compose.yml up -d   - start container Squid
       /usr/local/bin/docker-compose -f /deploy/docker-compose/docker-squid-compose.yml restart - restart container Squid
       docker exec -it <container id> bash                                                         - aceder o container Squid
       docker cp <container id>:/etc/squid/squid.conf <dir>                            - aceder ao ficherio de configuração do squid

#################################################################################################################################
    ##  Harbor ##

       utilizador : nexus ( sudo -iu nexus )

       docker ps -a                                                                                - lista container Harbor
       docker logs -f --tail=300 <container id>                                                    - logs da aplicacao Harbor
       /usr/local/bin/docker-compose -f /deploy/harbor/harbor/docker-compose.yml down -v    - stop container Harbor
       /usr/local/bin/docker-compose -f /deploy/harbor/harbor/docker-compose.yml  up -d   - start container Harbor
       /usr/local/bin/docker-compose -f /deploy/harbor/harbor/docker-compose.yml restart - restart container Harbor
```
### RHSSO 
RHSSO does not run on containers and for that you will need to log on the machines (they are in DMZ) and change to user deploy after login<br>
You can add these to your ssh config file so you can log directly withouth passing through login server (you will need to exchange your ssh public key with the login servers):<br>
```
## RHSSO Maquinas Qualidade
  Host lqc6001ias05 lqc6001ias06
     ProxyCommand ssh -XA functionalUser@lqc6001lgs05 -W %h:%p
     User functionalUser

## RHSSO Maquinas Producao
  Host lpc6001ias05 lpc6001ias06
     ProxyCommand ssh -XA functionalUSer@lpc6001lgs10 -W %h:%p
     User functionalUser
  
```
```
sudo -iu deploy
```
and check your permissions with:<br>
```
-bash-4.2$ sudo -l
Matching Defaults entries for deploy on LQC6001IAS05:
    !visiblepw, always_set_home, match_group_by_gid, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS
    LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE
    LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User deploy may run the following commands on LQC6001IAS05:
    (root) NOPASSWD: /usr/bin/systemctl restart sdpnc.service
    (root) NOPASSWD: /usr/bin/systemctl start sdpnc.service
    (root) NOPASSWD: /usr/bin/systemctl stop sdpnc.service
    (root) NOPASSWD: /usr/bin/systemctl status sdpnc.service
    (root) NOPASSWD: /usr/bin/systemctl restart sdpnc-queuewai.service
    (root) NOPASSWD: /usr/bin/systemctl start sdpnc-queuewai.service
    (root) NOPASSWD: /usr/bin/systemctl stop sdpnc-queuewai.service
    (root) NOPASSWD: /usr/bin/systemctl status sdpnc-queuewai.service
    (root) NOPASSWD: /bin/systemctl * rh-sso
```
log file is located on:<br>
__/deploy/apps/rh-sso-7.3/standalone/log/server.log__

### Ansible Tower 
Tower also does not run on containers so you will need to log on the machines and try to find out what happenned. So in order to do that you will login with your functional user on the host machines and change to user awx __(sudo -iu awx)__ and check logs on __/var/log/tower/tower.log__<br>

You can add the following to your ssh config file:<br>
```
###############################################
MAQUINA ISOLATED NODE ANSIBLE TOWER
###############################################
Host  lpc6001cit09
  User functionalUser
  ProxyCommand  ssh -XA functionalUser@lpc6001lgs10 -W %h:%p
##############################################
###############################################
MAQUINAS DESENVOLVIMENTO ANSIBLE TOWER
###############################################
Host  ldc6001cit01 ldc6001cit02
  User functionalUser
##############################################
###############################################
MAQUINAS PRODUCAO ANSIBLE TOWER
###############################################
Host  lpc6001cit06 lpc6001cit07 lpc6001cit08
  User functionalUser
##############################################
```
Also to stop/start the services you can check the permissions in your functional user:<br>
```
[yp00799@ldc6001cit01 ~]$ sudo -l
Matching Defaults entries for yp00799 on ldc6001cit01:
    !visiblepw, always_set_home, match_group_by_gid, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS
    LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE
    LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User yp00799 may run the following commands on ldc6001cit01:
    (awx) NOPASSWD: ALL
    (nginx) NOPASSWD: ALL
    (rabbitmq) NOPASSWD: ALL
    (memcached) NOPASSWD: ALL
    (ALL) NOPASSWD: /usr/bin/ansible-tower-service *
    (ALL) NOPASSWD: /bin/chmod -R 775 /var/log/supervisor
[yp00799@ldc6001cit01 ~]$
```
### Update image to a newer version
For example to update sonarqube image running on lpc6001aps21 you will need to log on [Gitlab Homepage](https://gitlab.grupocgd.com "Gitlab Homepage") and clone the repo of sonarqube image:<br>
```
git clone https://gitlab.grupocgd.com/docker/docker-image-sonarqube.git
```
And alter the version of sonarqube on Dockerfile:<br>
```
ARG SONARQUBE_VERSION=8.0
ARG SONARQUBE_ZIP_URL=https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-${SONARQUBE_VERSION}.zip
```
After that you need to change the VERSION file to the newer version you just downloaded and Makefile will take care of naming the image and pushing it to nexus with the help of the __.gilab-ci.yml__ file<br>
Content of gitlab-ci.yml:<br>
```
stages:
       - build

variables:
        DOCKER_HOST: tcp://localhost:2375
        #DOCKER_DRIVER: overlay
        DOCKER_TLS_CERTDIR: ""

# Docker dind is a service used in order to allow docker in docker
# This way it is possible to use some docker features that require daemon
services:
        - nexus.grupocgd.com:8443/gitlab/docker_dind 


before_script:
        - export HTTP_PROXY="http://${proxyUser}:${proxyPassword}@proxy.grupocgd.com:8080"
        - export HTTPS_PROXY="http://${proxyUser}:${proxyPassword}a@proxy.grupocgd.com:8080"
        - echo "...Log in to the registry Harbor"
        - 'docker login -u $harborUser -p $harborToken registry.dev.grupocgd.com'

harbor: 
        image:
                name: nexus.grupocgd.com:8443/gitlab/docker_docker
        stage: build
        script:
            - 'make build'
```
For the last part of it you will log on the machine lpc6001aps21 and change to user sonar with sudo -iu sonar and pull the latest available image with:<br>
```
docker pull registry.dev.grupocgd.com/cgd/sonarqube:latest
```
and then stop and start the docker-compose file with user deploy:
```
/usr/local/bin/docker-compose -f /deploy/docker-compose/docker-sonar-compose.yml down
```
```
/usr/local/bin/docker-compose -f /deploy/docker-compose/docker-squid-compose.yml up -d
```

### Sonarqube
Add LDAP configuration to Sonarqube<br>
Add the following to the sonar.properties file<br>

```
sonar.security.realm=LDAP
sonar.security.savePassword=true

ldap.url: ldap://dsjxxi.grupocgd.com:389
ldap.baseDn: OU=CGD,DC=GrupoCGD,DC=com
ldap.bindDn: CN=Hercules APLIC_PRD1,OU=HRC - Services,OU=Utilizadores-Servicos,OU=Servicos-Transversais,OU=CGD,DC=GrupoCGD,DC=com
ldap.bindPassword: OhMVyGJf0cBHLzkuryDF
ldap.userObjectClass: user
ldap.user.baseDn: OU=CGD,DC=GrupoCGD,DC=com
ldap.user.request: (&(objectClass=user)(sAMAccountName={login}))

sonar.sourceEncoding=UTF-8
```
Add the following to configure database<br>:
```
sonar.jdbc.username=sonarqube
sonar.jdbc.password=sonarqube
sonar.jdbc.url=jdbc:postgresql://vip-lpc6001pos06b.grupocgd.com:5445/BDGPSNR01
sonar.jdbc.schema=SNR01
```