##  Hardware requirements for the number of existing data

Estimate: Taking into account the number of users and the number of existing binary repositories<br>

![nexus-requirements](assets/nexus-cpu.png "Nexus repo")
![nexus-requirements](assets/nexus-memory.png "Nexus repo")
![nexus-requirements](assets/nexus-diskspace.png "Nexus repo")

## Create Content Selector
Teams must only have permissions on their repositories in order to push or pull binaries.<br>
Repos are created with 'SIGLA APLICACIONAL' for releases and 'SIGLA APLICACIONAL-Snapshots' if they have the necessity to a snapshots repository.<br>
![nexus-repo](assets/nexus-repo.png "Nexus repo")
Teams are created in Roles so you can map an AD group directly.<br>
Here you can give some priviledges on specific repos or even more detailed on directories inside each repo.<br>
![nexus-role](assets/nexus-role.png "Nexus role")

Ideally Nexus should have only one repo assigned to each team and then inside that repo would be each project defined by 'SIGLA APLICACIONAL' or 'SIGLA APLICACIONAL-Snapshots'<br>
Imagine that you have a repo called Canais and inside you have many projects and you only want the team to acess its project.<br>
In order to have that segregation we will need to create a content selector as follows:<br> 
Example for team Canais in maven2 format repos:<br>
```
format == 'maven2' and path =~ "/|/Canais/|/Canais/SDGHP/.*"
```
This way local users or Teams from AD may only push or pull from the repo that they control<br>
![nexus-content](assets/nexus-content.png "Nexus content")

After that you will create a priviledge and assign it to the role you created above:<br>
![nexus-privilege](assets/nexus-privilege.png "Nexus privilege")

## Check Blobstore File Sizing
You can log on the machine either dev or production and check for the filesize directly with user nexus:<br>
```
[nexus@LPC6001APS21 blobs]$ du -sh *
419G    default
23G     nexus3
[nexus@LPC6001APS21 blobs]$ pwd
/deploy/nexus3/nexus/nexus-data/blobs
```
If you want detailed information on how each repo is growing on size you can log on<br>
[Nexus Dev](https://nexus.dev.grupocgd.com "Nexus dev")<br>
[Nexus Prd](https://nexus.grupocgd.com "Nexus prd")<br>
and run the manual task:
![nexus-task](assets/nexus-task.png "Nexus Tasks")
```
*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
*/

/*
 * Utility script that scans blobstores and reads the asset properties files within to summarize which repositories
 * are using the blob store, and how much space each is consuming and how much space could potentially be reclaimed by
 * running a compact blobstore task.
 *
 * The script retrieves the blobstore locations from the Nexus system and also all defined repositories.
 *
 * It is possible to specify a whitelist of repository names *OR* a blacklist (whitelist takes priority)
 * If a whitelist is provided, only those repositories whitelisted will be included.
 * If a blacklist is provided (and no whitelist), any repositories that are blacklisted will be omitted.
 *
 * Any empty repositories are also included.
 *
 * The script tabulates both the total size, and the size that could be reclaimed by performing a compact blob store
 * task.
 *
 * Script was developed to run as an 'Execute Script' task within Nexus Repository Manager.
 *

 * ---------------- BEGIN CONFIGURABLE SECTION -------------*

 * Whitelist - a list of repository names that should be the only items included.
 *
 *   For example: REPOSITORY_WHITELIST = ['maven-central', 'npm-hosted']
 */

REPOSITORY_WHITELIST = []

/* Blacklist - a list of repository names that should not be included.
 *   This will only apply if REPOSITORY_WHITELIST is not set
 *
 *   For example: REPOSITORY_BLACKLIST = ['maven-central', 'npm-hosted']
 */

REPOSITORY_BLACKLIST = []

/* ---------------- END CONFIGURABLE SECTION ---------------*/

import groovy.json.JsonOutput

import java.text.SimpleDateFormat
import org.sonatype.nexus.common.app.ApplicationDirectories
import org.sonatype.nexus.internal.app.ApplicationDirectoriesImpl
import org.slf4j.LoggerFactory


def log = LoggerFactory.getLogger(this.class)

ApplicationDirectories applicationDirectories =
        (ApplicationDirectories)container.lookup(ApplicationDirectoriesImpl.class.name)

List<File> blobStoreDirectories = []
hasWhitelist = REPOSITORY_WHITELIST.size() > 0
hasBlacklist = !hasWhitelist && REPOSITORY_BLACKLIST.size() > 0

//Default location of results is the Nexus temporary directory
File resultsFileLocation = applicationDirectories.getTemporaryDirectory()

Map<String, BlobStatistics> blobStatCollection = [:].withDefault { 0 }

class BlobStatistics
{
    int totalRepoNameMissingCount = 0
    long totalBlobStoreBytes = 0
    long totalReclaimableBytes = 0
    Map<String, RepoStatistics> repositories = [:]
}

class RepoStatistics {
    long totalBytes = 0
    long reclaimableBytes = 0
}

def collectMetrics(final BlobStatistics blobstat, Set<String> unmapped,
                   final Properties properties, final File propertiesFile) {
    def repo = properties.'@Bucket.repo-name'
    if(repo == null && properties.'@BlobStore.direct-path') {
        repo = 'SYSTEM:direct-path'
    }
    if(repo == null) {
        // unexpected - log the unexpected condition
        if(blobstat.totalRepoNameMissingCount <= 50){
            log.warn('Repository name missing from {} : {}', propertiesFile.absolutePath, properties)
            log.info('full details: {}', properties)
        }
        blobstat.totalRepoNameMissingCount++
    } else {
        if (!blobstat.repositories.containsKey(repo)) {
            if (!unmapped.contains(repo)) {
                if (!repo.equals('SYSTEM:direct-path')) {
                    log.info('Found unknown repository in {}: {}', propertiesFile.absolutePath, repo)
                }
                blobstat.repositories.put(repo as String, new RepoStatistics())
            }
        }

        if (blobstat.repositories.containsKey(repo)) {
            blobstat.repositories."$repo".totalBytes += (properties.size as long)
            if (!repo.equals('SYSTEM:direct-path')) {
                blobstat.totalBlobStoreBytes += (properties.size as long)
            }

            if (properties.'deleted') {
                blobstat.repositories."$repo".reclaimableBytes += (properties.size as long)
                if (!repo.equals('SYSTEM:direct-path')) {
                    blobstat.totalReclaimableBytes += (properties.size as long)
                }
            }
        }
    }
}

def passesWhiteBlackList(final String name) {
    if (hasWhitelist) {
        return REPOSITORY_WHITELIST.contains(name)
    }
    if (hasBlacklist) {
        return !REPOSITORY_BLACKLIST.contains(name)
    }
    return true
}

def getScanner(final File baseDirectory) {
    def ant = new AntBuilder()
    def scanner = ant.fileScanner {
        fileset(dir: baseDirectory) {
            include(name: '**/*.properties')
            exclude(name: '**/metadata.properties')
            exclude(name: '**/*metrics.properties')
            exclude(name: '**/tmp')
        }
    }
    return scanner
}

Map<String, Map<String, Boolean>> storeRepositoryLookup = [:].withDefault { [:] }

repository.getRepositoryManager().browse().each { repo ->
    def blobStoreName = repo.properties.configuration.attributes.storage.blobStoreName
    storeRepositoryLookup.get(blobStoreName).put(repo.name, passesWhiteBlackList(repo.name))
}

blobStore.getBlobStoreManager().browse().each { blobstore ->
    //check that this blobstore is not a group (3.15.0+)
    if (blobstore.getProperties().getOrDefault('groupable',true)) {
        //S3 stores currently cannot be analysed via this script, so ignore (3.12.0+)
        if (blobstore.getProperties().get("blobStoreConfiguration").type == "S3") {
            log.info("Ignoring blobstore {} as it is using S3",
                    blobstore.getProperties().get("blobStoreConfiguration").name);
        }
        else {
            try {
                blobStoreDirectories.add(blobstore.getProperties().get("absoluteBlobDir").toFile())
            }
            catch (Exception ex) {
                log.warn('Unable to add blobstore {} of type {}: {}',
                        blobstore.getProperties().get("blobStoreConfiguration").name,
                        blobstore.getProperties().get("blobStoreConfiguration").type, ex.getMessage())
                log.info('details: {}', blobstore.getProperties())
            }
        }
    }
    else {
        log.info("Ignoring blobstore {} as it is a group store",
                blobstore.getProperties().get("blobStoreConfiguration").name);
    }
}

log.info('Blob Storage scan STARTED.')
blobStoreDirectories.each { blobStore ->

    log.info('Scanning {}', blobStore.absolutePath)

    BlobStatistics blobStat = new BlobStatistics()

    Set<String> unmapped = new HashSet<>()
    storeRepositoryLookup[blobStore.getName()].each { key, value ->
        if (value) {
            blobStat.repositories.put(key, new RepoStatistics())
        } else {
            unmapped.add(key)
        }
    }

    getScanner(blobStore).each { File propertiesFile ->
        def properties = new Properties()
        try {
            propertiesFile.withInputStream { is ->
                properties.load(is)
            }
        } catch (FileNotFoundException ex) {
            log.warn("File not found '{}', skipping", propertiesFile.getCanonicalPath())
        }
        collectMetrics(blobStat, unmapped, properties, propertiesFile)
    }
    blobStatCollection.put(blobStore.getName(), blobStat)
}

blobStatCollection.each() { blobStoreName, blobStat ->
    RepoStatistics directPath = blobStat.repositories.remove('SYSTEM:direct-path')
    if (directPath!=null) {
        log.info("Direct-Path size in blobstore {}: {} - reclaimable: {}", blobStoreName, directPath.totalBytes, directPath.reclaimableBytes)
    }
}

def filename = "repoSizes-${new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date())}.json"
File resultsFile = new File(resultsFileLocation, filename)
resultsFile.withWriter { Writer writer ->
    writer << JsonOutput.prettyPrint(JsonOutput
            .toJson(blobStatCollection
                    .findAll {a, b -> b.repositories.size() > 0}
                    .toSorted {a, b -> b.value.totalBlobStoreBytes <=> a.value.totalBlobStoreBytes}))
}
log.info('Blob Storage scan ENDED. Report at {}', resultsFile.absolutePath)
```
This task will run in container background and will check for the size of the blobstore.<br>
You will need to be listening on the container logs to check which tmp file it will create:<br>
```
docker logs --tail 200 -f <containerID>
```
```
2020-12-03 11:20:52,943+0000 INFO  [quartz-4-thread-18]  *SYSTEM Script1 - Direct-Path size in blobstore default: 18867848 - reclaimable: 0
2020-12-03 11:20:52,944+0000 INFO  [quartz-4-thread-18]  *SYSTEM Script1 - Direct-Path size in blobstore nexus3: 1662905 - reclaimable: 0
2020-12-03 11:20:52,949+0000 INFO  [quartz-4-thread-18]  *SYSTEM Script1 - Blob Storage scan ENDED. Report at /nexus-data/tmp/repoSizes-20201203-112052.json
2020-12-03 11:20:52,949+0000 INFO  [quartz-4-thread-18]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Task complete
```
You can get more detail on the json created after running the task, it gives you size on every repo in Nexus:<br>
```
docker exec -it <containerID> cat /nexus-data/tmp/repoSizes-20201203-112052.json
```
Content of json file:<br>
```
{
    "default": {
        "repositories": {
            "SDEMI": {
                "reclaimableBytes": 0,
                "totalBytes": 8780594
            },
            "RDSDC": {
                "reclaimableBytes": 0,
                "totalBytes": 2967829
            },
            "agile-releases": {
                "reclaimableBytes": 10649,
                "totalBytes": 45755832518
            },
            "docker-group": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "centos-docker": {
                "reclaimableBytes": 0,
                "totalBytes": 944539779
            },
            "Agile_Public": {
                "reclaimableBytes": 0,
                "totalBytes": 10907279
            },
            "SDMED": {
                "reclaimableBytes": 0,
                "totalBytes": 2891893281
            },
            "SDBOX": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "generic-bins": {
                "reclaimableBytes": 0,
                "totalBytes": 16864707
            },
            "docker-gitlab": {
                "reclaimableBytes": 0,
                "totalBytes": 86101397
            },
            "maven-central": {
                "reclaimableBytes": 0,
                "totalBytes": 4928917878
            },
            "maven-snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 438
            },
            "nuget-hosted": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "cubi": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "SDPSC": {
                "reclaimableBytes": 0,
                "totalBytes": 20669414
            },
            "apt": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "SDOPI": {
                "reclaimableBytes": 0,
                "totalBytes": 211387
            },
            "jcentral": {
                "reclaimableBytes": 0,
                "totalBytes": 8780355
            },
            "thirdparty": {
                "reclaimableBytes": 0,
                "totalBytes": 974400103
            },
            "SDGHP": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "SDSPL": {
                "reclaimableBytes": 0,
                "totalBytes": 2914228
            },
            "Smartgwt": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "choco": {
                "reclaimableBytes": 0,
                "totalBytes": 7437394
            },
            "docker.elastic.co": {
                "reclaimableBytes": 0,
                "totalBytes": 4324392131
            },
            "Agile_BRMS_ODM": {
                "reclaimableBytes": 0,
                "totalBytes": 259808389
            },
            "Agile_RIIG": {
                "reclaimableBytes": 0,
                "totalBytes": 9080981
            },
            "home-made": {
                "reclaimableBytes": 0,
                "totalBytes": 438
            },
            "maven-repo": {
                "reclaimableBytes": 0,
                "totalBytes": 35146376
            },
            "registry.access.redhat.com": {
                "reclaimableBytes": 0,
                "totalBytes": 467388253
            },
            "nuget.org-proxy": {
                "reclaimableBytes": 0,
                "totalBytes": 139018902
            },
            "gitlab-charts": {
                "reclaimableBytes": 0,
                "totalBytes": 261131
            },
            "anaconda": {
                "reclaimableBytes": 0,
                "totalBytes": 1739386286
            },
            "SDAGR": {
                "reclaimableBytes": 0,
                "totalBytes": 22927
            },
            "SDJAC": {
                "reclaimableBytes": 0,
                "totalBytes": 126828
            },
            "jboss-eap-official": {
                "reclaimableBytes": 0,
                "totalBytes": 5163552
            },
            "SDSSN": {
                "reclaimableBytes": 0,
                "totalBytes": 69326722
            },
            "choco_group": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "SDRIG": {
                "reclaimableBytes": 0,
                "totalBytes": 4555949
            },
            "ansible": {
                "reclaimableBytes": 0,
                "totalBytes": 38932735
            },
            "google-yum": {
                "reclaimableBytes": 0,
                "totalBytes": 10159599
            },
            "ptsm_releases": {
                "reclaimableBytes": 0,
                "totalBytes": 438
            },
            "nuget-group": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "npm.org-proxy": {
                "reclaimableBytes": 0,
                "totalBytes": 934949700
            },
            "rh-sso-7.3.0.GA": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "centos-git": {
                "reclaimableBytes": 0,
                "totalBytes": 10631195
            },
            "centos-atom": {
                "reclaimableBytes": 0,
                "totalBytes": 2331298264
            },
            "SDGBC": {
                "reclaimableBytes": 0,
                "totalBytes": 4408988056
            },
            "ptsm": {
                "reclaimableBytes": 0,
                "totalBytes": 4467
            },
            "SDRPI": {
                "reclaimableBytes": 0,
                "totalBytes": 712224322
            },
            "maven-download": {
                "reclaimableBytes": 0,
                "totalBytes": 8842660
            },
            "02SDC": {
                "reclaimableBytes": 0,
                "totalBytes": 1572671
            },
            "releases": {
                "reclaimableBytes": 0,
                "totalBytes": 54651196
            },
            "gradle": {
                "reclaimableBytes": 0,
                "totalBytes": 4871661
            },
            "snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 102444
            },
            "SDCON-Confirming": {
                "reclaimableBytes": 0,
                "totalBytes": 1244450484
            },
            "yum-proxy-centos": {
                "reclaimableBytes": 0,
                "totalBytes": 11237322481
            },
            "composer-cgd": {
                "reclaimableBytes": 0,
                "totalBytes": 74799406
            },
            "jboss-datagrid-6.6.0-maven-repository": {
                "reclaimableBytes": 0,
                "totalBytes": 133147113
            },
            "wai-snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 2349548395
            },
            "SDTFC": {
                "reclaimableBytes": 0,
                "totalBytes": 3941995
            },
            "03SDC": {
                "reclaimableBytes": 0,
                "totalBytes": 176525
            },
            "jboss-wfk-2.7.0-maven-repository": {
                "reclaimableBytes": 0,
                "totalBytes": 141198051
            },
            "gcr.io": {
                "reclaimableBytes": 0,
                "totalBytes": 484799534
            },
            "centos-sublime": {
                "reclaimableBytes": 0,
                "totalBytes": 3884
            },
            "cgd-docker-private": {
                "reclaimableBytes": 0,
                "totalBytes": 66045625555
            },
            "k8s.gcr.io": {
                "reclaimableBytes": 0,
                "totalBytes": 3223542527
            },
            "centos-nux": {
                "reclaimableBytes": 0,
                "totalBytes": 7466625
            },
            "SDEPM": {
                "reclaimableBytes": 0,
                "totalBytes": 30297404
            },
            "nginx": {
                "reclaimableBytes": 0,
                "totalBytes": 4005367
            },
            "SDPNC": {
                "reclaimableBytes": 0,
                "totalBytes": 8864032595
            },
            "maven-releases": {
                "reclaimableBytes": 0,
                "totalBytes": 434563
            },
            "docker-hub": {
                "reclaimableBytes": 0,
                "totalBytes": 71620944071
            },
            "jboss-eap-7.0.0.GA-maven-repository": {
                "reclaimableBytes": 0,
                "totalBytes": 517263143
            },
            "SDGBC_Shared": {
                "reclaimableBytes": 0,
                "totalBytes": 389631
            },
            "SDOCE": {
                "reclaimableBytes": 0,
                "totalBytes": 435441
            },
            "SDAPB": {
                "reclaimableBytes": 0,
                "totalBytes": 59117388578
            },
            "rpm.nodesource.com": {
                "reclaimableBytes": 0,
                "totalBytes": 13880400
            },
            "ptsm_snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 438
            },
            "binaries": {
                "reclaimableBytes": 0,
                "totalBytes": 7154467010
            },
            "epel": {
                "reclaimableBytes": 0,
                "totalBytes": 1789732913
            },
            "rancher-stable-charts": {
                "reclaimableBytes": 0,
                "totalBytes": 45582
            },
            "SDGTM": {
                "reclaimableBytes": 0,
                "totalBytes": 18409
            },
            "SDRAO": {
                "reclaimableBytes": 0,
                "totalBytes": 18549
            },
            "nexus": {
                "reclaimableBytes": 0,
                "totalBytes": 2988950
            },
            "Agile_Snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 124079176646
            },
            "CRAN": {
                "reclaimableBytes": 0,
                "totalBytes": 72163707
            },
            "SDSFX": {
                "reclaimableBytes": 0,
                "totalBytes": 366155370
            },
            "SDKON": {
                "reclaimableBytes": 0,
                "totalBytes": 220451
            },
            "SDRMI": {
                "reclaimableBytes": 0,
                "totalBytes": 54245
            },
            "SDSGA": {
                "reclaimableBytes": 0,
                "totalBytes": 25883280
            },
            "docker-quay.io": {
                "reclaimableBytes": 0,
                "totalBytes": 4123008377
            },
            "public": {
                "reclaimableBytes": 0,
                "totalBytes": 7093
            },
            "TINK": {
                "reclaimableBytes": 0,
                "totalBytes": 286375089
            },
            "apache-snapshots": {
                "reclaimableBytes": 0,
                "totalBytes": 20966
            },
            "jboss-deprecated-items-group": {
                "reclaimableBytes": 0,
                "totalBytes": 410208
            },
            "SDASK": {
                "reclaimableBytes": 0,
                "totalBytes": 37126
            },
            "java.net-m2": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "java.net-m1": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "jboss-eap-6.4.0.GA-maven-repository": {
                "reclaimableBytes": 0,
                "totalBytes": 296468711
            },
            "conda-forge": {
                "reclaimableBytes": 0,
                "totalBytes": 873939508
            },
            "itext": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "SDPPO": {
                "reclaimableBytes": 0,
                "totalBytes": 123692584
            },
            "caixa-easy": {
                "reclaimableBytes": 0,
                "totalBytes": 82505330
            },
            "SDSML": {
                "reclaimableBytes": 0,
                "totalBytes": 41203
            },
            "centos-cgd": {
                "reclaimableBytes": 0,
                "totalBytes": 89500760
            },
            "centos-vscode": {
                "reclaimableBytes": 0,
                "totalBytes": 580877368
            },
            "pypi": {
                "reclaimableBytes": 0,
                "totalBytes": 423908744
            },
            "maven-public": {
                "reclaimableBytes": 0,
                "totalBytes": 9562817
            },
            "wai-releases": {
                "reclaimableBytes": 0,
                "totalBytes": 3791183671
            }
        },
        "totalBlobStoreBytes": 440519685727,
        "totalReclaimableBytes": 10649,
        "totalRepoNameMissingCount": 3015
    },
    "nexus3": {
        "repositories": {
            "w4m-ubuntu-mongo": {
                "reclaimableBytes": 0,
                "totalBytes": 447699
            },
            "raw-plat_dados-SDCDP": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "maven-xpand-cloudera-repos": {
                "reclaimableBytes": 0,
                "totalBytes": 414599428
            },
            "raw-global-noredeploy": {
                "reclaimableBytes": 393,
                "totalBytes": 291101018
            },
            "docker-w4m": {
                "reclaimableBytes": 3581141681,
                "totalBytes": 7245665128
            },
            "maven-xpand-spark": {
                "reclaimableBytes": 0,
                "totalBytes": 191092
            },
            "SDGHP-Snapshots": {
                "reclaimableBytes": 4186793562,
                "totalBytes": 14562006688
            },
            "python-global-cgd-redeploy": {
                "reclaimableBytes": 1261407,
                "totalBytes": 1261407
            },
            "w4m-ubuntu": {
                "reclaimableBytes": 5718474,
                "totalBytes": 362881815
            },
            "w4m-node-old-stable": {
                "reclaimableBytes": 0,
                "totalBytes": 7541
            },
            "docker-microsoft": {
                "reclaimableBytes": 0,
                "totalBytes": 262180924
            },
            "maven-xpand-cloudera": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "w4m-node-stable": {
                "reclaimableBytes": 10699,
                "totalBytes": 17810
            },
            "yum-global-redeploy": {
                "reclaimableBytes": 242085,
                "totalBytes": 354162470
            },
            "w4m-on-prem": {
                "reclaimableBytes": 85136,
                "totalBytes": 120007748
            },
            "debian": {
                "reclaimableBytes": 0,
                "totalBytes": 0
            },
            "w4m-node-latest": {
                "reclaimableBytes": 10698,
                "totalBytes": 18183826
            },
            "YUM-IUS": {
                "reclaimableBytes": 3812927,
                "totalBytes": 26993045
            },
            "w4m-ubuntu-security": {
                "reclaimableBytes": 2927892,
                "totalBytes": 83736564
            },
            "yum-plat_dados-SDCDP": {
                "reclaimableBytes": 104153,
                "totalBytes": 104153
            },
            "yum-plat-dados": {
                "reclaimableBytes": 36023,
                "totalBytes": 36023
            },
            "yum-global-repo": {
                "reclaimableBytes": 11030,
                "totalBytes": 11030
            }
        },
        "totalBlobStoreBytes": 23743595409,
        "totalReclaimableBytes": 7782156160,
        "totalRepoNameMissingCount": 0
    }
```
You can then convert the size in GB with your calculator or bash command:<br>
```
echo "23743595409/1024/1024/1024" | bc
```

### Get Nexus certificate details
```
curl --insecure -vvI https://nexus.grupocgd.com 2>&1 | awk 'BEGIN { cert=0 } /^\* SSL connection/ { cert=1 } /^\*/ { if (cert) print }'
```
```
 echo | openssl s_client -servername nexus -connect nexus.grupocgd.com:443 2>/dev/null | openssl x509 -text
```

### Download repo files from nexus repository using springboot
Clone the nexus repo from gitlab:<br>
```
git clone https://gitlab.grupocgd.com/docker/docker-image-nexus.git
```
Alter the credentials.properties file with user to access nexus and run:<br>
```
java -jar nexus3-export-1.0.jar http://nexus.grupocgd.com releases
```
The downloaded files will be store in your temp folder.<br>


### Import files from Nexus2 to Nexus3
You can also use this script on Nexus3 to Nexus3 just run int on your tmp folder previous created.<br>
Log on the host of nexus2 and go to the repo directory and run the script:<br>
```
#!/bin/bash

# copy and run this script to the root of the repository directory containing files
# this script attempts to exclude uploading itself explicitly so the script name is important
# Get command line params
while getopts ":r:u:p:" opt; do
        case $opt in
                r) REPO_URL="$OPTARG"
                ;;
                u) USERNAME="$OPTARG"
                ;;
                p) PASSWORD="$OPTARG"
                ;;
        esac
done
find . -type f -not -path './mavenimport.sh' -not -path '/.' -not -path '/^archetype-catalog.xml' -not -path '/^maven-metadata-local.xml' -not -path '/^maven-metadata-deployment*.xml' | sed "s|^./||" | xargs -I '{}' curl -u "$USERNAME:$PASSWORD" -X PUT -v -T {} ${REPO_URL}/{} -k;
```

execute it as follows:<br>
```
./maveninport -r https://nexus.grupocgd.com/repository/<repo existing in nexus3> -u user -p password
```