# Harbor deploy guide and info

## Instalar Harbor

* Maquina de desenvolvimento: ldc6001aps69
* Maquina de producao: lpc6001aps21


Existem duas opções para instalação do harbor:
- Versao offline: Download do ficheiro .tar do repositório e cópia para as maquinas destino. 
- Versao online: Próprio script de install faz o download das imagens do docker-hub

**Releases-Page:** https://github.com/goharbor/harbor/releases

Os requisitos necessarios para correr o harbor são:

![Harbor-Project](assets/requisitos_hardware.png)
![Harbor-Project](assets/requisitos_software.png)
![Harbor-Project](assets/requisitos_port.png)

Depois de conferir os requisitos e ter feito o download dos binários, vamos copia-los para a maquina destino e fazemos o unzip. 
Dentro da directoria agora descomprimida existe um ficheiro harbor.yml que é o ficheiro de configurações do harbor. 
Vamos alterar este ficheiro antes de fazer a instalação dos binários com os seguintes parâmetros:
```
hostname: nome-maquina.grupocgd.com
external_url: nome-dns-criado.grupocgd.com
harbor_admin_password: Password-desejada
data_volume: directorio-da-base-de-dados-local
proxy:
 http_proxy: http://squid.grupocgd.com:3128
 https_proxy: http://squid.grupocgd.com:3128
absolute_url: true
```

Caso queiram utilizar uma base de dados externa alteram a configuração neste trecho:
```
Uncomment external_database if using external database.
external_database:
harbor:
host: harbor_db_host
port: harbor_db_port
db_name: harbor_db_name
username: harbor_db_username
password: harbor_db_password
ssl_mode: disable
max_idle_conns: 2
max_open_conns: 0
```

Parâmetros adicionais e documentação podem ser encontrados em:

**harbor.yml params:** https://goharbor.io/docs/2.0.0/install-config/configure-yml-file/

Apos configuração do ficheiro harbor.yml vamos correr o script de instalação com a componente de chartmuseum para repositórios do tipo helm chart e com a componente de scan (clair)


```
[]$ ./install.sh --with-chartmuseum --with-clair

```
Outras componentes podem ser encontradas aqui:
https://goharbor.io/docs/2.0.0/install-config/run-installer-script/


## Configurar HA-PROXY
Configurar o site no projecto de HA-proxy nlb-inventory de modo a que consigamos fazer o balanceamento do(s) nó(s) de harbor. 

```
 registry.dev.grupocgd.com:
    ip_fe:      10.11.207.68
    ip_be:      10.12.207.68
    use_cache:  none
    ciphers_add: ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS:!AES256
    url_paths:
      root:
        endpoints:
          ldc6001aps69:
            address: 10.11.207.13:8060
```

!!! note "Nota"

    É necessário acrescentar essa gama de cifras que garante que versões de docker mais antigas consigam fazer o push das imagens para os repositórios uma vez que usam uma cifra mais antiga na autenticação no harbor.   


## Configurar OIDC configuration
Aceder ao nosso endereço de keycloak(RHSSO) no ambiente para o qual se quer configurar o harbor OIDC (https://auth.cqgrupocgd.com/auth/) (https://auth.grupocgd.com/auth/)

* 1º - Criar um Client dentro de um Realm no lado do Keycloack 
* 2º - Adicionar um  secret & Id para se autenticar no client
* 3º - Criar o callback URI dentro do cliente
* 4º - Criar dentro do User federation (LDAP connection) um mapper para os groups dentro da AD
* 5º - Criar dentro do Client um mapper para os grupos de LDAP 
* 6º - Acessar ao harbor e configurar o OIDC server e testar o acesso aos grupos. 

![Harbor-Project](assets/harbor_keycloak_client.png)
![Harbor-Project](assets/harbor_credentials_keycloak.png)
![Harbor-Project](assets/harbor_keycloak_client.png)
![Harbor-Project](assets/harbor-groups.png)
![Harbor-Project](assets/groups-continuation2.png)
![Harbor-Project](assets/harbor_configuration_auth.png)
![Harbor-Project](assets/groups-harbor.png)


## Harbor unknow blob error when pushing images
Correr um container com a opção -v data_dir onde temos o harbor instalado de modo a conseguir aceder ao ficheiro ( por exemplo usar o container commander3 existente no Nexus)
Alterar no ficheiro harbor/common/config/registry/config.yml

http:
  addr: :5000
  secret: placeholder
  **relativeurls: true**

Após esta alteração correr:

```
docker-compose -f docker-compose.yml up -d
```

## Delete OIDC user 
Fazer login no container da base de dados do harbor:
```
docker exec -it 95bc661ee7dc(harbor-db) psql -U postgres
psql (9.6.14)
Type "help" for help.
postgres=#
\c registry 
registry=# select *  from harbor_user; 
--
(1 row)

registry=#
registry=# delete from oidc_user where user_id = 5;
DELETE 1
registry=# delete from harbor_user where user_id = 5;
```



