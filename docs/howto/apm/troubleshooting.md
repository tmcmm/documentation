# Troubleshooting APM

### Cluster Slowness
There are four elements within the APM that will cause the APM cluster to slow down.

1. Number of Metrics (live and historic)
2. Number of agents & applications
3. Number of traces
4. EM Resources (number of EMs/CPU/Memory/Disk access/Network)

[Documentation](https://comm.support.ca.com/kb/introscope-enterprise-manager-troubleshooting/kb000093176 "Troubleshoot Documentation")

### Port already in use
__Check process running on port:__
```
lsof - t - i:5002
```
__kill process listenning on that port:__
```
fuser - k 5002/tcp
```
If it is a process that is in FIN_WAIT state it is not possible to kill
```
netstat - an | grep 5002 | grep FIN
```
```
while true; do netstat -an | egrep - i "5001"; done &
```
```
If nestat - ano | grep - qi fin; then echo encontrei; else echo nao encontrei; fi
```
__Kill process in FIN_WAIT:__

__record what tcp_max_orphans's current value__
```
original_value=$(cat /proc/sys/net/ipv4/tcp_max_orphans)
#set the tcp_max_orphans to 0 temporarily
echo 0 > /proc/sys/net/ipv4/tcp_max_orphans
```
```
echo $original_value > /proc/sys/net/ipv4/tcp_max_orphans
```

__verify with:__
```
netstat -an|grep FIN_WAIT1
```
### Errors in PCE Backend
__Backend hosts:__
```
upc6001hbb13
upc6001hbb14
upc6001hbb15
upc6001hbb16
upc6001hbb17
upc6001hbb18
upc6001hbb19
upc6001hbb20
upc6001hbb21
upc6001hbb22
upc6001hbb23
upc6001hbb24
upc6001hbb25
upc6001hbb26
```
1. Check log files from the back-end node from PCE and check if there is any timeout
2. Stop the service on the host that is having the problem

![log_path](assets/log_path.png "log_path")
![log_file](assets/log_file.png "log_file")
![log_console](assets/log_console.png "log_console")
![log_disable](assets/log_disable.png "log_disable")

## ThreadDump 

__Check Java version of Introscope:__
```
ps -ef | grep -i java
./jre/bin/java -version
java version "1.8.0_112"
Java(TM) SE Runtime Environment (build 1.8.0_112-b15)
Java HotSpot(TM) 64-Bit Server VM (build 25.112-b15, mixed mode)
```
__Heap dump:__
It contains all objects information heap memory.<br>
When ever we will get memory related issues that time we need to generate heap dump for finding root cause.<br>
__Example:__
```
jmap -dump:format=b,file=heap.bin 30007 > heapDump.txt
```

__Thread dump or Java core:__
It contains all threads information (waiting, run, hung, deadlock threads). Whenever we will get hung, dead lock issues that time we need to generate heap dump for finding root cause.<br>
```
jstack -l 9860 > /usr/opt/apps/introscope/threaddump.txt
```
Analyze  Thread Dump (process that consumes CPU)
```
jstack -l 9860 > /usr/opt/apps/introscope/threaddump.txt
top -H -p <pid>
```
Identify wich thread uses more cpu and look for the hexadecimal value on the thread file<br>
![hexadecimal](assets/hexadecimal.png "hexadecimal")

## GCHeap Mitigation

Heap mitigation site: [Heap Mitigation](https://gceasy.io/ "Heap")<br>

__What causes Consecutive full GCs?__<br>
 
Consecutive Full GCs are caused because of one single reason: 
Under allocation of JVM heap size or Under allocation of Perm Gen/metaspace.
 
- Your application’s traffic volume has started to grow since the last time you have tuned the JVM heap size. May be your business is improving, more users have started to use your application.
 
- During your peak volume time period, more objects would get created than normal time. May be you didn’t tune your JVM for peak traffic volume or your peak traffic volume has increased since the last time you have tuned the JVM heap size.
 
__Solutions:__<br>
 
- Increase JVM Heap Size<br>
 
>Since consecutive Full GCs runs due to lack of memory, increasing JVM heap size should solve the problem. Say suppose you have allocated JVM heap size to be 2.5GB, then try increasing it to 3 GB and see whether it resolves the problem. JVM heap size can be increased by passing the argument: “-Xmx”. Example:<br>
 
-Xmx3G<br>
 
>This argument will set the JVM heap size to be 3 GB. If it still doesn’t resolve the problem then try increasing the JVM heap size step by step. Over-allocation of JVM heap is also not good either, it might increase the GC pause time as well.
 
- Increase Perm Gen/Metaspace Size
 
 >Some times full GCs can run consecutively, if Perm Gen or metaspace is under-allocated. In such circumstance just increase the Perm Gen/Metaspace size.
 
- Add more JVM instances
 
>Adding more JVM instances is an another solution to this problem.When you add more JVM instance, then traffic volume will get distributed. The amount of traffic volume handled by one single JVM instance will go down. If less traffic volume is sent to a JVM instance, then less objects will be created. If less objects are created, then you will not run into the consecutive Full GC problems. 
 
- Reduce long GC pauses
 
[Long Pauses Analyzes](https://blog.gceasy.io/2016/11/22/reduce-long-gc-pauses/ "Long pauses")  

- Choice of GC Algorithm
 
>Choice of GC algorithm has a major influence on the GC pause time. Unless you are a GC expert or intend to become one 🙂 or someone in your team is a GC expert – you can tune GC settings to obtain optimal GC pause time. Assume if you don’t have GC expertise, then I would recommend using G1 GC algorithm, because of it’s auto-tuning capability. In G1 GC, you can set the GC pause time goal using the system property ‘-XX:MaxGCPauseMillis.’ Example:
 
 -XX:MaxGCPauseMillis=200<br>
 
>As per the above example, Maximum GC Pause time is set to 200 milliseconds. This is a soft goal, which JVM will try it’s best to meet it. If you are already using G1 GC algorithm and still continuing to experience high pause time, then refer to this article.
 

 
 
Understanding GC line<br>
[Times: user=25.56 sys=0.35, real=20.48 secs]<br>
 
 
>Real is wall clock time – time from start to finish of the call. This is all elapsed time including time slices used by other processes and time the process spends blocked (for example if it is waiting for I/O to complete).
 
>User is the amount of CPU time spent in user-mode code (outside the kernel) within the process. This is only actual CPU time used in executing the process. Other processes and time the process spends blocked do not count towards this figure.
 
>Sys is the amount of CPU time spent in the kernel within the process. This means executing CPU time spent in system calls within the kernel, as opposed to library code, which is still running in user-space. Like ‘user’, this is only CPU time used by the process.
 
>User+Sys will tell you how much actual CPU time your process used. Note that this is across all CPUs, so if the process has multiple threads, it could potentially exceed the wall clock time reported by Real.
 
 
- Background IO Traffic
 
>If there is a heavy file system I/O activity (i.e. lot of reads and writes are happening) it can also cause long GC pauses. This heavy file system I/O activity may not be caused by your application. Maybe it is caused by another process that is running on the same server, still, can cause your application to suffer from long GC pauses. Here is a brilliant article from LinkedIn Engineers, which walks through this problem in detail.
 
 [Long GC Article](https://engineering.linkedin.com/blog/2016/02/eliminating-large-jvm-gc-pauses-caused-by-background-io-traffic "Long gc pauses")     
 
>When there is a heavy I/O activity, you will notice the ‘real’ time to be significantly more than ‘user’ time. Example:
 
[Times: user=0.20 sys=0.01, real=18.45 secs]<br>