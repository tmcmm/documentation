## APM Theory

### APM quality infrasctruture
__EM FARM:__<br>
LQC6001MON03 - Linux: [2 CPUs *4 cores = 8 cores  + 16 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
LQC6001MON07 - Linux [2 CPUs *4 cores = 8 cores + 16 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
LQC6001MON11 - Linux: [2 CPUs* 4 cores = 8 cores  + 16GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
__TIM FARM-PRINT:__<br>
LQZ6001MON01 - Linux [4 CPUs *4 cores = 16 cores + 8 GB RAM -RHELinux Server release 6.10 (Santiago)]<br>
LQZ6001MON02 - Linux: [4 CPUs *4 cores = 16 cores  + 8 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
__MOM/WEBVIEW Cluster:__<br>
LQC6001MON04 - Linux [4 CPUs *4 cores = 16 cores + 20 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
LQC6001MON12 - Linux [4 CPUs *4 cores = 16 cores + 20 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>
__DATABASE- PostgreSQL:__<br>
LQC6001MON08- Linux [4 CPUs *2 cores = 8 cores + 24 GB RAM - RHELinux Server release 6.10 (Santiago)]<br>

![Infrastructure](assets/apm-quality.png "Infrastruture Production")

### APM production infrasctruture
__EM FARM:__<br>
LPC6001MON08 - Linux: [2 CPUs *4 cores = 8 cores  + 64 GB RAM - RHEL 6.9]<br>
LPC6001MON11 - Linux [2 CPUs *4 cores = 8 cores + 64 GB RAM - RHEL 6.9]<br>
LPC6001MON12 - Linux: [2 CPUs* 4 cores = 8 cores  + 64GB RAM - RHEL 6.9]<br>
__TIM FARM-PRINT:__<br>
LPC6001MON24 - Linux [2 CPUs *6 cores = 12 cores + 16 GB RAM - RHEL 6.9]<br>
LPC6001MON25 - Linux: [2 CPUs *6 cores = 12 cores  + 16 GB RAM - RHEL 6.9]<br>
__MOM/WEBVIEW Cluster:__<br>
LPC6001MON22 - Linux [2 CPUs *6 cores = 12 cores + 32 GB RAM - RHEL 7.4]<br>
LPC6001MON23 - Linux [2 CPUs *6cores = 12 cores + 32 GB RAM - RHEL 7.4]<br>
__DATABASE- PostgreSQL:__<br>
LPC6001POS02

Infrastructure: 
![Infrastructure](assets/Infra.png "Infrastruture Production")
## **Components:**
__Enterprise Manager:__<br>
- The Enterprise Manager acts as the repository for Introscope performance metrics. Receives performance metrics from one or more introscope agents. We can deploy the various Enterprise Managers in different ways depending on the size and complexity of the environment itself.<br>
__Agents:__<br>
- Introscope agents collect and report various types of performance metrics:<br>
     Various components within an application<br>
     Application servers<br>
    Own computing environment<br>
- The agent is deployed for each JVM (Java Virtual Machine), PHP, or .NET process.The total number of agents depends on the Introscope sizing itself.The agents report every 15 seconds by default.<br>
-  We can also import generic real-time data into the Introscope through a modified agent called EPA (Environment Performance Agent). This agent uses simple scripts that allow Introscope to monitor virtually performance metrics for any type of application subsystem, that is, for example, we can use an EPA agent to monitor the operating system, middleware messages or even transactional servers.<br>
__Investigator:__<br>
- The investigator contains two primary tabs:<br>
    - Metric browser: shows the visualization of the metrics in a tree format, this allows Introscope users to see different types of information about the selected component or resource<br>
    - Triage Map: allows you to view the application-related triage map, which is a central application view around the other monitored applications. It allows to see graphically the components that make up the application, in order to be able to see the errors and the status.<br>
__Webview:__
-  The webview is a web interface for dashboarding, metrics and other application elements monitored by Introscope.
- Within the webview we can:<br>
    - Check application performance metric of Introscope on any computer that has a compatible browser without the need to install any clients.<br>
    -  Only the workstation lets us edit and create dashboards and run reports.<br>
__Team Center:__<br>
 - Team center gives an overview of the application environment.<br>
 - The dashboards show the general overview.<br>
 - The map shows the relationships between the individual components of the environment<br>
__SmartStor:__<br>
- The smartstor is the largest of the 4 data stores to store metrics and transactional data. This stores all application performance data (Introscope metrics).<br>
Smartstor allows:<br>
- Analyze historical metrics<br>
  - Identify the root cause of the application downtime.<br>
  - Analyze performance capacity without the need for an external database<br>
__APM database:__<br>
 - Both Introscope and CA CEM use the APM database. This data is used by the Team Center perspective map and for CA CEM incidents and defects. The database also stores all relational configuration data.<br>
__CA CEM:__<br>
- CA CEM monitors business transactions to isolate the cause of the problem in the data center. Measures the performance and quality of transactions and identifies defects and variance and quantifies the impact on users and the business.<br>
__TIM:__<br>
- Transaction Impact Monitor server is responsible for recording and observing HTTP packets, identifying user logins, and related transactions. TIM is also used to monitor and report defects and other statistics to CA CEM. There can be one or more TIMs in a CA CEM environment.<br>


## Relevant Links:
- __Quality Environment:<br>__
    [Teamcenter Quality Environment](https://apm.cqgrupocgd.com/ApmServer/ "Teamcenter Quality Environment")<br>
    [Teamcenter Quality Environment - Direct Link  without HA-PROXY](https://lqc6001mon04.grupocgd.com:8443/ApmServer/ "Teamcenter Quality Environment")<br>
    [Workstation](https://apm.cqgrupocgd.com:5443/ "Workstation Quality Environment")<br>
    [Custom Experience Monitoring (CEM)](https://apm.cqgrupocgd.com:444 "CEM Quality Environment")<br>
    [TIM-01 lqz6001mon01](http://10.24.204.11/cgi-bin/ca/apm/tim/index "Tim Quality Environment 01")<br>
    [TIM-02 lqz6001mon02](http://10.24.204.13/cgi-bin/ca/apm/tim/index  "Tim Quality Environment 02")<br>
    [EEM Quality Environment](https://eemqld:5250/spin/eiam/eiam.csp "EEM Quality Environment")<br>
    [Rest API Quality Environment](https://apm.cqgrupocgd.com:444/apm/appmap/graph/incremental?sinceVersion= "REST API Quality Environment")<br>

- __Production Environment:<br>__
    [Teamcenter Production Environment](https://apm.grupocgd.com/ApmServer/ "Teamcenter Quality Environment")<br>
    [Teamcenter Production Environment - Direct Link without HA-PROXY](https://lpc6001mon22.grupocgd.com:8443/ApmServer/ "Teamcenter Quality Environment")<br>
    [Workstation](https://apm.grupocgd.com:5443/ "Workstation Production Environment")<br>
    [Custom Experience Monitoring (CEM)](https://apm.grupocgd.com:444 "CEM Production Environment")<br>
    [TIM-01 lpc6001mon24](http://10.27.132.36/cgi-bin/ca/apm/tim/index "Tim Production Environment 01")<br>
    [TIM-02 lpc6001mon25](http://10.27.132.37/cgi-bin/ca/apm/tim/index  "Tim Production Environment 02")<br>
    [EEM Production Environment](https://eem:5250/spin/eiam/eiam.csp "EEM Production Environment")<br>
    [Rest API Production Environment](https://apm.grupocgd.com:444/apm/appmap/graph/incremental?sinceVersion= "REST API Production Environment")<br>
