# Managing APM

### DEBUG mode on collectors
```
[yyasig2@lpc6001mon12 ~]$ perl -p -i -e 
's/log4j.logger.Manager=INFO/log4j.logger.Manager=DEBUG/g' <collector_dir>/config/IntroscopeEnterpriseManager.properties
```
__Reference:__ [Documentation](https://knowledge.broadcom.com/external/article/33600/apm-logs-and-how-to-enable-debug.html "Broadcom Documentation")

### Associate Agents to a Domain
To assign an agent to a domain you need to alter __customprocessname__ in the __agent.profile__.<br>
Available domains can be found in the file __domains.xml__ in the MOM host under /usr/opt/apps/introscope/config/domains.xml <br>


### Log GC Heap on JVM startup
You have to add to the file __/usr/opt/apps/introscope/Introscope_Enterprise_Manager.lax__
```
 -verbose:gc -Xloggc:/usr/opt/apps/introscope/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=3M
```
### Integrate EEM servers in APM
Users are managed here in EEM that integrates with AD<br>
Open the realms.xml file in the __EM_Home/config__ directory<br>

```
<realm descriptor="EEM Realm" id="EEM Server 1" active="true">
<realm descriptor="EEM Realm" id="EEM Server 2" active="true">
  <realm active="true" id="EEM 2" descriptor="EEM Realm">
        <property name="enableAuthorization">
            <value>true</value>
        </property>
        <property name="password">
            <value></value>
        </property>
        <property name="appname">
            <value>AppPerfMgmt</value>
        </property>
        <property name="host">
            <value>eem</value>
        </property>
        <property name="plainTextPasswords">
            <value>true</value>
        </property>
        <property name="username">
            <value>EiamAdmin</value>
        </property>
    </realm>
    <realm active="true" id="Local Users and Groups" descriptor="Local Users and Groups Realm">
        <property name="usersFile">
            <value>users.xml</value>
        </property>
    </realm>
    <realm active="true" id="SAML Users and Groups" descriptor="SAML Realm"/>
</realms>
```

### Add Domain into EEM
You have to create a xml file and pass it to the __EEM_Server__:__spc6001sdm03__ navigate to the directory C:\Program Files\CA\SharedComponents\iTechnology and run:

```
safex.exe -h localhost -u EiamAdmin -p <keepass> -f domains.xml
```

Log on [EEM PRD Homepage](https://eem:5250/spin/eiam/eiam.csp "EEM Homepage")  with user __eiamadmin__ on application __AppPerfMgmt__ with the password (on keepass) and confirm that the realm was created

![eem](assets/eem_prd.png "eem prd")

__domain.xml file example:__
```
<Safex>
   <Attach label="AppPerfMgmt"/>
   <!-- add policies -->
   <Add>
        <Policy name="Canais-PRD_Domain" folder="/Policies">
        <Description>Dominio Canais-PRD_Domain</Description>
        <Identity>ug:CanaisUser</Identity>
        <Action>full</Action>
        <ResourceClassName>Domain</ResourceClassName>
        <Resource>Canais-PRD_Domain</Resource>
        </Policy>
     </Add>
<Detach/>
</Safex>
```

### Apply Hotfixes
- This hotfix script updates the following product components:<br>
Enterprise Manager, WebView, Workstation, CommandCenter Server, PostgreSQL database and APMSQL Server.

- Stop:
-Enterprise Manager (MOM/collector)
-WebView
-Workstation
-CommandCenter Server
-APMSQL Server
-PostgreSQL

!!!note "Note"
        Individual components that are installed together can be also patched  in one go, for example, EM, Webview and APMSQL Server

- Copy APM10.7.0HF11.jar to the HOME directory of the product components you would like  to apply the hotfix. For example, if WebView is installed separately, run the hotfix script from that WebView HOME directory.

- Execute
__If windows:__ jre\bin\java -jar APM10.7.0HF11.jar<br>
__If unix:__ jre/bin/java -jar APM10.7.0HF11.jar<br>

!!!note "Note"
        Make sure to run the script using the appropriate user credentials.

!!!note "Note" 
        Since PostgreSQL directory does not contain jre, if the Database patch is being applied  by copying the hotfix jar in the postgreSQL install directory, then the customer needs to invoke the jar using a supported java present in the machine.

- Verify the results: 
>> Hotfix 10.7.0-HF11 was applied successfully
If the HF script fails for some reason with the below message, 
the script will automatically rollback the changes. Open a support
case and attach the 
hotfix.log file.

Or<br>

>> Hotfix 10.7.0-HF11 was rolled back successfully

- Start:
-Enterprise Manager (MOM/collector)
-WebView
-Workstation
-CommandCenter Server
-APMSQL Server
-PostgreSQL
The following KB article answers few questions related to hotfix
installation -
https://support.ca.com/us/knowledge-base-articles.TEC1876984.html



### APM Agents Licensing
APM agents are obtained through a plugin installed in each of the collectors that counts the .NET and JAVA agent licenses.

This plugin (installed in __EM Install Directory/product/enterprisemanager/plugins__ excludes EPA agents that should not be counted for this purpose.<br>

After installing the tool, EMs have a new metrics tab called licensing and from this tab it is possible to obtain licensed agents by creating a Metric grouping (lqc6001mon03@(.*)\) and then creates a calculator that does the sum of these.<br>


The installed packages are:<br>
com.wily.swat.supportability.licensing_9.1.0.jar -> __EM Install Directory/product/enterprisemanager/plugins__<br>
supportability.licensing.properties -> __EM Install Directory/config__<br>

You can check licenced agents in the webview console:
![apm-agents](assets/apm-agents.png "apm agents")


### Configure TomcatAgent
You will need to go the the network shared folder that contains the binaries:<br>
__\\software\Downloads\ACE-PW-Packages\Introscope\APM\10.7\APM 10.7__<br>
__GEN500000000000813.tar__
![tomcat-agents](assets/tomcat-agent.png "tomcat agents")

### Configure TomcatAgent


### Tunnel script on lpc6001mon05
We have some agents on the DMZ zone that are still attached to the old MOM so for workaround, there is a script that is tunneling the requests to the lpc6001mon05 port to new host apm.grupocgd.com
```
#!/bin/bash

while true; do
   ps -ef | grep "ssh -f localhost -L lpc6001mon05:5001:apm.grupocgd.com:5001 -N" | grep -v grep > /dev/null 2>&1;
   proc_status=$?;
   echo $proc_status
   if [ ! $proc_status -eq 0 ]; then
      echo "proc failed. rexecuting... $(date "+%m%d%Y %T")"
      ssh -f localhost -L lpc6001mon05:5001:apm.grupocgd.com:5001 -N >> /var/tmp/apm_socket_script.log 2>&1 &
   else
      echo "proc is online: $(date "+%m%d%Y %T")";
   fi
   sleep 5;
done
```
__Start the script:__
```
nohup ./home/YYASIG2/apm_socket_connection.sh >> /var/tmp/apm_socket_script.log 2>&1 &
```

### Create a Local User
Access to file __EM_Home/config/users.xml__

- Set an encrypted password manually. a.Set plaintextPasswords="false" in the users.xml file. 


- Run the appropriate script located in the __EM_Home/tools directory__. <br>
For Windows, MD5Encoder.bat "password"<br>
For UNIX, MD5encoder.sh "password"<br>

!!!note "Note"
     When running the MD5Encoder.sh script, use a backslash to escape any special characters in your password. For example, if your password is pa$word, place a backslash before the dollar sign ("$") character to make the script run correctly. The correct command line is: 
```
./MD5Encoder.sh pa\$word
```
- Copy the generated encrypted password, and paste it into the second line of the users.xml file. 

For example,
```
<user password="5b5ab9639b79259f54bc39515540aeaf" name="john"/>
```

### Add Users to ACL's in EEM
Log on the EEM server (App - AppPerfMgmt User - EiamAdmin Pwd - On Keepass) you can find the eem server urls in the [Apm-Relevant Links Documentation](apm.md)<br>
You can add the user to the default ACLs in the tab __Manage Identities__, search the user in the default global users
![eem](assets/eem.png "eem user identities")

### Create other ACL other than default
After logging on the EEM server you can go to the tab __Manage Identities__ and click create new application user
![eem](assets/eem1.png "eem user identities")
Then add the user to the new created group like the previous step.
After that you can go on __Manage Access Policies__ tab and choose your domain, and add the new group with the ACL you want.
![eem](assets/eem2.png "eem user identities")
![eem](assets/eem3.png "eem user identities")
