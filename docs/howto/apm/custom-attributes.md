# Custom Attributes APM

In order to configure custtom attributes in APM (to be mapped in CMDB) we will use the API (series of functions accessible via programming) of APM.<br> 
In the web context the API is a defined set of request and http response. In the web 2.0 version, the idea of SOAP (Simple Object Access Protocol) requests is a protocol to exchange structured information on a decentralized platform
and distributed.<br> 
The protocol is based on the Markup Language Extensible (XML) for your message format)) instead of REST (Representational State Transfer, is an architectural style which defines a set of restrictions and properties based on HTTP.<br>

Web Services that obey the REST architectural style, or web services RESTful, provide interoperability between computer systems in the Internet.<br>
REST-compliant web services allow systems to applicants to access and manipulate textual representations of using a uniform, predefined set of stateless operations.<br>
Other types of web services, such as SOAP web services, expose theirarbitrary sets of operations.<br>

For this we will install Postman which is a service that tests the REST clients and makes calls to the API using GET, POST, PUT methods.<br>

To use Postman we have to define an api token on the APM side to that we can have valid API authentication.<br>

We will need to log on apm with the AD user [Webview](https://apm.grupocgd.com/ApmServer/ "APM's Homepage") and create a bearer token in the security tab menu.

![Infrastructure](assets/token.png "Bearer-Token")

After defining this token, we need to launch Postman and put the parameters in the GET method and the token in the Authorization Bearer<br>

__url:__ https://apm.cqgrupocgd.com:444/apm/appmap/vertex?q=(attributes.cmdbResourceType: BUSINESSTRANSACTION OR attributes.applicationName: ceb)<br>

**Apm-cmdb-token (PRD):**<br>
**Bearer 0a0abb92-5ad4-46f6-af13-2249d987e0c1**

**Apm-cmdb-token (QLD):**<br>
**Bearer 4bf35638-eec9-48c0-8394-db3884119f1d**

We receive the result from the query back. These attributes can be parameterized through the python script or else manually on each object or using the PUT method on the postman.<br>


[Gitlab Project](https://gitlab.grupocgd.com/canais/ca-apm-atc-custom-attributes-rules "Gilab python project")<br>
We need to import this project containing the python script to interact with APM rest api<br>

**For cmdb integration purposes, the following CA APM TeamCenter (ATC) attributes should be used:**<br>
- cmdbApplicationName - Name of the relevant application or application component - value type: string<br>
- cmdbApplicationServer - Application instance / process name - value type: string<br>
- cmdbResource - If the vertex is eligible for import into cmdb - type of value: true / false<br>
- cmdbResourceType - What type of resource / object - type of value: BUSINESSTRANSACTION / GENERICFRONTEND / DATABASE<br>
- cmdbHostname - Reference to the hostname where the vertex resides (In order to relate to existing hostnames in cmdb)<br>
- cmdbDatabase - Database name (application backend). Example - bdoqpce. Only applies to cmdbResourceType: DATABASE<br>
- cmdbCGDSiglaAplicacional - Name of the application acronym CGD (PNAD internal process)<br>

![cmdb](assets/cmdb_attr.png "Cmdb attributes")

__rules.json file for PCE:__
```
{
  "rules": [
    {
      "name":"Add applicationServer and applicationPlatform attribute based on ca apm agent name of sdpce.",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            { "itemType" : "attributeFilter", "attributeName": "agent", "attributeOperator": "MATCHES", "values": [ "*-sdpce-*" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"agent", "expression":".*-sdpce-(.*)$" }
      ],
      "attributes": {
        "applicationServer": "{0}",
        "applicationPlatform": "sdpce"
      }
    },
    {
      "name":"Add cmdbApplicationServer attribute and define sdpce frontends as cmdb resources",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationPlatform", "attributeOperator": "IN", "values": [ "sdpce" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] },
            {  "itemType" : "attributeFilter", "attributeName": "agent", "attributeOperator": "NOT_MATCHES", "values": [ "*-sdpce-pce", "*-sdpce-pce-*", "*-sdpce-bkomdw", "*-sdpce-genmdw", "*-sdpce-bkoweb" ] }
            ]
          },
          {
            "andItems":[
              {  "itemType" : "attributeFilter", "attributeName": "applicationServer", "attributeOperator": "IN", "values": [ "bkoweb" ] },
              {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] },
              {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "NOT_IN", "values": [ "cdo", "ceb" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"applicationServer", "expression":"(.*)" },
        { "attribute":"applicationName", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbApplicationServer": "{0}",
        "cmdbApplicationName": "{1}",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "SDPCE"
      }
    },
    {
      "name":"Add cmdbApplicationServer attribute to backoffice emulators",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
              {  "itemType" : "attributeFilter", "attributeName": "applicationServer", "attributeOperator": "IN", "values": [ "bkoweb" ] },
              {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] },
              {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "IN", "values": [ "cdo", "ceb" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"applicationServer", "expression":"(.*)" },
        { "attribute":"applicationName", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbApplicationServer": "{0}",
        "cmdbApplicationName": "{1}-emul",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "SDPCE"
      }
    },
    {
      "name":"Aggregate sdpce ejb frontends with application name",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationPlatform", "attributeOperator": "IN", "values": [ "sdpce" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] },
            {  "itemType" : "attributeFilter", "attributeName": "agent", "attributeOperator": "MATCHES", "values": [ "*-sdpce-pce", "*-sdpce-pce-*", "*-sdpce-bkomdw", "*-sdpce-genmdw" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"applicationServer", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbApplicationServer": "{0}",
        "cmdbApplicationName": "{0}",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "SDPCE"
      }
    },
    {
      "name":"Add cmdbApplicationName attribute and define business transaction type",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "IN", "values": [ "cdo", "ceb", "CDO-APPS", "CEB-APPS", "cdoMobile", "cebMob" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "BUSINESSTRANSACTION" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"applicationName", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbApplicationName": "{0}",
        "cmdbResource": "true",
        "cmdbResourceType": "BUSINESSTRANSACTION",
        "cmdbCGDSiglaAplicacional": "SDPCE"
      }
    },
    {
      "name":"Set Argus application cmdb attributes ",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "IN", "values": [ "Argus" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] }
            ]
          }
          ]
      },
      "attributes": {
        "cmdbApplicationName": "Argus",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "SDPSC"
      }
    },
    {
      "name":"Set SMWS application cmdb attributes ",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "IN", "values": [ "CA GSE Authentication Service" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] }
            ]
          }
          ]
      },
      "attributes": {
        "cmdbApplicationName": "SMWS",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "SDPSC"
      }
    },
    {
      "name":"Set WAI application cmdb attributes ",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "applicationName", "attributeOperator": "MATCHES", "values": [ "UW*" ] },
            {  "itemType" : "attributeFilter", "attributeName": "type", "attributeOperator": "IN", "values": [ "GENERICFRONTEND" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"applicationName", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbApplicationName": "{0}",
        "cmdbResource": "true",
        "cmdbResourceType": "GENERICFRONTEND",
        "cmdbCGDSiglaAplicacional": "AISDC"
      }
    },
    {
      "name":"Set database resources",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "tablespace", "attributeOperator": "MATCHES", "values": [ ".*" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"tablespace", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbDatabase": "{0}",
        "cmdbResource": "true",
        "cmdbResourceType": "DATABASE"
      }
    },
    {
      "name":"Set cmdbHostname",
      "query": {
          "includeStartPoint": false,
          "orItems":[
          {
            "andItems":[
            {  "itemType" : "attributeFilter", "attributeName": "cmdbResource", "attributeOperator": "IN", "values": [ "true" ] },
            {  "itemType" : "attributeFilter", "attributeName": "hostname", "attributeOperator": "MATCHES", "values": [ ".*" ] }
            ]
          }
          ]
      },
      "regex":[
        { "attribute":"hostname", "expression":"(.*)" }
      ],
      "attributes": {
        "cmdbHostname": "{0}"
      }
    }
  ]
}
```

**Pipeline Jenkins custom-attributes:**
```
pipeline {
    agent any
    options{timestamps()}

    // Set Pipeline environment variables
    environment {
        GIT_URL = "http://lpc6001cit04.grupocgd.com/canais/ca-apm-atc-custom-attributes-rules.git"
        GIT_BRANCH = "master"
        RULES_GIT_URL = "http://lpc6001cit04.grupocgd.com/jenkins-pipelines/apm-custom-attributes-pipeline-library.git"
        RULES_GIT_BRANCH = "master"
        GIT_AUTH = "gitlab_jenkins"
        PROXY_AUTH = credentials('CGD_PROXY_AUTH')
        PROXY = "http://${PROXY_AUTH}@proxy.grupocgd.com:8080"
        APM_REST_URL_CQ = "https://apm.cqgrupocgd.com:444/apm/appmap"
        APM_API_TOKEN_CQ = credentials('APM_API_TOKEN_CQ')
        APM_REST_URL_PR = "https://apm.grupocgd.com:444/apm/appmap"
        APM_API_TOKEN_PR = credentials('APM_API_TOKEN_PR')
        RULES_FILE = "rules/rules.json"
    }
    // Set scheduler periodicity
    triggers { cron('H H/2 * * *') }


    // Stages and steps execution
    stages {
        stage('checkout'){
            steps{
                git branch: "${GIT_BRANCH}",
                url: "${GIT_URL}",
                credentialsId: "${GIT_AUTH}"
                dir('rules'){
                    git branch: "${RULES_GIT_BRANCH}",
                    url: "${RULES_GIT_URL}",
                    credentialsId: "${GIT_AUTH}"
                }
            }
         }
         stage('prepare python env'){
            steps{
                sh """
                if [ ! -f ${WORKSPACE}/venv/bin/python ];then
                    /bin/virtualenv --never-download venv;
                fi
                """
                sh '${WORKSPACE}/venv/bin/pip install --proxy=${PROXY} -r ${WORKSPACE}/requirements.txt'
            }
         }
         stage('Qualidade'){
             steps{
                 sh """
                 ${WORKSPACE}/venv/bin/python ${WORKSPACE}/atc-rules.py \
                 -u ${APM_REST_URL_CQ} -t ${APM_API_TOKEN_CQ} \
                 -r ${RULES_FILE}
                 """
             }
         }
         stage('Producao'){
             steps{
                 sh """
                 ${WORKSPACE}/venv/bin/python ${WORKSPACE}/atc-rules.py \
                 -u ${APM_REST_URL_PR} -t ${APM_API_TOKEN_PR} \
                 -r ${RULES_FILE}
                 """
             }
         }
    }

    // Post processing and notifications
    post {
        always {
            echo "Job has finished!"
        }
        success {
            echo "Job ended successfully!"
        }
        failure {
            echo "Job ended with failures!"
            mail to:"rui.cardoso.alves@cgd.pt", subject:"FAILED: ${currentBuild.fullDisplayName}",
            body: "Boo, we failed. check pipeline in ${env.RUN_DISPLAY_URL}"
        }
        unstable {
            echo "Job ended unstable!"
        }
        changed {
            echo "The state of the job changed from previous execution!"

        }
    }
}
```

__To test the rest api call:__
```
pip install -r requirements.txt
```

```
python atc-rules.py -u ${APM_REST_URL} -t ${APM_API_TOKEN} -r ${RULES_FILE}
APM_REST_URL: https://apm.cqgrupocgd.com:444/apm/appmap
RULES_FILE: rules.json
APM_API_TOKEN: 4bf35638-eec9-48c0-8394-db3884119f1d
```
__rules.json example:__
```
{
  "rules": [
    {
      "name": "first rule as an example",
      "query": {
        "orItems": [
          {
            "andItems": [
              {
                "itemType": "attributeFilter",
                "attributeName": "agent",
                "attributeOperator": "MATCHES",
                "values": [
                  ".*-agent-.*"
                ]
              }
            ]
          }
        ]
      },
      "regex": [
        {
          "attribute": "agent",
          "expression": "(.*)-agent-(.*)$"
        },
        {
          "attribute": "hostname",
          "expression": "(.*)"
        }
      ],
      "attributes": {
        "applicationServer": "jvm-{0}",
        "applicationPlatform": "platform-{1}",
        "customHost": "{2}"
      }
    },
    {
      "name": "second rule as an example",
      "query": {
        "includeStartPoint": false,
        "orItems":[
          {
            "andItems":[
              {
                "itemType" : "attributeFilter",
                "attributeName": "agentDomain",
                "attributeOperator": "IN",
                "values": [ "SuperDomain/testDomain" ]
              },
              {
                "itemType" : "attributeFilter",
                "attributeName": "agent",
                "attributeOperator": "MATCHES",
                "values": [ ".*test.*" ]
              }
            ]
          }
        ]
      },
      "attributes": {
        "exampleAttribute": "exampleValue"
      }
    }
  ]
}
```
__requirements.txt:__<br>
urllib3<br>
requests<br>

__script de python:__
```
from optparse import OptionParser
import json
import logging
import sys
import requests
from pprint import pformat
import re
import urllib3
import copy
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def parser(args):
    """Script Arguments Parser ( using optparse module )"""

    opt_parser = OptionParser()

    opt_parser.add_option("-u", "--rest-url", dest="rest_url", help="CA APM Rest url")
    opt_parser.add_option("-t", "--api-token", dest="api_token", help="CA APM API Rest api token")
    opt_parser.add_option("-r", "--rules", dest="rules", help="Path to attributes rules file")
    opt_parser.add_option("-v", "--verbose_level", dest="verbose_level", default=20, help="Define log verbosity level")

    options = opt_parser.parse_args(args)[0]

    if not options.rest_url:
        opt_parser.error("CA APM Rest url is not defined! - (option -h for help)")
    if not options.api_token:
        opt_parser.error("CA APM API Rest api token is not defined! - (option -h for help)")
    if not options.rules:
        opt_parser.error("Path to attributes rules file is not defined! - (option -h for help)")

    return options


class ApmApi(object):
    """APM API Resource"""
    def __init__(self, rest_url, auth_token):
        self.rest_url = rest_url
        self.auth_token = auth_token
        self.headers = {
            'Content-type': 'application/hal+json;charset=utf-8',
            'Authorization': 'Bearer {0}'.format(self.auth_token)
        }

    def get_vertex_list(self, query):
        """Get a vertex list base on APM query"""
        response = requests.post('{0}/graph/vertex'.format(self.rest_url),
                                json=query,
                                headers=self.headers, verify=False)
        vertex_list = response.json()['_embedded']['vertex']
        logging.debug(pformat(vertex_list))
        return vertex_list

    def update_vertex(self, vertex_id, attributes):
        """Update vertex_id object with specified attributes"""
        url = "{0}/graph/vertex/{1}".format(self.rest_url, vertex_id)
        payload = {}
        for key in attributes:
            payload[key] = [attributes[key]]

        update_payload = {'attributes': payload}
        logging.info("Updating vertex id {0} with attributes -> {1}".format(vertex_id, update_payload))
        response = requests.patch(url, json=update_payload, headers=self.headers, verify=False)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            logging.debug(response.text)
        return response


def init_logging(logging_level):
    """ Initialize logging facility
    Logging level - numeric value
    CRITICAL - 50 / ERROR - 40 / WARNING - 30 / INFO - 20 / DEBUG - 10 / NOTSET - 0 """
    logging.basicConfig(level=int(logging_level), format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    logging.info("Logging facility is loaded.")


def load_rules(json_file_path):
    """Load attributes rules file to dict"""
    with open(json_file_path) as f:
        data = json.load(f)
        logging.debug("Rules Loaded: {0}".format(pformat(data)))
        return data


def regex_replace_attributes(vertex_attrs, regex_expressions, new_attrs):
    """Replace new attributes values with variables if needed"""
    logging.debug("regex replace data: -> attributes: {0} \n-> regex expressions: {1} \n-> new attributes: {2}".format(
        pformat(vertex_attrs), pformat(regex_expressions), pformat(new_attrs)))

    format_groups = ()
    for rule in regex_expressions:
        r = re.compile(rule['expression'])
        attr = vertex_attrs[rule['attribute']]
        if len(attr) > 1:
            logging.warn("Regex rule supports only one value (first will be used), but found {0}={1}".format(rule['attribute'], pformat(attr)))
        format_groups = format_groups + r.search(attr[0]).groups()

    pattributes = {}
    for key, value in new_attrs.iteritems():
        if value is not None:
            pattributes[key] = value.format(*format_groups)
        else:
            pattributes[key] = None

    return pattributes


def vertex_needs_update(vertex_attrs, new_attrs):
    """remove attributes that already been set or deleted"""
    pattributes = copy.deepcopy(new_attrs)
    for key in new_attrs.keys():
        if (key in vertex_attrs and vertex_attrs[key][0] == new_attrs[key]) or \
                (key not in vertex_attrs and new_attrs[key] is None):
            del pattributes[key]

    return pattributes


def main():
    """Main logic."""
    args = parser(sys.argv)
    init_logging(args.verbose_level)
    data = load_rules(args.rules)
    apm_api = ApmApi(args.rest_url, args.api_token)
    count = 0
    for rule in data['rules']:
        logging.info("Executing rule with name: {0}".format(rule['name']))
        vertex_list = apm_api.get_vertex_list(rule['query'])
        for vertex in vertex_list:
            attr = rule['attributes']
            if 'regex' in rule:
                try:
                    attr = regex_replace_attributes(vertex['attributes'], rule['regex'], rule['attributes'])
                except KeyError as err:
                    logging.warn("Problem with key in this vertex: {0} \n-> rule: {1} \n-> KeyError: {2}".format(
                        pformat(vertex),
                        pformat(rule),
                        err))
                    continue
            fattrs = vertex_needs_update(vertex['attributes'], attr)
            if fattrs:
                apm_api.update_vertex(vertex['id'], fattrs)
                count += 1
            else:
                logging.debug("Vertex id {0} already updated with attributes: {1}".format(vertex['id'], attr))

    logging.info("Updated vertex count: {0}".format(count))


if __name__ == '__main__':
    main()
```



__Testing with curl:__
```
curl -Lk -H "Authorization: Bearer 491d322b-410a-4dbb-b0c5-5e0a7e97acbc" -H "Accept: application/json" -H "Content-Type: application/json" https://apm.grupocgd.com:444/apm/appmap/apmData/schema 
```
```
curl -Lk -H "Authorization: Bearer 491d322b-410a-4dbb-b0c5-5e0a7e97acbc" -H "Accept: application/json" -H "Content-Type: application/json" https://apm.grupocgd.com:444/apm/appmap/graph/incremental?sinceVersion=0 
```
Query to search latest updates:
```
curl -Lk -H "Authorization: Bearer 491d322b-410a-4dbb-b0c5-5e0a7e97acbc" -H "Accept: application/json" -H "Content-Type: application/json" https://apm.grupocgd.com:444/apm/appmap/audit?from=2020-07-01T12:25:41Z&to=2020-07-08T15:20:43Z&action=UPDATE&type=ALERT
```

Send the output to a json file:
```
 curl -Lk -o output.json -H "Authorization: Bearer 491d322b-410a-4dbb-b0c5-5e0a7e97acbc" -H "Accept: application/json" -H "Content-Type: application/json" http://lpc6001mon22.grupocgd.com:8081/apm/appmap/audit?from=2019-07-01T12:25:41Z&to=2020-07-08T15:20:43Z&action=UPDATE&type=ALERT -o file.json
```