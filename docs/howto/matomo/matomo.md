# Installing Matomo

#### Hardware Requirements
Taking into account the number of daily visitors to internal sites ~ 37k we are estimating about 800k unique visitors per month. Migrating the existing Caixadirecta in Urchin we are looking at an estimate of 100 million pageviews per month, with the components being segregated:<br>

![tracking-requirements](assets/tracking-matomo.png "tracking visitors")

#### Software Requirements
 - The recommendation is to install the latest version of PHP 7.x with the extensions php7.x-curl php7.x-gd php7.x-cli mysql-server php7.x-mysql php-xml php7.x-mbstring.<br>
 For this part you will need to clone the git

- MySQL 5.7+ or MariaDB is recommended for the management of Relational data. In a technical analysis we can conclude that MariaDB is the database engine with the best performance, is open source and exists in the local OS repositories.

- Installation: Installation can be done centrally using Ansible to install python packages.

- HA-PROXY configuration: Check the HA-Proxy for a free address and request to reserve it:
__Address management:__ [Ha-PROXY available free Ip's](https://teamsites.portalssi.grupocgd.com/deg-canais/DocumentaoAplicaes/TechnicalDocumentation/HAProxy"Heap")<br>
__Excel:__ POCHA5.xls<br>

It will be necessary to reserve an available frontend and backend address to later be assigned when creating the dns.<br>
In order to create the dns entry you have to open a service catalog as the example below:<br>


![service-catalog](assets/dns1.png "service catalog")
![service-catalog-2](assets/dns2.png "service catalog 2")

Then we will pull the existing HA-PROXY repo code from gitlab and in the sites.yml file in the matomo domain, add a new entry with the dns name and the ip’s reserved in the HA-PROXY excel.

[HA-PROXY gitlab repo](https://gitlab.grupocgd.com/ansible-inventories/nlb-inventory.git "Ha-Proxy Gitlab Repo")

![nlb iventory project](assets/ha-proxy.png "HA-Proxy project")

After creating the entry in the HA-proxy configuration file, it is necessary to load a trusted certificate for the handshake to be performed.<br>
The certificate was created as follows:<br>

Log on [AWX Homepage](https://awx.grupocgd.com "AWX Homepage") with your domain user and search for the Job template __ansible-job-certificates__<br>

What the job does is to create a csr file and a private key: stands for Certificate Signing Request as defined in PKCS#10; it contains information such as the public key and common name required by a Certificate Authority to create and sign a certificate for the requester.<br>
```
- hosts: localhost
  connection: localhost

  tasks:
  - name: ensure tmp directory exists
    file:
      path: "{{ ansible_binaries_cache_folder }}"
      state: directory
    register: tmp_folder

  - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
    openssl_privatekey:
      path: "{{ ansible_binaries_cache_folder }}/{{ cert_common_name }}.pem"

  - name: Generate an OpenSSL Certificate Signing Request
    openssl_csr:
      path: "{{ ansible_binaries_cache_folder }}/{{ cert_common_name }}.csr"
      privatekey_path: "{{ ansible_binaries_cache_folder }}/{{ cert_common_name }}.pem"
      common_name: "{{ cert_common_name }}"
      country_name: "{{ cert_country_name | default('PT') }}"
      email_address: "{{ cert_email_address }}"
      organization_name : "{{ cert_organization_name | default ('Caixa Geral de Depositos S.A.') }}"
      organizational_unit_name: "{{ cert_organizational_unit_name | default ('SSI-Projectos WEB') }}"
      subject_alt_name: "DNS:{{ cert_common_name }},DNS:{{  cert_common_name.split('.')[0] | lower }}\
      {% if cert_subject_alt_name is defined and cert_subject_alt_name|length > 0 %},\
      {{ cert_subject_alt_name }}{% endif %}"

  - name: send email with output
    mail:
     subject: "Certificate request for {{ cert_common_name }} has been successfully provisioned."
     to: "{{ cert_email_address }}"
     from: "{{ smtp_from }}"
     host: "{{ smtp_server }}"
     attach:
      - "{{ tmp_folder.path }}/{{ cert_common_name }}.csr"
      - "{{ tmp_folder.path }}/{{ cert_common_name }}.pem"

  - name: cleanup csr and private key from temporary directory
    file:
      path: "{{ item }}"
      state: absent
    with_items:
      - "{{ tmp_folder.path }}/{{ cert_common_name }}.csr"
      - "{{ tmp_folder.path }}/{{ cert_common_name }}.pem"
```
__variables:__
```
ansible_binaries_cache_folder: ~/.ansible/binaries_cache
smtp_server: mail.grupocgd.com
smtp_from: ansible@cgd.pt
```
Job has a survey attached so when you launch it, it asks for the DNS name to create the csr file.<br>
![certificate survey](assets/survey.png "survey")<br>
Now that you have the csr file you will open a service catalog to be signed by the Certificate Authority of CGD.<br>

![catalog certificate](assets/catalog.png "catalog")<br>
After a while you should receive the signed certificate .cer(Base 64) on your email<br>
.cer stands simply for certificate, usually an X509v3 certificate, again the encoding could be PEM or DER; a certificate contains the public key, but it contains much more information (most importantly the signature by the Certificate Authority over the data and public key, of course).<br>

After receiving the signed certificate you should concatenate the cer(Base64) with your private key generated in Ansible Job  .pem and save it to a .txt file.<br>

Log on [Jenkins Homepage](https://jenkins.grupocgd.com "Jenkins Homepage") and pass the txt file to the job __encrypt_certificate__<br>
This job will encrypt the certificate with the vault password for later decryption on HA-PROXY nodes.<br>
This is mostly to avoid any certificate on clear text being exposed in the Gitlab<br>
![encrypt certificate](assets/encrypt.png "encrypt")

After that you will load the certificate encrypted to the Gitlab Project under your site and domain.
[HA-PROXY gitlab repo](https://gitlab.grupocgd.com/ansible-inventories/nlb-inventory.git "Ha-Proxy Gitlab Repo")

And go back to Jenkins where you will run the Job __pipeline_create_confs__ based on the domain you want to deploy the site's certificate.<br>

### Firewall Rules
In production environment is necessary to request the firewall rules for external exposure of the matomo snippet address for monitoring external CGD url's such as CaixaDirecta and for this we will ask for firewall rules for the external HA-PROXY nodes (lpe6001nlb01, lpe6001nlb02, lpe6001nlb03, lpe6001nlb04) for the php nodes (exposing port 8081):<br>

Go to the Teamsites url and find the excel with the firewall rules [Teamsites Page](http://teamsites.portalssi.grupocgd.com/psi/its/Documentos%20Partilhados/Forms/AllItems.aspx?RootFolder=http%3a%2f%2fteamsites%2eportalssi%2egrupocgd%2ecom%2fpsi%2fits%2fDocumentos%20Partilhados%2fNovo%20Circuito%20Regras%20FW%2fMatriz%20Regras%20FW%2fSSI%2dPW&FolderCTID=0x0120007581257813739C4B9C58BD3F466BDA73 "Excel")

It will be also necessary to ask for firewall rules if the php nodes (endpoints) are in the DMZ. In order to balance the requests, communication is established between the HA-PROXY back-end nodes and the front-end nodes of the matomo nodes.<br>
Also if you want to register new nodes from the Internal Vlan you will need to ask for rules to access the matomo database __lpc6001mon17__ that it is in the DMZ zone.<br>

## Install Notes

- Install php and nginx packages and configure both with the help of the Ansible.<br>
Log on [AWX Homepage](https://awx.grupocgd.com "AWX Homepage") and launch the job template __matomo-install-configure__ with the right inventory file pointing to the php endpoints.<br>
![matomo job template](assets/matomo_ansible.png "matomo job template")<br>
- This ansible job will install and configure everything regarding php nodes<br>

If your php nodes are in the DMZ zone and you need to install binaries manually you will have to do a reverse proxy tunnel in order to access the internet.<br>
Run the command:<br>
```
 ssh -R (destination port) 3128:squid.grupocgd.com:3128 user@node_hostname
 export http_proxy=http://localhost:3128
 export https_proxy=$http_proxy
```
If you are installing matomo for the first time you need to access the browser to configure matomo via wizard and in order to do that run the command (if your nodes are in the DMZ):<br>
```
ssh -L 2222(localport):localhost:(destination port)80 user@node_hostname (Expose DMZ port on port 2222 of localhost)
```
Acess the web browser on http://localhost:2222 and proceed with the installation Wizard.<br>
![matomo wizard](assets/matomo-wizard.png "matomo wizard")<br>

Synchronize the config.ini.php file between the php nodes:<br>

- The config.ini.php file is used in all requests made to matomo.

- Matomo files and plugins: Matomo files, including existing plugins, must be in sync with the php nodes.

- Enable database session storage: It will be necessary in a cluster of matomo nodes to activate the database session storage and for that we will add the following entry to the file config.ini.php:<br>
```
session_save_handler = dbtable
```
- Multi_Server_Environment: In order for matomo to know that there are several php nodes, it is necessary to place the following entry in the config.ini.php file (In this way all changes to files that are made via the UI will not be valid because it will be exchanged on a single server):<br>
```
multi_server_environment = 1
```
## Install Mariadb locally

__Bash Script to install and configure Mariadb locally:__
```
#!/bin/bash -e
clear
echo "============================================"
echo "Instalador de MariaDB"
echo "============================================"
banner(){
  echo "+------------------------------------------+"
  printf "| %-40s |\n" "`date`"
  echo "|                                          |"
  printf "|`tput bold` %-40s `tput sgr0`|\n" "$@"
  echo "+------------------------------------------+"
}
banner "Criado por Tiago Mendes"
sleep 3
version=$(lsb_release -a 2>/dev/null | grep Description | awk '{print $2}' | awk -F\. '{print$1}')
install_repo(){
FILE="/etc/yum.repos.d/mariadb.repo"
if [ $version == "RedHat" ]; then
echo "Adicionando repositorio mariadb aos repositorios locais do $version"
/usr/bin/cat <<EOT >>$FILE
MariaDB 10.3 RedHat repository list - created 2018-12-13 18:13 UTC
http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl=http://yum.mariadb.org/10.3/rhel7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOT
fi
if [ $version == "Fedora" ]; then
echo "Adicionando repositorio mariadb aos repositorios locais do $version"
/usr/bin/cat <<EOT >>$FILE
MariaDB 10.3 Fedora repository list - created 2019-02-06 11:24 UTC
http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.3/fedora28-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOT
fi
echo "Precione qualquer tecla para sair..."
read -s exit
exit 1
}
instalar_packages(){
	for i in "mariadb httpd"
		do 
		echo $i
		echo "Que pacote deseja instalar: $i"
		read -e package
		if [ $version == "Fedora" ]; then
		dnf install -y -v $package
		fi
		if [ $version == "Fedora" ]; then
                yum install -y -v $package
                fi
		echo "================================================"
		echo "Instalação concluida, vamos levantar os serviços"
		echo "================================================"
		systemctl start $package.service
		systemctl enable $package.service
		done 
}
mariadb(){
echo "========================================================="
echo "Vamos criar a base de dados com os pivilégios necessários"
echo "========================================================="
/usr/bin/mysql_secure_installation
echo "===================================================="
echo "Verificar se a base de dados já existe ou se é nova"
echo "===================================================="
echo "Hostname da maquina da base de dados: "
read -e dbhost
echo "Nome da Base de dados: "
read -e dbname
echo "User da Base de dados: "
read -e dbuser
while true; do
    read -s -p "Senha de acesso a Base de dados para o user root: " dbpass  
    echo "$dbpass" | sed 's/./*/g'
    read -s -p "Repita a senha: " dbpass2
    echo "$password2" | sed 's/./*/g'
    [ "$dbpass" = "$dbpass2" ] && echo "Password Identicas" && break
    echo "Passwords nao combinam,tentar novamente"
done
echo "======================================================"
echo "Criando e Verificando a existencia da base de dados"
echo "======================================================"
mysql=`mysql --host=$dbhost --user=root --password=$dbpass -s -N --execute="SELECT IF(EXISTS (SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbname'), 'Yes','No')";`
if [ "$mysql" == "Yes" ]; then
        echo "Base de dados existente, deseja usar a mesma? (y/n)"
        read -e usethis
        if [ "$usethis" == n ]; then
                echo "Voltar a correr o script para gerar nova base de dados"
                echo "Precione qualquer tecla para sair..."
                read -s exit
                exit 1
	else
	echo "====================================================================="
        echo "Voltar a correr o script para instalacao do wordpress na bd existente"
        echo "====================================================================="

        fi

else
        mysql=`mysql --host=$dbhost --user=root --password=$dbpass --execute="CREATE DATABASE $dbname";`
	mysql=`mysql --host=$dbhost --user=root --password=$dbpass --execute="CREATE USER '$dbuser'@'$dbhost' IDENTIFIED BY '$dbpass'";`
	echo "entrei"
	mysql=`mysql --host=$dbhost --user=root --password=$dbpass --execute="GRANT ALL PRIVILEGES on $dbname.* to '$dbuser'@'$dbhost' identified by '$dbpass'";`
        echo "=============================================="
        echo "Base de dados criada e privilegios concedidos!"
        echo "=============================================="
fi
}
remove_mariadb(){
echo "===================================================="
echo "Desinstalando MariaDB, aguarde..."
echo "===================================================="
sudo yum remove mariadb mariadb-server -y
echo "---- A remover directoria do mysql----"
rm -rf /var/lib/mysql
}
options=("Adicionar Repositorio" "Instalar Packages" "Configuracao MariaDB" "Remove MariaDB" "Quit")
select opt in "${options[@]}"
do    
	case $opt in
        "Adicionar Repositorio")
        install_repo
        break
	    ;;
        "Instalar Packages")
        instalar_packages
        sudo yum clean all
	break
            ;;
	"Configuracao MariaDB")
        mariadb
        break
            ;;
        "Remove MariaDB")
        remove_mariadb
        break
            ;;
        "Quit")
         exit 0
            break
            ;;
        *) echo "Opcao invalida $REPLY";;
    esac
done 
```

## Users Management
User management is done through the user management console on the matomo settings page.<br>
![matomo users](assets/matomo-users.png "matomo users")<br>
There is also the possibility to create local users and it allows you to assign permissions to a website.<br>
![matomo users](assets/matomo-users1.png "matomo users")
However, the first time you try to login, the user should not have any access permissions, being necessary to give permissions so that the user can login.<br>
![matomo users](assets/matomo-users2.png "matomo users")
Permissions are given at the user level and for each existing site. There is no possibility to assign permissions to groups of AD.<br>
![matomo users](assets/matomo-users3.png "matomo users")

Matomo defines 4 types of permissions:<br>

- __view permission:__ 
Applies to a specific site. With that permission, a user can view the reports for a given site.<br>

- __write permission:__ 
Applies to a specific site. With that permission, a user can view the reports for a given site, and create, update and delete entities for the website, such as Goals, Funnels, Forms, A / B Tests, Heatmaps, Session Recordings, etc.<br>

- __admin permission:__ 
Applies to a specific site. With that permission, a user can view and configure a given site (name, URLs, timezone, etc.). They can also grant other users the "view" or "admin" permission.<br>

- __super user permission:__ 
Applies to whole Matomo (all sites). With that permission, a user can view and configure all sites. They can also perform all administrative tasks such as add new sites, add users, change user permissions, activate and deactivate plugins or install new ones from the Marketplace.

Reference: [ACL's Documentation](https://matomo.org/docs/manage-users/ "Matomo ACL's")<br>

## Site Management

Site management is done in the control panel.<br>
You can add an external website as an internal website (intranet). After creating the new address, a piece of Javascript code is generated so that it can be injected into the pages to be monitored.<br>
Each site will have a unique id that is loaded in the snippet, so the matomo can interpret the collected metrics and associate them with the dashboards.<br>

![matomo site management](assets/matomo-sites.png "matomo sites")<br>

__Matomo Sites Attributes:__<br>
__1.__ Name: the website’s name<br>
__2.__ Alias ​​URLs: domains and subdomains that are tracked in this website. This will ensure that tracked domains don’t appear in the Referrer report.<br>
__3.__ Excluded IPs: a list of IPs, or IP ranges (132.4.3. * Or 143.2 ..) to be excluded from being tracked on all websites.<br>
__4.__ Excluded Parameters: URL query parameters that should be excluded from URLs, for example session parameters. This will ensure your Actions> Pages reports stay clean and easy to analyze.<br>
__5.__ Time zone: each website will report data in its given time zone. If you update the website time zone, only the reports going forward will be affected.<br>
__6.__ Currency: the currency symbol will be displayed next to revenue in your goals.<br>
__7.__ Ecommerce: if your website is an Ecommerce store you can enable the Ecommerce reports<br>

After that is generated a Javascript Snippet:<br>
![matomo snippet](assets/matomo-snippet.png "matomo snippet")
 
[Manage Websites Matomo ](https://matomo.org/docs/manage-websites/ "Matomo Websites")<br>
[Tracking Websites Matomo ](https://developer.matomo.org/guides/tracking-javascript-guide "Matomo Websites")<br>

## Procedures & Troubleshooting

### Evaluate Matomo's performance:
In order to evaluate the performance of the Matomo database we will login to the matomo database machine (lpc6001mon17), and belonging to the GGSSIA-DEG-UEG3-Channels group we can login directly to the piwik user:<br>
```
% GGSSIA-DEG-UEG3-Channels ALL = (piwik) NOPASSWD: /bin/bash
sudo -iu piwik
```
Logging in to the Piwik user we will run the existing performance script in /apps/Piwik/script_tuning that will retrieve the access variables to a local file (~/.my.cnf), the password can be found in the team keepass of Channels.<br>

Output Example:
```
-- MYSQL PERFORMANCE TUNING PRIMER –
MySQL Version 5.5.60-MariaDB x86_64
Uptime = 41 days 17 hrs 10 min 46 sec
Avg. qps = 101
Total Questions = 365319354
Threads Connected = 17
(..)
```
After that it will suggest to change the values found in /etc/my.cnf file <br>
After changing the file, it is necessary to restart the mariadb service. With the piwik user it is possible to restart the service using the command<br>

```
sudo systemctl restart mariadb
```

### Mariadb Performance Queries:
Log in to the mysql user (sudo –u mysql / bin / bash) and log in to the piwik database (mysql -u piwik -p piwik) using the existing password in keepass<br

__Query to find out how many GB the piwik database has:__
```
SELECT table_schema as "Database", Round (Sum (data_length + index_length) / 1024/1024/1024, 1) AS "Size in GB" FROM information_schema.tables GROUP BY table_schema;>
```
__Run the following query to find the RIBPS metric (Recommended InnoDB Buffer Pool Size) based on all data and indexes:__
```
SELECT CEILING (Total_InnoDB_Bytes * 1.6 / POWER (1024.3)) RIBPS FROM
(SELECT SUM (data_length + index_length) Total_InnoDB_Bytes
FROM information_schema.tables WHERE engine = 'InnoDB') A;
```
__Query to find out how many Gb of memory are currently being used by Matomo:__
```
SELECT (PagesData * PageSize) / POWER (1024.3) DataGB FROM
(SELECT variable_value PagesData
FROM information_schema.global_status
WHERE variable_name = 'Innodb_buffer_pool_pages_data') A,
(SELECT variable_value PageSize
FROM information_schema.global_status
WHERE variable_name = 'Innodb_page_size') B;
```

__Query to know the tables that use more memory:__
```
select table_schema, table_name as table_name, round (data_length / 1024 / 1024,2) as size_mb from information_schema.tables where table_schema like 'piwik' order by size_mb desc limit 10;
```
!!!note "Note"
In order to optimize the database and before changing the sql my.cnf file, it is necessary to take into account that there are metrics that must be met. <br>
For example the ‘Innodb_buffer_pool_size is configured to 50 to 75 percent of system memory’ and in our case it’s at the limit since we’re putting that parameter at 45GB and we have 64GB of available memory.

[Optimizing Innodb DiskIo](https://dev.mysql.com/doc/refman/5.5/en/optimizing-innodb-diskio.html "Matomo SQL Optimize")
[Innodb Variables](https://mariadb.com/kb/en/library/innodb-system-variables/ "Matomo SQL Optimize")

### Activate debug mode on Matomo logs:
Add to te file (/apps/Piwik/piwik/config/config.ini.php) the lines:
![matomo log](assets/matomo-log.png "matomo log")

### Activate debug mode on tracker:
Add to te file (/apps/Piwik/piwik/config/config.ini.php) the line:
![matomo tracker](assets/matomo-tracker.png "matomo tracker")

### Update matomo to the latest version:
In order to update matomo:
- We first back up the existing php configuration file at (/apps/matomo/matomo/config/config.ini.php) - Job template does that
- Download the latest version of matomo available in the archive:
```
wget https://builds.matomo.org/matomo.zip
```
- Transfer the files to the Nexus Repository and Launch the Matomo Job Template in the AWX.

- After that you will need to upgrade the database with the command:
```
/opt/rh/rh-php72/root/bin/php /apps/matomo/matomo/console core:update
```
__You can run all these commands or you can use an ansible playbook that automates it:__ <br>
```
  # ============================================================
  #  Author: Tiago
  #  Description: Upgrade Matomo version
  #  Reference:
  #
  #
  #
  # ============================================================
- hosts: all
  vars:
     date: "{{ lookup('pipe', 'date +%Y%m%d-%H%M') }}"
     matomo_version: 3.13.4

  vars_files:
    - "vault/secrets.yml"

  tasks:
#  - name: Download latest Version of Matomo from matomo repository
#    get_url:
#      url: https://nexus.grupocgd.com/repository/matomo/
#      dest: "{{ ansible_binaries_cache_folder }}"
#    delegate_to: 127.0.0.1  


#  - name: Push latest version to Nexus
#    command: "curl -kv -u {{user_nexus}}:{{pass_nexus}} -X PUT --upload-file {{ ansible_binaries_cache_folder }}/matomo.zip {{nexus_url}}com/cgd/matomo/latest/matomo.zip -H 'authorization: Basic YWRtaW46YWRtaW4xMjM=' -H 'cache-control: no-cache'"
#    become: true
#    register: nexus_push
#    delegate_to: 127.0.0.1

  - name: Download Matomo Latest Version
    get_url:
      dest: "{{ ansible_binaries_cache_folder }}/matomo.zip"
      url: "{{ nexus_url }}com/cgd/matomo/{{matomo_version}}/matomo-{{matomo_version}}.zip"
      url_username: "{{user_nexus}}"
      url_password: "{{pass_nexus}}"
      validate_certs: False
    delegate_to: 127.0.0.1
    register: Binaries_Matomo

  - name: Backup do ficheiro existente de config.ini do matomo
    copy:
      remote_src: yes
      src: "{{ matomo_dir }}/config/config.ini.php"
      dest: "/apps/matomo/config.ini_{{ date }}.php"
      owner: matomo
      group: matomo
    ignore_errors: yes
    register: backup_config_ini


  - name: Descompactar Binarios Remotamente
    become: true
    unarchive:
      src: "{{ ansible_binaries_cache_folder }}/matomo.zip"
      dest: "/apps/matomo/"
      owner: nginx
      group: nginx
    register: unarchive_matomo

  - name: Faz o upgrade da base de dados
    become_user: nginx
    command: "{{ rhel_php_path }} {{ matomo_dir }}/console core:update -vvv --yes"
    when: unarchive_matomo is successful

  - name: Restart rh-php72-php-fpm on RHEL
    become: true
    service:
      name: rh-php72-php-fpm
      state: restarted

  - name: Restart nginx on RHEL
    become: true
    service:
      name: nginx
      state: restarted
```

### Disable Internet access in Matomo:
Add to the file (/apps/matomo/matomo/config/config.ini.php) the lines:<br>
```
enable_internet_features = 0
enable_marketplace = 0
```
These lines are already loaded into a template by default in the Ansible php install job.

### Change the directory where the database writes the data:
In order not to fill the default dir of writing sql (/var/lib/mysql) let's change the entry in the sql configuration file (/etc/my.cnf):<br>
![matomo data](assets/matomo-data.png "matomo data")

### Not being able to view the logs with the matomo user:
As a matomo user we are not allowed to see the logs in the /var/log/ directory associated with the different services.<br>
However, as matomo user we are able to access nginx user and mariadb in order to read the logs in that directory.<br>
```
sudo -u nginx bash
```
### Configure proxy in matomo:
In order to configure the proxy in matomo we will add the proxy entry to the /apps/matomo/matomo/config/config.ini.php file:<br>
```
[proxy]
host = "proxy.grupocgd.com"
port = 8080
username = "user"
password = "password"
```
### Change the default password of the super_user matomo to access the database:
Generate the password (for this example we use the matomo password):<br>
```
php72 -r 'echo password_hash (md5 ("matomo"), PASSWORD_DEFAULT). "\ n"; '
```
__Generated hash: $2y$10$DXpyEobALXNtdBy32WDTeOY6RM8IK7I6W9IrT01/EBkhC9bIADOjK__<br>

__Change the password:__<br>
Log into the existing database on lpc6001mon17 with the user mysql (mysql -u piwik -p piwik) and run the following query:<br>
```
UPDATE matomo_user SET password = "$ 2y $ 10 $ DXpyEobALXNtdBy32WDTeOY6RM8IK7I6W9IrT01 / EBkhC9bIADOjK" WHERE login = 'matomo' AND superuser_access = 1;
```

__If we want it to be a matomo version higher than 2.0.3:__
```
UPDATE `piwik_user` SET` password` = MD5 ('changeMe'), `token_auth` = MD5 (CONCAT (‘ matomo ’, password)) WHERE` login` = 'matomo' AND superuser_access = 1
```

### Create a super_user matomo to access the site:
Log into the existing database on lpc6001mon17 with the user mysql (mysql -u piwik -p piwik) and run the following query:<br>
```
insert into piwik_user (login, password, alias, email, token_auth, superuser_access) VALUES ('matomo', MD5 ('matomo'), 'matomo', 'matomo @ matomo.pt', MD5 (CONCAT ('matomo', MD5 ('matomo'))), 1);
```

### Give access permissions to the database through workstations:
Log into the existing database on lpc6001mon17 with the user mysql (mysql -u piwik -p piwik) and run the following query:<br>

```
MariaDB [matomo]> CREATE USER 'cgdw808148374'@'10.50.13.92' IDENTIFIED BY 'cgdw808148374';
MariaDB [matomo]> GRANT ALL PRIVILEGES ON *. * TO 'cgdw808148374'@'10.50.13.92' WITH GRANT OPTION;
```

### Give access to the database to the remaining php nodes:
Configure the mariadb /etc/my.cnf file with the following entry
```
[mysqld]
datadir = /var/lib/mysql
socket = /var/lib/mysql/mysql.sock
bind-address = 0.0.0.0
```
$ netstat -anp | grep 3306
tcp 0 0 0.0.0.0:3306 0.0.0.0:* LISTEN

```
mysql = `mysql --host = localhost --user = root --password = <password> --execute =" GRANT ALL ON matomo. * to 'matomo'@'10.11.207.242' identified by 'matomo' ";`
```

If it is in the old version where the database is called piwik:<br>
```
mysql = `mysql --host = localhost --user = root --password = --execute =" GRANT ALL ON piwik. * to 'matomo'@'192.168.111.230' identified by 'matomo' ";`
```
### Enable matomo auto-archiving

Create a cron job in the /etc/cron.d/matomo-archive directory:<br>
```
MAILTO = "tiago.miguel.mendes@cgd.pt”
5 * * * * nginx / opt / rh / rh-php72 / root / usr / bin / php / apps / matomo / matomo / consolecore: archive --url = https: //pwa.grupocgd.com> / apps / matomo /matomo/matomo-archive.log
```
###	Clear Matomo Cache ‘Error: Form security failed.’
In order to clear the matomo cache we will run the following command with the user nginx:<br>
```
/opt/rh/rh-php72/ root/bin/php/apps/matomo/matomo/console cache: clear -vvv
DEBUG [2019-09-05 09:32:03] 129862 UserSynchronizer :: makeConfigured (): LDAP access synchronization not enabled.
DEBUG [2019-09-05 09:32:03] 129862 UserSynchronizer :: makeConfigured: configuring with defaultSitesWithViewAccess =
```
### Activate Slow query-log on matomo
In order to activate the slow-query log in Matomo we will change the /etc/my.cnf file to the log entry:<br>
![matomo slow](assets/matomo-slow.png "matomo slow")

And then we will set the global variable logging in the matomo database:<br>
```
SET GLOBAL slow_query_log = ‘ON’;
```

### Install a new plugin on matomo
In order to install a new plugin on matomo without having to activate the Marketplace to retrieve the plugin remotely from the public repository, we can download it from [Matomo Plugins](https://plugins.matomo.org/ "Matomo plugins") and copy it into the machine(s) in question to the __/apps/matomo/matomo/plugins__ directory<br>
```
unzip -d matomo/plugins/<plugin>.zip
```
Command line for activating the plugin:<br>
```
php /apps/matomo/matomo/console plugin: activate LoginTokenAuth
```

__Activated plugin LoginTokenAuth__

### Move the data to a new directory and change the directory where the temporary files are written:
In order to move the data to a new directory we will move the data using the rsync protocol which is faster in transferring files:<br>
```
rsync -avh <dir_data> <new_dir>
```
Change the following entries in the database configuration file:<br>
```
[mysqld]
datadir = /DB/mysql
socket = /<new_dir>/mysql/mysql.sock
tmpdir = /<new_dir_tmp>
[client]
port = 3306
socket = / <ew_dir>/mysql/mysql.sock
```

### Make calls directly to a specific host via VIP

There is configured in HA-PROXY an acl that allows to receive the request by server in the VIP directly by putting via the web https://pwa.grupocgd.com/?server=lpc6001mon35<br>
![matomo acl server](assets/ha-proxy-acl-server.png "matomo acl")


## Service commands
Restart php service:<br>
```
sudo systemctl restart rh-php72-php-fpm.service
```
Restart nginx service:<br> 
```
sudo systemctl restart nginx
```
Restart mariadb service:<br>
```
sudo systemctl restart mariadb
```

## Location of logs and configuration files
__PHP error logs:__ /etc/opt/rh/rh-php72/log/php-fpm/error.log<br>
__Nginx error logs:__ /var/log/nginx/error.log<br>
__Matomo's logs:__ /apps/matomo/matomo.log<br>
__Mariadb logs:__ /var/log/mariadb/mariadb.log<br>

__Php-fpm configuration file:__ /etc/opt/rh/rh-php72/php-fpm.d/www.conf<br>
__Matomo php configuration file:__ /apps/matomo/matomo/config/config.ini.php<br>
__Sql configuration file:__ /etc/my.cnf<br>

## Production php nodes & Database
__lpc6001mon34__ 192.168.111.230<br>
__lpc6001mon35__ 192.168.111.231<br>
__lpc6001mon36__ 192.168.111.232<br>
__lpc6001mon37__ 192.168.111.233<br>
__Database:__<br>
__lpc6001mon17__ 192.168.111.188

## Development php nodes & Database
__ldc6001acm01__ 10.11.207.242<br>
__Database:__<br>
__ldc6001acm02__ 10.11.207.243

## Add machines to ssh file
You can add the following to your ssh config file:<br>
```
##############################################
# MAQUINAS MATOMO DESENVOLVIMENTO
##############################################
Host ldc6001acm01 ldc6001acm02
   User username
###############################################
# MAQUINAS MATOMO PRODUCAO
###############################################
#PHP nodes
Host lpc6001mon34 lpc6001mon35 lpc6001mon36 lpc6001mon37
   User username
ProxyCommand  ssh -XA username@lpc6001lgs12 -W %h:%p

#Database
Host lpc6001mon17
   User username
ProxyCommand ssh -XA username@lpc6001lgs12 -W %h:%p
```
## Dump and Restore Matomo database
You can use the container mydumpler that is faster on dumping and importing the sql data<br>
Launch the container with:
```
docker run -it -d --name mydumpler registry.dev.grupocgd.com/cgd/mydumpler:1.0 
```
Log on container and execute the command (example testing on __ldc6001acm02__):
```
mydumper --database=piwik_restore --host=ldc6001acm02 --user=root --port=3306 --password=ldc6001acm02 --outputdir=/apps/matomo/mydump --rows=50000 --threads=6 --compress --build-empty-files --compress-protocol &
```
Check for the process running and guarantee that you don't get logout from the machine bash with the command:<br>
```
while true; do ps -eo pid,comm,cmd,start,etime| grep 87214  && sleep 30 ; done
```
~46mins:<br>
![matomo dump](assets/matomo-dump.png "matomo dump")
To load the database:<br>
```
myloader --database=piwik_restore --host=lpc6001mon17 --user=root --port=3306 --password=matomo --directory=/apps/matomo/mydump --threads=6 --compress-protocol &
```
![matomo load](assets/matomo-dump1.png "matomo load")
