# Rancher

TODO: Construção de página de plataforma para o rancher

## Configuração de um novo role e criação de projeto no rancher

- A autenticação e autorização no rancher estão integradas com o rhsso. Para configuração dos utilizadores ver [acessos](acessos.md)

- Para mapear roles do rhsso no rancher é necessário que o nosso utilizador seja adicionado temporáriamente ao role a configurar. Caso contrário o role não vai aparecer na lista de grupos durante o processo de configuração no rancher.

### Acesso a cluster

- Configurar grupo no acesso ao cluster e atribuir role "Custom"
![add role to cluster](assets/add_role_cluster.png)

- Selecionar permissão de "View Nodes"
![add role to cluster](assets/role_cluster_view.png)

### Criação de novo projeto e atribuição de role Project Members

- Criar novo projeto para aplicação ou grupo de aplicações
![add role to cluster](assets/create_k8s_project.png)

- Atribuir ao grupo o role "Project Members"
![add role to cluster](assets/add_role_project.png)