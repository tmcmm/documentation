# Acessos - Autenticação e Autorização

## Introdução

A autenticação da plataforma CICD (Toolchain) utiliza o rhsso para autenticação e autorização dos seus utilizadores. Podem ser criados roles que representem papéis e acessos em cada uma das tools que constituem a plataforma. Por sua vez este roles ão mapeados nos sistemas de autorização de cada tool que pertença à plataforma de CICD.

TODO: Descrição da plataforma de rhsso e referência a partir desta página

## REALM devops

No rhsso foi criado um REALM que suporta a plaforma de CICD: "devops"

## Definição de roles

A definição de roles no REALM devops deve ter em conta a seguinte nomenclatura: "application id"-"role"

- application id: Grupo de aplicações, sistema aplicacional ou sigla aplicacional que identifica a área tecnológica que será suportada pela plataforma CICD. Exemplos:
    - sdpce (Sigla aplicacional da plataforma de canais eletrónicos)
    - agile (Grupo de aplicações que identificam as várias aplicações da plataforma Agile)
    - arqcentral (Sistema aplicacional que agrega várias aplicações da arquitetura central)

- role: Nome do role que terão os utilizadores que desenvolvem, gerem e/ou administram as aplicações ou grupo aplicacionais. Exemplos:
    - developers
    - owners
    - admins

## Exemplo de criação de role

### Criação de novo role
![add role](assets/add_role.png)

### Utilizadores associados ao role
![role members](assets/role_members.png)
