# User Guide Harbor

## Criar Projectos

### Projectos devem ser criados segundo a seguinte estrutura:


![Harbor-Project](assets/harbor-project.png)

* Ex: cgd-plat_dados-sdcdp:
  - cgd : Grupo pai
  - plat_dados : Sistema aplicacional
  - sdcdp: Sigla aplicacional

* Poderá ser criado um project por ambiente ou segregar tudo o que se trata de um mesmo projecto/equipa num único project diferenciando depois os repositorios Ex-cgd-plat_dados-sdcdp/sdcdp/dev/webservice/

* Projects deverão ser criados como public - Por defeito o harbor atgribui apenas acesso de leitura aos demais utilizadores ficando a cargo do project owner de atribuir as restantes permissões. Caso haja algum impedimento na leitura das imagens deverá ser criado como private.

* Repositorios pessoais para teste deverão ser criados como private.

**Não usar caracteres especiais na definicao do projecto. Separar os nomes atraves de hífen, e usar apenas underscore "_" no caso de ser necessario na especificação do sistema aplicacional**

!!! note "Nota"

    Projectos nao permitem ser criados como nested ou seja projectos dentro de projectos. Ao contrario dos repositorios que ja poderao ser criados nested (ex: cgd-plat_dados-sdcdp/sdcdp/dev/bitwarden/identity).
    

### Guidelines para criacao de imagens 

* Utilizar letras mínisculas e evitar utilizar números ou caractéres especiais para compor o nome da imagem - Não utilizar por exemplo: postgres-canais#3

* Utilizar nomes que sejam auto-explicativos da imagem que se esta a colocar no repositório, Ex:Ansible:3.1 para caracterizar uma imagem com o software ansible 

* Caso seja uma imagem costumizada deverá seguir o mesmo padrão dos repositorios separando por "-" ou "/" sendo que à semelhança do repositório do docker hub as imagens estão separadas por "/". Deverá ser criada com cgd seguido da sigla aplicacional 


### Guidelines para criacao de repositorios externos  

* Repositorios externos são configurados no painel de administração do harbor.

* Por defeito o harbor disponibiliza alguns repositórios default como é o caso do docker hub ou helm hub no caso dos charts.

* Por cada imagem externa que seja necessária é preciso criar uma replication rule à semelhança da imagem abaixo:

  - Deverá seguir o critério identificação do repositório externo - tipo de imagem (software)

* Iremos ter um repositório publico (apenas de leitura) com as imagens publicas mais necessitadas/usadas

* As imagens externas poderão ser carregadas no project publico de imagens externas ou noutro à escolha caso se justifique

![Harbor-Project](assets/external-registry.png)


### Project Quotas & Labels

* Os projects são criados com as seguintes restrições que poderão ser facilmente alteradas caso haja uma necessidade de aumento de número de imagens/tamanho do repositório. Estes parâmetros são alterados na tab de administração do harbor.

![Harbor-Project](assets/project-quotas.png)

* A semelhança das quotas também é possível definir Labels ao nível do harbor (gerais) para uso dos utilizadores, ou os próprios administradores do project poderão criá-las na tab de labels dentro do project.
Isto permite a procura das imagens dentro dos repositórios pesquisando por label


![Harbor-Project](assets/labels.png)


![Harbor-Project](assets/filter.png)
