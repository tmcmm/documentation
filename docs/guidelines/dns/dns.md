# Domain Name System para aplicações

## DNS Interno

### Subdomínios reservados para os vários ambientes na CGD:

* Desenvolvimento -> .dev.grupocgd.com
* Testes Integrados -> .tst.grupocgd.com
* Controlo de qualidade -> .qld.grupocgd.com
* Staging ou pré-produção -> .stg.grupocgd.com
* Disaster Recovey Site -> .drs.grupocgd.com
* Produção -> .grupocgd.com ou .prd.grupocgd.com (quando aplicável e se quiser especificar o ambiente no host)

!!! note "Nota"

    Em caso de ser necessário várias instânciações do mesmo ambiente 
    deverão ser complementados com um incremento numérico. 
    Exemplo para ambiente de desenvolvimento: "*.dev2.grupocgd.com"

### Guidelines para nomeação de registos para serviços

* Utilizar letras mínisculas e evitar utilizar números ou outros caractéres para compor o nome do serviço - Não utilizar por exemplo: **myservice_name2**.dev.grupocgd.com

* O único caracter que poderá ser utilizado é o hífen para colar nomes que identificam o serviço - Exemplo: **myservice-france**.dev.grupocgd.com

* Para serviços que expôem exclusivamente web services ou api's deve ser usado o prefixo "api-" - Exemplo: **api-**myservicename.dev.grupocgd.com

## Wildcard DNS 

### Domínio de dns para aplicações:
Os registos de dns internos que necessitem de nome com wildcards ou relacionados com certificados wildcard devem ser criados como subdominios sobre o domínio com nome **.grupocgd.com** ou sobre os subdomínios reservados para os vários ambientes não produtivos.

* Exemplo para registo dns "docs" de produção com wildcard : *.docs.grupocgd.com
* Exemplo para registo dns "docs" de qualidade com wildcard : *.docs.qld.grupocgd.com

!!! note "Nota"

    Não deverá ser criado nenhum wildcard dns a apontar para o dominio root grupocgd.com
    ou para os subdomínios reservados para os ambientes da CGD, apenas para subdominios criados para o efeito. 
